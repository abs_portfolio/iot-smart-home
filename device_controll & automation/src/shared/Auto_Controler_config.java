/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ghost
 */
@XmlRootElement

public class Auto_Controler_config implements Serializable {

    @XmlElement
    private int device_ttl;
    private boolean device_ttl_bool = false;
    @XmlElement
    private double critical_temprature_max;
    private boolean critical_temprature_max_bool = false;
    @XmlElement
    private double critical_temprature_min;
    private boolean critical_temprature_min_bool = false;
    @XmlElement
    private double critical_humidity_max;
    private boolean critical_humidity_max_bool = false;
    @XmlElement
    private double critical_humidity_min;
    private boolean critical_humidity_min_bool = false;
    @XmlElement
    private String open_lights_time;
    private boolean open_lights_time_bool = false;
    @XmlElement
    private String close_lights_time;
    private boolean close_lights_time_bool = false;
    private boolean state ;

    public Auto_Controler_config() {
    }

    public Auto_Controler_config(int device_ttl, boolean device_ttl_bool,
            double critical_temprature_max, boolean critical_temprature_max_bool,
            double critical_temprature_min, boolean critical_temprature_min_bool,
            double critical_humidity_max, boolean critical_humidity_max_bool,
            double critical_humidity_min, boolean critical_humidity_min_bool) {
        this.setCritical_humidity_max(critical_humidity_max);
        this.setCritical_humidity_max_bool(critical_humidity_max_bool);
        this.setCritical_humidity_min(critical_humidity_min);
        this.setCritical_humidity_min_bool(critical_humidity_min_bool);
        this.setCritical_temprature_max(critical_temprature_max);
        this.setCritical_temprature_max_bool(critical_temprature_max_bool);
        this.setCritical_temprature_min(critical_temprature_min);
        this.setCritical_temprature_min_bool(critical_temprature_min_bool);
        this.setDevice_ttl(device_ttl);
        this.setDevice_ttl_bool(device_ttl_bool);
        this.setState(state);
    }
      public Auto_Controler_config(String start_date, String finish_date, int device_ttl, boolean device_ttl_bool,
            double critical_temprature_max, boolean critical_temprature_max_bool,
            double critical_temprature_min, boolean critical_temprature_min_bool,
            double critical_humidity_max, boolean critical_humidity_max_bool,
            double critical_humidity_min, boolean critical_humidity_min_bool , boolean state) {
        this.setCritical_humidity_max(critical_humidity_max);
        this.setCritical_humidity_max_bool(critical_humidity_max_bool);
        this.setCritical_humidity_min(critical_humidity_min);
        this.setCritical_humidity_min_bool(critical_humidity_min_bool);
        this.setCritical_temprature_max(critical_temprature_max);
        this.setCritical_temprature_max_bool(critical_temprature_max_bool);
        this.setCritical_temprature_min(critical_temprature_min);
        this.setCritical_temprature_min_bool(critical_temprature_min_bool);
        this.setDevice_ttl(device_ttl);
        this.setDevice_ttl_bool(device_ttl_bool);
        this.setState(state);
        this.setOpen_lights_time(start_date);
        this.setOpen_lights_time_bool(true);
        this.setClose_lights_time(finish_date);
        this.setClose_lights_time_bool(true);
    }


    public Auto_Controler_config(String start_date, String finish_date, int device_ttl, boolean device_ttl_bool,
            double critical_temprature_max, boolean critical_temprature_max_bool,
            double critical_temprature_min, boolean critical_temprature_min_bool,
            double critical_humidity_max, boolean critical_humidity_max_bool,
            double critical_humidity_min, boolean critical_humidity_min_bool) {
        this.setCritical_humidity_max(critical_humidity_max);
        this.setCritical_humidity_max_bool(critical_humidity_max_bool);
        this.setCritical_humidity_min(critical_humidity_min);
        this.setCritical_humidity_min_bool(critical_humidity_min_bool);
        this.setCritical_temprature_max(critical_temprature_max);
        this.setCritical_temprature_max_bool(critical_temprature_max_bool);
        this.setCritical_temprature_min(critical_temprature_min);
        this.setCritical_temprature_min_bool(critical_temprature_min_bool);
        this.setDevice_ttl(device_ttl);
        this.setDevice_ttl_bool(device_ttl_bool);
        this.setState(state);
        this.setOpen_lights_time(start_date);
        this.setOpen_lights_time_bool(true);
        this.setClose_lights_time(finish_date);
        this.setClose_lights_time_bool(true);
    }

    public String getOpen_lights_time() {
        return open_lights_time;
    }

    public String getClose_lights_time() {
        return close_lights_time;
    }

    public void setOpen_lights_time_bool(boolean open_lights_time_bool) {
        this.open_lights_time_bool = open_lights_time_bool;
    }

    public void setClose_lights_time_bool(boolean close_lights_time_bool) {
        this.close_lights_time_bool = close_lights_time_bool;
    }

    public boolean isOpen_lights_time_bool() {
        return open_lights_time_bool;
    }

    public boolean isClose_lights_time_bool() {
        return close_lights_time_bool;
    }

    public void setOpen_lights_time(String open_lights_time) {
        this.open_lights_time = open_lights_time;
        this.open_lights_time_bool = true;
    }

    public void setClose_lights_time(String close_lights_time) {
        this.close_lights_time = close_lights_time;
        this.close_lights_time_bool = true;
    }

    public void setDevice_ttl(int device_ttl) {
        this.device_ttl = device_ttl;
        this.device_ttl_bool = true;
    }

    public void setCritical_temprature_max(double critical_temprature_max) {
        this.critical_temprature_max = critical_temprature_max;
        this.critical_temprature_max_bool = true;
    }

    public void setCritical_temprature_min(double critical_temprature_min) {
        this.critical_temprature_min = critical_temprature_min;
        this.critical_temprature_min_bool = true;
    }

    public void setCritical_humidity_max(double critical_humidity_max) {
        this.critical_humidity_max = critical_humidity_max;
        this.critical_humidity_max_bool = true;
    }

    public void setCritical_humidity_min(double critical_humidity_min) {
        this.critical_humidity_min = critical_humidity_min;
        this.critical_humidity_min_bool = true;
    }

    public int getDevice_ttl() {
        return device_ttl;
    }

    public double getCritical_temprature_max() {
        return critical_temprature_max;
    }

    public double getCritical_temprature_min() {
        return critical_temprature_min;
    }

    public double getCritical_humidity_max() {
        return critical_humidity_max;
    }

    public double getCritical_humidity_min() {
        return critical_humidity_min;
    }

    public boolean isDevice_ttl_bool() {
        return device_ttl_bool;
    }

    public boolean isCritical_temprature_max_bool() {
        return critical_temprature_max_bool;
    }

    public boolean isCritical_temprature_min_bool() {
        return critical_temprature_min_bool;
    }

    public boolean isCritical_humidity_max_bool() {
        return critical_humidity_max_bool;
    }

    public boolean isCritical_humidity_min_bool() {
        return critical_humidity_min_bool;
    }

    public boolean isState() {
        return state;
    }

    public void setDevice_ttl_bool(boolean device_ttl_bool) {
        this.device_ttl_bool = device_ttl_bool;
    }

    public void setCritical_temprature_max_bool(boolean critical_temprature_max_bool) {
        this.critical_temprature_max_bool = critical_temprature_max_bool;
    }

    public void setCritical_temprature_min_bool(boolean critical_temprature_min_bool) {
        this.critical_temprature_min_bool = critical_temprature_min_bool;
    }

    public void setCritical_humidity_max_bool(boolean critical_humidity_max_bool) {
        this.critical_humidity_max_bool = critical_humidity_max_bool;
    }

    public void setCritical_humidity_min_bool(boolean critical_humidity_min_bool) {
        this.critical_humidity_min_bool = critical_humidity_min_bool;
    }

    public void setState(boolean state) {
        this.state = state;
    }

}
