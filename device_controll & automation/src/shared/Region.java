/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared;

/**
 *
 * @author onelove
 */
public enum Region {
    LIVING_ROOM, KITCHEN, BEDROOM;

    public static Region decodeRegion(String region) {
        if (region.equalsIgnoreCase("living_room")) {
            return Region.LIVING_ROOM;
        } else if (region.equalsIgnoreCase("kitchen")) {
            return Region.KITCHEN;
        } else if (region.equalsIgnoreCase("bedroom")) {
            return Region.BEDROOM;
        }
        return null;
    }

}
