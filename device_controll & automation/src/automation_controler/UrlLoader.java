/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automation_controler;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ghost
 */
public class UrlLoader {

    private final static String ip = "192.168.1.10";
    private final static int port = 8080;

    public static URL get_live_avg() {
        System.out.println("http://" + ip + ":" + port + "/cass_prod/sensors/live/getCurrAvg");
        try {
            return new URL("http://" + ip + ":" + port + "/cass_prod/sensors/live/getCurrAvg");
        } catch (MalformedURLException ex) {
            Logger.getLogger(UrlLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
