/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automation_controler;

import automation_controler.rest_client.Responce_Wraper;
import static automation_controler.rest_client.Rest_client.requestRest_api;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.Server;
import static server.Server.getAutomationControler;
import static server.Server.getExaust_controler;
import static server.Server.getLights_controler;
import static server.Server.getRadiator_controler;
import shared.Auto_Controler_config;
import shared.Region;

/**
 *
 * @author ghost
 */
public class AutomationControler extends Thread {

    private static int device_ttl = 30;
    private static double critical_temprature_max = 30.00;
    private static double critical_temprature_min = 23.00;
    private static double critical_humidity_max = 70.00;
    private static double critical_humidity_min = 30.00;
    private static boolean state = false;
    private static String open_lights = "22:00:00";
    private static String close_lights = "01:00:00";

    @Override
    public void run() {
        AutomationControler.state = true;
        try {
            startControler();
        } catch (InterruptedException ex) {
            Logger.getLogger(AutomationControler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void startControler() throws InterruptedException {
        Thread thisThread = Thread.currentThread();
        System.out.println("Automation Controler initialized...");
        do {
            Responce_Wraper responce = read_live_data();
            System.out.println(responce.getRecord_list().size());
            if (responce != null) {

                for (int i = 0; i < responce.getRecord_list().size(); i++) {
                    System.out.println(responce.getRecord_list().get(i).getSensor_id());
                    System.out.println(responce.getRecord_list().get(i).getRegion());
                    if (isTemprature(responce.getRecord_list().get(i).getSensor_id())) {
                        if (responce.getRecord_list().get(i).getAverage() > critical_temprature_max) {
                            //do somehting to lower the temprature opne exaust
                            System.out.println("Starting Exaust");
                            getExaust_controler().get(Region.decodeRegion(responce.getRecord_list().get(i).getRegion())).start_exaust();
                        } else if (responce.getRecord_list().get(i).getAverage() < critical_temprature_min) {
                            //open radiator
                            System.out.println("Starting Radiator");
                            getRadiator_controler().get(Region.decodeRegion(responce.getRecord_list().get(i).getRegion())).start_radiator();
                        }
                    } else if (isHumidity(responce.getRecord_list().get(i).getSensor_id())) {
                        if (responce.getRecord_list().get(i).getAverage() > critical_humidity_max) {
                            //do somehting to lower the temprature opne radiator
                            System.out.println("Starting Radiator");
                            Server.getRadiator_controler().get(Region.decodeRegion(responce.getRecord_list().get(i).getRegion())).start_radiator();
                        } else if (responce.getRecord_list().get(i).getAverage() < critical_humidity_min) {
                            System.out.println("Starting Exaust");
                            Server.getExaust_controler().get(Region.decodeRegion(responce.getRecord_list().get(i).getRegion())).start_exaust();
                        }
                    }

                }
            }
            checkLightsSchedule();
            Thread.sleep(30 * 1000);
        } while (thisThread.equals(getAutomationControler()));
    }

    public final static void checkLightsSchedule() {
        Date date = new Date();
        System.out.println(date.toString());
        if (date.after(getDatetime(open_lights))) {
            try {
                //open the lights
                getLights_controler().get(Region.BEDROOM).start_lights();
                getLights_controler().get(Region.KITCHEN).start_lights();
                getLights_controler().get(Region.LIVING_ROOM).start_lights();
            } catch (InterruptedException ex) {
                Logger.getLogger(AutomationControler.class.getName()).log(Level.SEVERE, null, ex);
            }
            return;
        }
        if (date.after(getDatetime(close_lights))) {
            try {
                //close lights
                getLights_controler().get(Region.BEDROOM).stop_lights();
                getLights_controler().get(Region.KITCHEN).stop_lights();
                getLights_controler().get(Region.LIVING_ROOM).stop_lights();
            } catch (InterruptedException ex) {
                Logger.getLogger(AutomationControler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static Date getDatetime(String temp) {
        Date date = new Date();
        String[] values = temp.split(":");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(values[0]));
        cal.set(Calendar.MINUTE, Integer.parseInt(values[1]));
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        System.out.println("for : " + temp + cal.getTime().toString());
        return cal.getTime();
    }

    public static boolean isState() {
        return state;
    }

    public static void setState(boolean state) {
        AutomationControler.state = state;
    }

    private boolean isTemprature(String sensor_id) {
        if (sensor_id.contains("DHT22")) {
            return true;
        }
        return false;
    }

    private boolean isHumidity(String sensor_id) {
        if (sensor_id.contains("AM2303")) {
            return true;
        }
        return false;
    }

    public static void setStartLights(String start) {
        open_lights = start;
    }

    public static void setStopLights(String close) {
        close_lights = close;
    }

    public static void setDevice_ttl(int device_ttl) {
        AutomationControler.device_ttl = device_ttl;
    }

    public static void setCritical_temprature_max(double critical_temprature_max) {
        AutomationControler.critical_temprature_max = critical_temprature_max;
    }

    public static void setCritical_temprature_min(double critical_temprature_min) {
        AutomationControler.critical_temprature_min = critical_temprature_min;
    }

    public static void setCritical_humidity_max(double critical_humidity_max) {
        AutomationControler.critical_humidity_max = critical_humidity_max;
    }

    public static void setCritical_humidity_min(double critical_humidity_min) {
        AutomationControler.critical_humidity_min = critical_humidity_min;
    }

    public static int getDevice_ttl() {
        return device_ttl;
    }

    public static double getCritical_temprature_max() {
        return critical_temprature_max;
    }

    public static double getCritical_temprature_min() {
        return critical_temprature_min;
    }

    public static double getCritical_humidity_max() {
        return critical_humidity_max;
    }

    public static double getCritical_humidity_min() {
        return critical_humidity_min;
    }

    public static Responce_Wraper read_live_data() {
        System.out.println("READ LIVE DATA");
        return requestRest_api(UrlLoader.get_live_avg());
    }

    public static void configControler(Auto_Controler_config config) {

        critical_humidity_max = config.getCritical_humidity_max();
        critical_humidity_min = config.getCritical_humidity_min();
        critical_temprature_max = config.getCritical_temprature_max();
        critical_temprature_min = config.getCritical_temprature_min();
        if (config.getOpen_lights_time() != null) {
            open_lights = config.getOpen_lights_time();
        }
        if (config.getClose_lights_time() != null) {
            close_lights = config.getClose_lights_time();
        }
    }

    public static Auto_Controler_config extract_controller_configuration() {
        return new Auto_Controler_config(open_lights, close_lights,
                device_ttl, true,
                critical_temprature_max, true,
                critical_temprature_min, true,
                critical_humidity_max, true,
                critical_humidity_min, true, state);
    }

}
