package server;

import automation_controler.AutomationControler;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import static rasp_controll.Controler.closeControler;
import static rasp_controll.Controler.startControler;
import rasp_controll.Exaust_controler;
import rasp_controll.LightsControler;
import rasp_controll.Radiator_controler;
import shared.Region;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author onelove
 */
public class Server {
    
    private static final int port = 1978;
    private static final String hostname = "192.168.1.11";
    
    public static void main(String args[]) {
        ServerSocket serverSocket = null;
        Socket socket = null;
        startControler();
        initializeControlers();
        try {
            serverSocket = new ServerSocket(port, 50, InetAddress.getByName(hostname));
            System.out.println("Server is up and running at port :: " + port);
        } catch (IOException e) {
            e.printStackTrace();
            
        }
        while (true) {
            try {
                socket = serverSocket.accept();
            } catch (IOException e) {
                closeControler();
                System.out.println("I/O error: " + e);
            }
            // new threa for a client
            new EchoThread(socket).start();
        }
    }
    
    public static Map<Region, Radiator_controler> radiator_controler;
    private static Map<Region, Exaust_controler> exaust_controler;
    private static Map<Region, LightsControler> lights_controler;
    protected static volatile AutomationControler auto_controler;
    
    public static Map<Region, Radiator_controler> getRadiator_controler() {
        return radiator_controler;
    }
    
    public static Map<Region, Exaust_controler> getExaust_controler() {
        return exaust_controler;
    }
    
    public static Map<Region, LightsControler> getLights_controler() {
        return lights_controler;
    }
    
    public static AutomationControler getAutomationControler() {
        return auto_controler;
    }
    
    public static void startAutomation() {
        auto_controler = new AutomationControler();
        auto_controler.start();
        AutomationControler.setState(true);
    }
    
    public static void stoptAutomation() {
        auto_controler = null;
        AutomationControler.setState(false);
    }
    
    private static void initializeControlers() {
        System.out.println("Starting initialization.....");
        radiator_controler = new HashMap<Region, Radiator_controler>();
        radiator_controler.put(Region.KITCHEN, new Radiator_controler(Region.KITCHEN, "KITCHEN_Radiator"));
        radiator_controler.put(Region.BEDROOM, new Radiator_controler(Region.BEDROOM, "BEDROOM_Radiator"));
        radiator_controler.put(Region.LIVING_ROOM, new Radiator_controler(Region.LIVING_ROOM, "LIVING_ROOM_Radiator"));
        
        exaust_controler = new HashMap<Region, Exaust_controler>();
        exaust_controler.put(Region.KITCHEN, new Exaust_controler(Region.KITCHEN, "KITCHEN_Exaust"));
        exaust_controler.put(Region.BEDROOM, new Exaust_controler(Region.BEDROOM, "BEDROOM_Exaust"));
        exaust_controler.put(Region.LIVING_ROOM, new Exaust_controler(Region.LIVING_ROOM, "LIVING_ROOM_Exaust"));
        
        lights_controler = new HashMap<Region, LightsControler>();
        lights_controler.put(Region.KITCHEN, new LightsControler(Region.KITCHEN, "KITCHEN_Lights"));
        lights_controler.put(Region.BEDROOM, new LightsControler(Region.BEDROOM, "BEDROOM_Lights"));
        lights_controler.put(Region.LIVING_ROOM, new LightsControler(Region.LIVING_ROOM, "LIVING_ROOM_Lights"));
        System.out.println("Controlers initialized");
    }
    
    public static void Radiator_tongleTTL(Region region, int device_ttl) throws InterruptedException {
        getRadiator_controler().get(region).start_for_spacific_time_radiator(device_ttl);
    }
}
