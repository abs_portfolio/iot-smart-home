/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rasp_controll;

import com.google.gson.Gson;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import java.util.HashMap;
import java.util.Map;
import static rasp_controll.Controler.createDigitalOutPut_fromPin;
import static rasp_controll.Controler.enablePin;
import static rasp_controll.Controler.enablePin_ForSpacificTime;
import static rasp_controll.Controler.shutDownPin;
import shared.Region;

/**
 *
 * @author onelove
 */
public class Exaust_controler {

    private final static Gson jsonfy = new Gson();
    private final static Pin[] avalable_pins = {RaspiPin.GPIO_01, RaspiPin.GPIO_05, RaspiPin.GPIO_06};
    private static Map<Region, Pin> set;

    static {
        set = new HashMap<Region, Pin>();
        set.put(Region.KITCHEN, avalable_pins[0]);
        set.put(Region.BEDROOM, avalable_pins[1]);
        set.put(Region.LIVING_ROOM, avalable_pins[2]);
    }

    public final MyPinState getPinState() throws InterruptedException {
        return new MyPinState(Controler.checkPinState(Controler.getPinList().get(name)), name);
    }

    private static final Pin getNextAvailablePin() {
        Pin temp = null;
        for (int i = 0; i < avalable_pins.length; i++) {
            if (avalable_pins[i] != null) {
                temp = avalable_pins[i];
                avalable_pins[i] = null;
            } else {
                continue;
            }

        }
        return temp;
    }

    @Override
    public String toString() {
        return jsonfy.toJson(this);
    }

    private static final Pin givePinBack(Pin pin) {
        Pin temp = null;
        for (int i = 0; i < avalable_pins.length; i++) {
            if (avalable_pins[i] == null) {
                avalable_pins[i] = pin;
            } else {
                continue;
            }

        }
        return temp;
    }

    private boolean state;
    private Region region;
    private String name;
    private Pin cuuPin;

    public Region getRegion() {
        return region;
    }

    public Exaust_controler(boolean state, Region region, String name) {
        this.state = state;
        this.region = region;
        this.name = name;
        this.cuuPin = getNextAvailablePin();
        createDigitalOutPut_fromPin(getCuuPin(), name, PinState.LOW);
    }

    public Exaust_controler(Pin pin, Region region, String name) {
        this.state = state;
        this.region = region;
        this.name = name;
        this.cuuPin = pin;
        createDigitalOutPut_fromPin(getCuuPin(), name, PinState.LOW);
    }

    public Exaust_controler(Region region, String name) {
        this.state = false;
        this.region = region;
        this.name = name;
        this.cuuPin = set.get(region);
        createDigitalOutPut_fromPin(getCuuPin(), name, PinState.LOW);
    }

    public Pin getCuuPin() {
        return cuuPin;
    }

    public void start_exaust() throws InterruptedException {
        if (!state) {
            enablePin(Controler.getPinList().get(name));
            this.state = true;
        } else {
            System.err.println("The module is already up...");
        }
    }

    public void stop_exaust() throws InterruptedException {
        if (state) {
            shutDownPin(Controler.getPinList().get(name));
            this.state = false;
        } else {
            System.err.println("The module is already dwun...");
        }
    }

    public final void deleteExaust() {
        Controler.unregisterPin(getCuuPin().getName());
        givePinBack(getCuuPin());

    }

    public void start_for_spacific_time_exaust(int time) throws InterruptedException {
        if (!state) {
            enablePin_ForSpacificTime(Controler.getPinList().get(name), time);
        } else {
            System.err.println("The module is already up...");
        }
    }

}
