/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rasp_controll;

import com.pi4j.io.gpio.PinState;

/**
 *
 * @author onelove
 */
public class MyPinState {
    private PinState state ;
    private String name ;

    public void setState(PinState state) {
        this.state = state;
    }

    public MyPinState() {
    }

    public MyPinState(PinState state, String name) {
        this.state = state;
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PinState getState() {
        return state;
    }

    public String getName() {
        return name;
    }
}
