
# Configuration

# DHT           ->  5V,     pin 7 (GPIO4)
# BMP           ->  3.3V,   pin 3+5 (SDA I2C & SCL I2C)
# Photocell     ->  3.3V,   pin 11 (GPIO17)
# MQ-7			->	5V,		pin 13 (GPIO27)
#
#

import pigpio
import DHT22
import smbus
import time
from time import sleep
import RPi.GPIO as GPIO

######################
######## DHT #########
######################

# Initiate GPIO for pigpio
pi = pigpio.pi()

# Setup the sensor
dht22 = DHT22.sensor(pi, 4) # use the actual GPIO pin name
#dht22.trigger()

# We want out sleep time to be above 2 seconds - meeeeh, 0.1 is fine too (as tested)
sleepTime = 0.1

def readDHT22() :

	sleep(sleepTime)

	# Get a new reading
	dht22.trigger()

	# Save our values
	humidity = '%.1f' % (dht22.humidity())
	temp = '%.1f' % (dht22.temperature())
	return (humidity, temp)


humidity, temperature = readDHT22()     # First calibrating reading (999.0)
humidity, temperature = readDHT22()     # Second reading (true values)

######################
######## BMP #########
######################

# bus = smbus.SMBus(1)

# BMP180 address, 0x77
# data = bus.read_i2c_block_data(0x77, 0xAA, 22)

# Convert the data
# AC1 = data[0] * 256 + data[1]
# if AC1 > 32767 :
# 	AC1 -= 65535
# AC2 = data[2] * 256 + data[3]
# if AC2 > 32767 :
# 	AC2 -= 65535
# AC3 = data[4] * 256 + data[5]
# if AC3 > 32767 :
# 	AC3 -= 65535
# AC4 = data[6] * 256 + data[7]
# AC5 = data[8] * 256 + data[9]
# AC6 = data[10] * 256 + data[11]
# B1 = data[12] * 256 + data[13]
# if B1 > 32767 :
# 	B1 -= 65535
# B2 = data[14] * 256 + data[15]
# if B2 > 32767 :
# 	B2 -= 65535
# MB = data[16] * 256 + data[17]
# if MB > 32767 :
# 	MB -= 65535
# MC = data[18] * 256 + data[19]
# if MC > 32767 :
# 	MC -= 65535
# MD = data[20] * 256 + data[21]
# if MD > 32767 :
# 	MD -= 65535
#
# time.sleep(0.5)
#
# bus.write_byte_data(0x77, 0xF4, 0x2E)
# time.sleep(0.5)
# data = bus.read_i2c_block_data(0x77, 0xF6, 2)

# Convert the data
# temp = data[0] * 256 + data[1]
#
# bus.write_byte_data(0x77, 0xF4, 0x74)
#
# time.sleep(0.5)
#
# data = bus.read_i2c_block_data(0x77, 0xF6, 3)

# Convert the data
# pres = ((data[0] * 65536) + (data[1] * 256) + data[2]) / 128

# Callibration for Temperature
# X1 = (temp - AC6) * AC5 / 32768.0
# X2 = (MC * 2048.0) / (X1 + MD)
# B5 = X1 + X2
# cTemp = ((B5 + 8.0) / 16.0) / 10.0
# fTemp = cTemp * 1.8 + 32

# Calibration for Pressure
# B6 = B5 - 4000
# X1 = (B2 * (B6 * B6 / 4096.0)) / 2048.0
# X2 = AC2 * B6 / 2048.0
# X3 = X1 + X2
# B3 = (((AC1 * 4 + X3) * 2) + 2) / 4.0
# X1 = AC3 * B6 / 8192.0
# X2 = (B1 * (B6 * B6 / 2048.0)) / 65536.0
# X3 = ((X1 + X2) + 2) / 4.0
# B4 = AC4 * (X3 + 32768) / 32768.0
# B7 = ((pres - B3) * (25000.0))
# pressure = 0.0
# if B7 < 2147483648L :
# 	pressure = (B7 * 2) / B4
# else :
# 	pressure = (B7 / B4) * 2
# X1 = (pressure / 256.0) * (pressure / 256.0)
# X1 = (X1 * 3038.0) / 65536.0
# X2 = ((-7357) * pressure) / 65536.0
# pressure = (pressure + (X1 + X2 + 3791) / 16.0) / 100

# Calculate Altitude
# altitude = 44330 * (1 - ((pressure / 1013.25) ** 0.1903))

######################
###### Photocell #####
######################

GPIO.setmode(GPIO.BCM)
GPIO.setup(17,GPIO.IN)

######################
######## MQ-7 ########
######################

#GPIO.setmode(GPIO.BOARD)
#GPIO.setup(7, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
#
#def action(pin):
    #print('Sensor detected action!')
    #return
#
#GPIO.add_event_detect(7, GPIO.RISING)
#GPIO.add_event_callback(7, action)
#
#try:
    #while True:
        #print('alive')
        #time.sleep(0.5)
#except KeyboardInterrupt:
    #GPIO.cleanup()
    #sys.exit()

GPIO.setmode(GPIO.BCM)
GPIO.setup(27,GPIO.IN)







# Output data to screen
print(" ")
print("DHT22/AM2302 readings")
print("=====================")
print("     Humidity is: " + humidity + "%")
print("     Temperature is: " + temperature + "C")

print(" ")

# print("BMP180 readings")
# print("===============")
# print("     Altitude: %.2f m" %altitude)
# print("     Pressure: %.2f hPa " %pressure)
# print("     Temperature in Celsius: %.2f C" %cTemp)
# print("     Temperature in Fahrenheit: %.2f F" %fTemp)
#
# print(" ")

print("Photocell readings")
print("==================")
num1 = GPIO.input(17)
print("     State: %d" %num1)

print(" ")

print("MQ-7 readings")
print("==================")
num2 = GPIO.input(27)
print("     State: %d" %num2)
print(" ")
