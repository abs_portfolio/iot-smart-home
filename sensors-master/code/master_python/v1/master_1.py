import pigpio
import DHT22
import smbus
import time
from time import sleep
import RPi.GPIO as GPIO

######################
######## DHT #########
######################

# Initiate GPIO for pigpio
pi = pigpio.pi()

# Setup the sensor
dht22 = DHT22.sensor(pi, 4) # use the actual GPIO pin name
#dht22.trigger()

# We want out sleep time to be above 2 seconds - meeeeh, 0.1 is fine too (as tested)
sleepTime = 0.1

def readDHT22() :

	sleep(sleepTime)

	# Get a new reading
	dht22.trigger()

	# Save our values
	humidity = '%.1f' % (dht22.humidity())
	temp = '%.1f' % (dht22.temperature())
	return (humidity, temp)


humidity, temperature = readDHT22()     # First calibrating reading (999.0)
humidity, temperature = readDHT22()     # Second reading (true values)

######################
###### Photocell #####
######################

GPIO.setmode(GPIO.BCM)
GPIO.setup(17,GPIO.IN)

######################
######## MQ-7 ########
######################

GPIO.setmode(GPIO.BCM)
GPIO.setup(27,GPIO.IN)

# Output data to screen
print(" ")

print("AM2302|" + humidity)
print("DHT22|" + temperature)
num1 = GPIO.input(17)
print("Photocell|%d" %num1)
num2 = GPIO.input(27)
print("MQ-7|%d" %num2)
