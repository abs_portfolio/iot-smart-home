import pigpio
import DHT22
from time import sleep

# Initiate GPIO for pigpio
pi = pigpio.pi()

# Setup the sensor
dht22 = DHT22.sensor(pi, 4) # use the actual GPIO pin name
#dht22.trigger()

# We want out sleep time to be above 2 seconds - meeeeh, 0.1 is fine too (as tested)
sleepTime = 0.1

def readDHT22() :

	sleep(sleepTime)

	# Get a new reading
	dht22.trigger()

	# Save our values
	humidity = '%.1f' % (dht22.humidity())
	temp = '%.1f' % (dht22.temperature())
	return (humidity, temp)


humidity, temperature = readDHT22()
humidity, temperature = readDHT22()
print("Humidity is: " + humidity + "%")
print("Temperature is: " + temperature + "C")
