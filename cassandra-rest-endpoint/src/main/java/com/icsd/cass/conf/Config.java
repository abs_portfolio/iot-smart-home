/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.conf;

import com.icsd.cass.cassandra_drivers.Connector;
import static com.icsd.cass.cassandra_drivers.Connector.generateSession;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 *
 * @author onelove
 */
@WebListener
public class Config implements ServletContextListener {

    private static final ArrayList<String> clusters;
    private static final String keyspace = "sensors";

    static {
        clusters = new ArrayList<String>();
//        clusters.add("192.168.56.101");
        clusters.add("185.244.128.27"); //localhost
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {
        System.out.println("INITIALIAZINGGGGG>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        try {
            Connector.initializeCluster(clusters);
            Connector.generateSession(keyspace);
        } catch (UnknownHostException ex) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        // Do stuff during webapp shutdown.
        System.out.println("CLOSINGGGGGG>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        Connector.terminateSession();
        Connector.closeClusterConnection();

    }

}
