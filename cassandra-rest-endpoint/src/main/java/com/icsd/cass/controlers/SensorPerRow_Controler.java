/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.controlers;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.icsd.cass.controlers.interfaces.SensorPerRowInterface;
import com.icsd.cass.models.SensorPerRow;
import com.icsd.cass.cassandra_drivers.Connector;
import com.icsd.cass.system_static.templates.ChartData;
import java.util.ArrayList;
import java.util.List;
import com.icsd.cass.system_static.templates.DataForm;
import com.icsd.cass.system_static.templates.GassWraper;
import com.icsd.cass.system_static.templates.ResponceWraper;
import com.icsd.cass.system_static.templates.Responce_Wraper;
import com.icsd.cass.system_static.templates.Search_Template;
import com.icsd.cass.system_static.templates.Sensor_Template;
import com.icsd.cass.system_static.templates.SystemTemplate;
import java.util.HashMap;

/**
 *
 * @author onelove
 */
public class SensorPerRow_Controler {

    private final static String table_name = "sensor_per_row";
    private SensorPerRowInterface sensors_accesor = null;

    public SensorPerRow_Controler() {
        sensors_accesor = Connector.getManager().createAccessor(SensorPerRowInterface.class);
    }

    public List<SensorPerRow> getAllRecords() {
        return sensors_accesor.getAll().all();
    }

    public ResponceWraper getAll_average_Records() {
        return new ResponceWraper(sensors_accesor.get_avg_all(), true);
    }

    public Responce_Wraper getAll_minimum_Records() {
        return new Responce_Wraper(DataForm.ChartData, sensors_accesor.get_min_all());
    }

    public Responce_Wraper getAll_maximum_Records() {
        return new Responce_Wraper(DataForm.ChartData, sensors_accesor.get_max_all());
    }

    public Responce_Wraper get_avg_temp_overal() {
        return new Responce_Wraper(DataForm.ChartData, sensors_accesor.get_avg_temp_overal("DHT22"));
    }

    public Responce_Wraper get_all_sensors() {
        return new Responce_Wraper(sensors_accesor.get_all_sensors(), true);
    }

    public GassWraper get_all_gasses() {
        return new GassWraper(sensors_accesor.get_avg_all());
    }

    public ResponceWraper return_average_between_ts_grooped(long start, long finish) {
        return new ResponceWraper(sensors_accesor.return_average_between_ts_grooped(start, finish), true);
    }

    public ResponceWraper return_min_between_ts_grooped(long start, long finish) {
        return new ResponceWraper(sensors_accesor.return_min_between_ts_grooped(start, finish), true);
    }

    public ResponceWraper return_max_between_ts_grooped(long start, long finish) {
        return new ResponceWraper(sensors_accesor.return_max_between_ts_grooped(start, finish), true);
    }

    public void setSensors_accesor(SensorPerRowInterface sensors_accesor) {
        this.sensors_accesor = sensors_accesor;
    }

    public static String getTabble_name() {
        return table_name;
    }

    public Responce_Wraper searchNVisualize_overal(Search_Template template) {
        return decode_request_overal(template);
    }

    public SensorPerRowInterface getSensors_accesor() {
        return sensors_accesor;
    }

    public ArrayList getSysInfo() {
        return decodeSystemData(sensors_accesor.getSystemInfo());
    }

    private Responce_Wraper decode_request_overal(Search_Template template) {
        switch (template.getFunct()) {
            case "avg": {
                ResponceWraper tmp = return_average_between_ts_grooped(template.getStart(), template.getFinish());
                return exclude_values(template.getSensor_list(), tmp);
            }
            case "min": {
                ResponceWraper tmp = return_min_between_ts_grooped(template.getStart(), template.getFinish());
                return exclude_values(template.getSensor_list(), tmp);
            }
            case "max": {
                ResponceWraper tmp = return_max_between_ts_grooped(template.getStart(), template.getFinish());
                return exclude_values(template.getSensor_list(), tmp);
            }
        }
        return null;
    }

    private Responce_Wraper exclude_values(List<String> keep, ResponceWraper resp) {
        Responce_Wraper temo = new Responce_Wraper();
        HashMap<String, ChartData> data = new HashMap<>();
        for (int i = 0; i < keep.size(); i++) {
            data.put(keep.get(i), resp.getChart_data().get(keep.get(i)));
        }
        temo.setChart_data(data);
        temo.setDateList(new ArrayList<>(resp.getDateList()));
        return temo;
    }

    private ArrayList decodeSystemData(ResultSet set) {
        ArrayList record_list = new ArrayList();
        for (Row row : set) {
            record_list.add(new SystemTemplate(
                    row.getString("key"),
                    row.getString("bootstrapped"),
                    row.getInet("broadcast_address").getHostAddress(),
                    row.getString("cluster_name"),
                    row.getString("cql_version"),
                    row.getString("data_center"),
                    row.getInet("listen_address").getHostAddress(),
                    row.getInet("rpc_address").getHostAddress(),
                    row.getString("thrift_version")
            ));

        }
        return record_list;
    }

    public void generate_requested_data(Search_Template json) {

    }

}
