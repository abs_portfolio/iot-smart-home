/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.controlers.interfaces;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Param;
import com.datastax.driver.mapping.annotations.Query;
import com.icsd.cass.models.SensorPerMinute;

/**
 *
 * @author onelove
 */
@Accessor
public interface SensorPerMinuteInterface {

    @Query("SELECT * FROM sensor_per_minute")
    public Result<SensorPerMinute> getAll();

    @Query("select count(*) as mesurments, sensor_id , date , region , model , AVG(value) as avg from sensor_per_minute GROUP BY sensor_id , date ;")
    public ResultSet get_avg_curr();

//    @Query("select * from sensor_per_minute where sensro_id= :sensor ALLOW FILTERING;")
//    public Result<SensorPerMinute> get_sensor_raw(@Param("sensor") String sensor);
    @Query("select count(*) as mesurments, sensor_id , date , region , model , AVG(value) as avg from sensor_per_minute where model = :model GROUP BY sensor_id , date ALLOW FILTERING;")
    public ResultSet get_avg_by_model_live(@Param("model") String model);

    @Query("select count(*) as mesurments, sensor_id , date , region , model , AVG(value) as avg, ts from sensor_per_minute where sensor_id = :sensor_id GROUP BY sensor_id , date ALLOW FILTERING;")
    public ResultSet get_avg_by_sensor_id_live(@Param("sensor_id") String sensor_id);

    @Query("select count(*) as mesurments, sensor_id , model ,region  from sensor_per_minute GROUP BY sensor_id , date ;")
    public ResultSet get_all_sensors();

    @Query("select count(*) as mesurments, sensor_id , date , region , model , max(value) as avg from sensor_per_minute GROUP BY sensor_id , date ;")
    public ResultSet get_max_curr();

    @Query("select count(*) as mesurments, sensor_id , date , region , model , min(value) as avg from sensor_per_minute GROUP BY sensor_id , date ;")
    public ResultSet get_min_curr();

    @Query("select count(*) as mesurments, sensor_id , date , region , model , max(value) as avg from sensor_per_minute where sensor_id = :sensor_id GROUP BY sensor_id , date ALLOW FILTERING;")
    public ResultSet get_max_by_sensor_id_live(@Param("sensor_id") String sensor_id);

    @Query("select count(*) as mesurments, sensor_id , date , region , model , min(value) as avg from sensor_per_minute where sensor_id = :sensor_id GROUP BY sensor_id , date ALLOW FILTERING;")
    public ResultSet get_min_by_sensor_id_live(@Param("sensor_id") String sensor_id);
   

}
