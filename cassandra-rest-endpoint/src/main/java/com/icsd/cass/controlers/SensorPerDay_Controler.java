/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.controlers;

import com.icsd.cass.controlers.interfaces.SensorPerDayInterface;
import com.icsd.cass.controlers.interfaces.SensorPerMinuteInterface;
import com.icsd.cass.models.SensorPerDay;
import com.icsd.cass.models.SensorPerMinute;
import com.icsd.cass.cassandra_drivers.Connector;
import com.icsd.cass.system_static.templates.ChartData;
import com.icsd.cass.system_static.templates.DataForm;
import java.util.List;
import com.icsd.cass.system_static.templates.Responce_Wraper;
import com.icsd.cass.system_static.templates.Search_Template;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author onelove
 */
public class SensorPerDay_Controler {

    private final static String table_name = "sensor_per_date";
    private Connector conn = null;
    private SensorPerDayInterface sensors_accesor = null;

    public SensorPerDay_Controler() {
        conn = new Connector();
        sensors_accesor = Connector.getManager().createAccessor(SensorPerDayInterface.class);
    }

    public List<SensorPerDay> getAllRecords() {
        return sensors_accesor.getAllDay().all();
    }

    public Responce_Wraper get_Currnet_average_Records() {
        return new Responce_Wraper(sensors_accesor.get_day_avg_());
    }

    public Responce_Wraper get_day_max_() {
        return new Responce_Wraper(sensors_accesor.get_day_max_());
    }

    public Responce_Wraper return_average_between_ts_grooped_daily(String start, String finish) {
        return new Responce_Wraper(DataForm.ChartData, sensors_accesor.return_average_between_ts_grooped(start, finish));
    }

    public Responce_Wraper return_min_between_ts_grooped_daily(String start, String finish) {
        return new Responce_Wraper(DataForm.ChartData, sensors_accesor.return_min_between_ts_grooped(start, finish));
    }

    public Responce_Wraper return_max_between_ts_grooped_daily(String start, String finish) {
        return new Responce_Wraper(DataForm.ChartData, sensors_accesor.return_max_between_ts_grooped(start, finish));
    }

    public Responce_Wraper searchNVisualize_daily_s(Search_Template template) {
        return decode_request_daily(template);
    }

    private Responce_Wraper decode_request_daily(Search_Template template) {
        switch (template.getFunct()) {
            case "avg": {
                Responce_Wraper tmp = return_average_between_ts_grooped_daily(template.getDate_start(), template.getDate_finish());
                return exclude_values_daily(template.getSensor_list(), tmp);
            }
            case "min": {
                Responce_Wraper tmp = return_min_between_ts_grooped_daily(template.getDate_start(), template.getDate_finish());
                return exclude_values_daily(template.getSensor_list(), tmp);
            }
            case "max": {
                Responce_Wraper tmp = return_max_between_ts_grooped_daily(template.getDate_start(), template.getDate_finish());
                return exclude_values_daily(template.getSensor_list(), tmp);
            }
        }
        return null;
    }

    private Responce_Wraper exclude_values_daily(List<String> keep, Responce_Wraper resp) {
        Responce_Wraper temo = new Responce_Wraper();
        HashMap<String, ChartData> data = new HashMap<>();
        for (int i = 0; i < keep.size(); i++) {
            data.put(keep.get(i), resp.getChart_data().get(keep.get(i)));
        }
        temo.setChart_data(data);
        temo.setDateList(new ArrayList<>(resp.getDateList()));
        return temo;
    }

//    public Responce_Wraper getAvgBySensorId(String sensor_id) {
//        return new Responce_Wraper(sensors_accesor.getAvgBySensorId(sensor_id));
//    }
//    public void deleteBySensorId(String sensor_id, String date) {
//        sensors_accesor.delteBySensorId(sensor_id, date);
//    }

    public Responce_Wraper get_all_sensors() {
        return new Responce_Wraper(sensors_accesor.get_all_sensors(), true);
    }

    public void setConn(Connector conn) {
        this.conn = conn;
    }

    public void setSensors_accesor(SensorPerDayInterface sensors_accesor) {
        this.sensors_accesor = sensors_accesor;
    }

    public static String getTabble_name() {
        return table_name;
    }

    public Connector getConn() {
        return conn;
    }

    public SensorPerDayInterface getSensors_accesor() {
        return sensors_accesor;
    }
}
