/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.controlers;

import com.icsd.cass.controlers.interfaces.SensorListInterface;
import com.icsd.cass.models.Sensor_List;
import com.icsd.cass.cassandra_drivers.Connector;
import java.util.List;
import com.icsd.cass.system_static.templates.Responce_Wraper;

/**
 *
 * @author onelove
 */
public class SensorList_Controler {

    private final static String table_name = "sensor_list";
    private Connector conn = null;
    private SensorListInterface sensors_accesor = null;

    public SensorList_Controler() {
        conn = new Connector();
        sensors_accesor = Connector.getManager().createAccessor(SensorListInterface.class);
    }
    //Invocated Methods

    public List<Sensor_List> getAllRecords() {
        return sensors_accesor.get_all_data().all();
    }

    public Responce_Wraper get_all_sensors() {
        return new Responce_Wraper(sensors_accesor.get_sensor_list(), true);
    }
    public static String getTable_name() {
        return table_name;
    }

    public Connector getConn() {
        return conn;
    }

    public SensorListInterface getSensors_accesor() {
        return sensors_accesor;
    }

    public void setConn(Connector conn) {
        this.conn = conn;
    }

    public void setSensors_accesor(SensorListInterface sensors_accesor) {
        this.sensors_accesor = sensors_accesor;
    }

}
