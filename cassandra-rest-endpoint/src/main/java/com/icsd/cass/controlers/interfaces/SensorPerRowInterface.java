/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.controlers.interfaces;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Param;
import com.datastax.driver.mapping.annotations.Query;
import com.icsd.cass.models.SensorPerRow;
import io.swagger.annotations.SwaggerDefinition;

/**
 *
 * @author onelove
 */
@Accessor
public interface SensorPerRowInterface {

    @Query("SELECT * FROM system.local")
    public ResultSet getSystemInfo();

    @Query("SELECT * FROM sensor_per_row")
    public Result<SensorPerRow> getAll();

    @Query("select count(*) as mesurments, sensor_id , date , region , model , AVG(value) as avg from sensor_per_row GROUP BY sensor_id , date ;")
    public ResultSet get_avg_all();
    @Query("select count(*) as mesurments, sensor_id , date , region , model , max(value) as avg from sensor_per_row GROUP BY sensor_id , date ;")
    public ResultSet get_max_all();
    @Query("select count(*) as mesurments, sensor_id , date , region , model , min(value) as avg from sensor_per_row GROUP BY sensor_id , date ;")
    public ResultSet get_min_all();

    @Query("select count(*) as mesurments, sensor_id , date , region , model , AVG(value) as avg from sensor_per_row where model = :model GROUP BY sensor_id , date ALLOW FILTERING;")
    public ResultSet get_avg_temp_overal(@Param("model") String model);

    @Query("select count(*) as mesurments, sensor_id , date , region , model , AVG(value) as avg from sensor_per_row where model = :model and region = :region GROUP BY sensor_id , date ALLOW FILTERING;")
    public ResultSet get_avg_temp_overal_region(@Param("model") String model, @Param("region") String region);

    @Query("select count(*) as mesurments, sensor_id , model,region  from sensor_per_row GROUP BY sensor_id , date ;")
    public ResultSet get_all_sensors();

    @Query("select count(*) as mesurments, sensor_id, date, avg(value) from sensor_per_row where date>= :start and date<= :finish GROUP BY sensor_id , date Allow Filtering;")
    public ResultSet extract_avg_between_dates(@Param("start") String start, @Param("finish") String finish);

    @Query("select count(*) as mesurments, sensor_id, date, min(value) from sensor_per_row where date>= :start and date<= :finish GROUP BY sensor_id , date Allow Filtering;")
    public ResultSet extract_min_between_dates(@Param("start") String start, @Param("finish") String finish);

    @Query("select count(*) as mesurments, sensor_id, date, max(value) from sensor_per_row where date>= :start and date<= :finish GROUP BY sensor_id , date Allow Filtering;")
    public ResultSet extract_max_between_dates(@Param("start") String start, @Param("finish") String finish);

    //restrict ts
    @Query("select count(*) as mesurments,region, model, sensor_id, date, AVG(value) as avg from sensor_per_row where ts>= :start and ts<= :finish GROUP BY sensor_id , date Allow Filtering;")
    public ResultSet return_average_between_ts_grooped(@Param("start") long start, @Param("finish") long finish);

    @Query("select count(*) as mesurments,region, model, sensor_id, date, MIN(value) as avg from sensor_per_row where ts>= :start and ts<= :finish GROUP BY sensor_id , date Allow Filtering;")
    public ResultSet return_min_between_ts_grooped(@Param("start") long start, @Param("finish") long finish);

    @Query("select count(*) as mesurments,region, model, sensor_id, date, MAX(value) as avg from sensor_per_row where ts>= :start and ts<= :finish GROUP BY sensor_id , date Allow Filtering;")
    public ResultSet return_max_between_ts_grooped(@Param("start") long start, @Param("finish") long finish);

    @Query("select sensor_id, date,ts,region, model, value as avg from sensor_per_row where ts>= :start and ts<= :finish Allow Filtering;")
    public ResultSet return_values_between_ts_grooped(@Param("start") long start, @Param("finish") long finish);

    //restrict ts and sensor_id
    @Query("select count(*) as mesurments, sensor_id, date,ts, region, model, AVG(value) as avg from sensor_per_row where ts>= :start and ts<= :finish and sensor_id = :sensor GROUP BY sensor_id , date Allow Filtering;")
    public ResultSet return_average_between_ts_grooped(@Param("sensor") String sensor, @Param("start") long start, @Param("finish") long finish);

    @Query("select count(*) as mesurments, sensor_id, date,ts, region, model, MIN(value) as avg from sensor_per_row where ts>= :start and ts<= :finish and sensor_id = :sensor GROUP BY sensor_id , date Allow Filtering;")
    public ResultSet return_min_between_ts_grooped(@Param("sensor") String sensor, @Param("start") long start, @Param("finish") long finish);

    @Query("select count(*) as mesurments, sensor_id, date,ts, region, model, MAX(value) as avg from sensor_per_row where ts>= :start and ts<= :finish and sensor_id = :sensor GROUP BY sensor_id , date Allow Filtering;")
    public ResultSet return_max_between_ts_grooped(@Param("sensor") String sensor, @Param("start") long start, @Param("finish") long finish);

    @Query("select sensor_id, date,ts,region, model, value as avg from sensor_per_row where ts>= :start and ts<= :finish and sensor_id = :sensor Allow Filtering;")
    public ResultSet return_values_between_ts_grooped(@Param("sensor") String sensor, @Param("start") long start, @Param("finish") long finish);

//    @Query("select count(*) as mesurments,  sensor_id , date , region , model , AVG(value) as avg from sensors.:table WHERE sensor_id = :sensor_id GROUP BY sensor_id , date ALLOW FILTERING;")
//    public ResultSet get_all_average_by_sensor_id(@Param("table") String table , @Param("sensor_id") String sensor_id) ;
//    @Query("select count(*) as mesurments,  sensor_id , date , region , model , AVG(value) as avg from " + "sensors" + "." + ":table " + "WHERE date >= :date GROUP BY sensor_id , date ALLOW FILTERING;")
//    public ResultSet get_all_average_after_date(@Param("table") String table , @Param("date") String date) ;
//   
//    @Query("select count(*) as mesurments,  sensor_id , date , region , model , AVG(value) as avg from " + "sensors" + "." + ":table " + "WHERE date <= :date GROUP BY sensor_id , date ALLOW FILTERING;")
//    public ResultSet get_all_average_before_date(@Param("table") String table , @Param("date") String date) ;
//   
//    @Query("select count(*) as mesurments,  sensor_id , date , region , model , AVG(value) as avg from " + "sensors" + "." + ":table " + "WHERE date = :date GROUP BY sensor_id , date ALLOW FILTERING;")
//    public ResultSet get_all_average_for_date(@Param("table") String table , @Param("date") String date) ;
//   
//    @Query("select count(*) as mesurments,  sensor_id , date , region , model , AVG(value) as avg from " + "sensors" + "." + ":table " + "WHERE region = :region GROUP BY sensor_id , date ALLOW FILTERING;")
//    public ResultSet get_all_average_for_region(@Param("table") String table , @Param("region") String region) ;
}
