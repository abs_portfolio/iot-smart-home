/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.system_static.templates;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author onelove
 */
public class ResponceWraper {

    private Map<String, ChartData> chart_data;
    private List<String> dateList;

    //Methods
    public ResponceWraper(ResultSet set, boolean shrt) {
        try {
            reformDataToChartData(set, shrt);
        } catch (ParseException ex) {
            Logger.getLogger(Responce_Wraper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void reformDataToChartData(ResultSet set, boolean shrt) throws ParseException {
        chart_data = new HashMap<String, ChartData>();
        dateList = new ArrayList<String>();
        for (Row row : set) {
            if (!dateList.contains(row.getString("date"))) {
                dateList.add(row.getString("date"));
            }
            if (chart_data.containsKey(row.getString("sensor_id"))) {
                chart_data.get(row.getString("sensor_id")).appendValueToDates(row.getString("date"));
                chart_data.get(row.getString("sensor_id")).appendDataToMap(row.getString("date"), row.getFloat("avg"));
            } else {
                chart_data.put(row.getString("sensor_id"),
                        new ChartData(row.getString("sensor_id"),
                                row.getString("model"), row.getString("region"),
                                row.getString("date"), row.getFloat("avg"))
                );
            }

        }
        if (shrt) {
            shortData();
        }
    }

    private void shortData() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String[] arr = dateList.toArray(new String[dateList.size()]);
        int n = arr.length;
        String temp = "";
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                Date date2 = format.parse(arr[j]);
                Date date1 = format.parse(arr[j - 1]);
                if (date2.compareTo(date1) <= 0) {
                    //swap elements  
                    temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }

            }
        }
        dateList = (List<String>) Arrays.asList(arr);

        for (Map.Entry<String, ChartData> entry : chart_data.entrySet()) {
            entry.getValue().reconstractValues();
            for (int i = 0; i < dateList.size(); i++) {
                if (!entry.getValue().getDates().contains(dateList.get(i))) {
                    entry.getValue().appendValueToValues(new Float(0));
                } else {
                    entry.getValue().appendValueToValues(entry.getValue().getData().get(dateList.get(i)));
                }
            }
            entry.getValue().emptyDatesList();
        }

    }

    public Map<String, ChartData> getChart_data() {
        return chart_data;
    }

    public List<String> getDateList() {
        return dateList;
    }

    public void setChart_data(Map<String, ChartData> chart_data) {
        this.chart_data = chart_data;
    }

    public void setDateList(List<String> dateList) {
        this.dateList = dateList;
    }
    
    

}
