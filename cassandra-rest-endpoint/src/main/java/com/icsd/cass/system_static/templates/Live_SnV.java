/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.system_static.templates;

import com.datastax.driver.mapping.Result;
import com.icsd.cass.models.SensorPerMinute;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 *
 * @author onelove
 */
public class Live_SnV {

    private HashMap<String, ChartData> chart_data;
    private List<String> dateList;

    public Live_SnV(Result<SensorPerMinute> results) throws ParseException {
        chart_data = new HashMap<String, ChartData>();
        dateList = new ArrayList<String>();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm", Locale.ENGLISH);

        for (SensorPerMinute row : results) {
            Date date = new java.util.Date(row.getTs());
            String time = df.format(date);

            if (!dateList.contains(time)) {
                dateList.add(time);
            }

            if (chart_data.containsKey(row.getSensorId())) {
                chart_data.get(row.getSensorId()).appendValueToDates(time);
                chart_data.get(row.getSensorId()).appendDataToMap(time, row.getValue());
            } else {
                chart_data.put(row.getSensorId(),
                        new ChartData(row.getSensorId(),
                                row.getModel(), row.getRegion(),
                                time, row.getValue())
                );
            }

        }
        shortData_time();
    }

    public Live_SnV() {

    }

    private void shortData_time() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        String[] arr = dateList.toArray(new String[dateList.size()]);
        int n = arr.length;
        String temp = "";
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                Date date2 = format.parse(arr[j]);
                Date date1 = format.parse(arr[j - 1]);
                if (date2.compareTo(date1) <= 0) {
                    //swap elements  
                    temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }

            }
        }
        dateList = (List<String>) Arrays.asList(arr);

        for (Map.Entry<String, ChartData> entry : chart_data.entrySet()) {
            entry.getValue().reconstractValues();
            for (int i = 0; i < dateList.size(); i++) {
                if (!entry.getValue().getDates().contains(dateList.get(i))) {
                    entry.getValue().appendValueToValues(new Float(0));
                } else {
                    entry.getValue().appendValueToValues(entry.getValue().getData().get(dateList.get(i)));
                }
            }
            entry.getValue().emptyDatesList();
        }

    }

    public HashMap<String, ChartData> getChart_data() {
        return chart_data;
    }

    public List<String> getDateList() {
        return dateList;
    }

    public void setChart_data(HashMap<String, ChartData> chart_data) {
        this.chart_data = chart_data;
    }

    public void setDateList(List<String> dateList) {
        this.dateList = dateList;
    }

}
