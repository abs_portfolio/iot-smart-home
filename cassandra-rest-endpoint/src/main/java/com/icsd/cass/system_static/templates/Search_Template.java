/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.system_static.templates;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author onelove
 */
@XmlRootElement
public class Search_Template implements Serializable {

    @XmlElement
    private String date_start;
    @XmlElement
    private String date_finish;
    @XmlElement
    private List<String> sensor_list = new ArrayList<String>();
    @XmlElement
    private boolean group_data;
    @XmlElement
    private String funct;
    @XmlElement
    private long start;
    @XmlElement
    private long finish;

    public Search_Template() {
        super();
    }

    public void setStart(long start) {
        this.start = start;
    }

    public void setFinish(long finish) {
        this.finish = finish;
    }

    public long getStart() {
        return start;
    }

    public long getFinish() {
        return finish;
    }

    public String getDate_start() {
        return date_start;
    }

    public String getDate_finish() {
        return date_finish;
    }

    public List<String> getSensor_list() {
        return sensor_list;
    }

    public boolean isGroup_data() {
        return group_data;
    }

    public String getFunct() {
        return funct;
    }

    public void setDate_start(String date_start) {
        this.date_start = date_start;
    }

    public void setDate_finish(String date_finish) {
        this.date_finish = date_finish;
    }

    public void setSensor_list(List<String> sensor_list) {
        this.sensor_list = sensor_list;
    }

    public void setGroup_data(boolean group_data) {
        this.group_data = group_data;
    }

    public void setFunct(String funct) {
        this.funct = funct;
    }

}
