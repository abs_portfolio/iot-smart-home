/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.system_static.templates;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author onelove
 */
@XmlRootElement
public class DayAverageReq {

    @XmlElement
    private String sensor_id;
    @XmlElement
    private String date;

    public void setSensor_id(String sensor_id) {
        this.sensor_id = sensor_id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSensor_id() {
        return sensor_id;
    }

    public String getDate() {
        return date;
    }

}
