/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.system_static.templates;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.mapping.Result;
import com.icsd.cass.models.SensorPerMinute;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author onelove
 */
public class Responce_Wraper {

    private ArrayList<Sensor_Template> record_list;
    private Map<String, List<Sensor_Template>> map;
    private HashMap<String, ChartData> chart_data;
    private List<String> dateList;

    public Responce_Wraper(ResultSet result) {
        record_list = new ArrayList();
        for (Row row : result) {
            record_list.add(new Sensor_Template(row.getLong("mesurments"),
                    row.getString("sensor_id"),
                    row.getString("date"),
                    row.getString("region"),
                    row.getString("model"),
                    row.getFloat("avg")));

        }

    }

    public Responce_Wraper(ResultSet result, boolean bool) {
        record_list = new ArrayList();
        if (bool) {
            for (Row row : result) {
                record_list.add(new Sensor_Template(row.getLong("mesurments"),
                        row.getString("sensor_id"),
                        row.getString("region"),
                        row.getString("model")));
            }
        } else {
            for (Row row : result) {
                record_list.add(new Sensor_Template(row.getLong("mesurments"),
                        row.getLong("ts"),
                        row.getString("sensor_id"),
                        row.getString("date"),
                        row.getString("region"),
                        row.getString("model"),
                        row.getFloat("avg")));

            }
        }
    }

    public Responce_Wraper(Result<SensorPerMinute> results) throws ParseException {
        chart_data = new HashMap<String, ChartData>();
        dateList = new ArrayList<String>();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm", Locale.ENGLISH);

        for (SensorPerMinute row : results) {
            Date date = new java.util.Date(row.getTs());
            String time = df.format(date);

            if (!dateList.contains(time)) {
                dateList.add(time);
            }

            if (chart_data.containsKey(row.getSensorId())) {
                chart_data.get(row.getSensorId()).appendValueToDates(time);
                chart_data.get(row.getSensorId()).appendDataToMap(time, row.getValue());
            } else {
                chart_data.put(row.getSensorId(),
                        new ChartData(row.getSensorId(),
                                row.getModel(), row.getRegion(),
                                time, row.getValue())
                );
            }

        }
        shortData_time();
    }

    public Responce_Wraper(DataForm form, ResultSet set) {
        try {
            reformData(form, set);
        } catch (ParseException ex) {
            Logger.getLogger(Responce_Wraper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Responce_Wraper(DataForm form, ResultSet set, boolean shrt) {
        try {
            reformData(form, set, shrt);
        } catch (ParseException ex) {
            Logger.getLogger(Responce_Wraper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Responce_Wraper(ArrayList<Sensor_Template> list) {
        this.record_list = list;
    }

    public Responce_Wraper() {
    }

    private void reformData(DataForm form, ResultSet set) throws ParseException {
        switch (form) {
            case ChartData: {
                reformDataToChartData(set);
                break;
            }
            case MapData: {
                reformToMapData(set);
                break;
            }
        }
    }

    private void reformData(DataForm form, ResultSet set, boolean shrt) throws ParseException {
        switch (form) {
            case ChartData: {
                reformDataToChartData(set, shrt);
                break;
            }
            case MapData: {
                reformToMapData(set);
                break;
            }
        }
    }

    private void reformDataToChartData(ResultSet set) throws ParseException {
        chart_data = new HashMap<String, ChartData>();
        dateList = new ArrayList<String>();
        for (Row row : set) {
            if (!dateList.contains(row.getString("date"))) {
                dateList.add(row.getString("date"));
            }
            if (chart_data.containsKey(row.getString("sensor_id"))) {
                chart_data.get(row.getString("sensor_id")).appendValueToDates(row.getString("date"));
                chart_data.get(row.getString("sensor_id")).appendDataToMap(row.getString("date"), row.getFloat("avg"));
            } else {
                chart_data.put(row.getString("sensor_id"),
                        new ChartData(row.getString("sensor_id"),
                                row.getString("model"), row.getString("region"),
                                row.getString("date"), row.getFloat("avg"))
                );
            }

        }

        shortData_timeData();
    }

    private void reformDataToChartData(ResultSet set, boolean shrt) throws ParseException {
        chart_data = new HashMap<String, ChartData>();
        dateList = new ArrayList<String>();
        for (Row row : set) {
            if (!dateList.contains(row.getString("date"))) {
                dateList.add(row.getString("date"));
            }
            if (chart_data.containsKey(row.getString("sensor_id"))) {
                chart_data.get(row.getString("sensor_id")).appendValueToDates(row.getString("date"));
                chart_data.get(row.getString("sensor_id")).appendDataToMap(row.getString("date"), row.getFloat("avg"));
            } else {
                chart_data.put(row.getString("sensor_id"),
                        new ChartData(row.getString("sensor_id"),
                                row.getString("model"), row.getString("region"),
                                row.getString("date"), row.getFloat("avg"))
                );
            }

        }
        if (shrt) {
            shortData();
        }
    }

    public void reformToMapData(ResultSet result) {
        map = new HashMap<String, List<Sensor_Template>>();
        record_list = new ArrayList();
        for (Row row : result) {
            if (map.containsKey(row.getString("sensor_id"))) {
                map.get(row.getString("sensor_id")).add(new Sensor_Template(row.getLong("mesurments"),
                        row.getString("sensor_id"),
                        row.getString("date"),
                        row.getString("region"),
                        row.getString("model"),
                        row.getFloat("avg")));
            } else {
                map.put(row.getString("sensor_id"), new ArrayList<Sensor_Template>());
                map.get(row.getString("sensor_id")).add(new Sensor_Template(row.getLong("mesurments"),
                        row.getString("sensor_id"),
                        row.getString("date"),
                        row.getString("region"),
                        row.getString("model"),
                        row.getFloat("avg")));
            }

        }

    }

    private ChartData constractValueMap(ChartData data) {
        int count = 0;
        if (data.getDates().size() == dateList.size()) {
            return data;
        }
        for (int i = 0; i < dateList.size(); i++) {
            if (!data.getDates().contains(dateList.get(i))) {
                data.appendValueToValues(new Float(0), count);
            }
            count++;
        }
        return data;
    }

    private void shortData() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String[] arr = dateList.toArray(new String[dateList.size()]);
        int n = arr.length;
        String temp = "";
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                Date date2 = format.parse(arr[j]);
                Date date1 = format.parse(arr[j - 1]);
                if (date2.compareTo(date1) <= 0) {
                    //swap elements  
                    temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }

            }
        }
        dateList = (List<String>) Arrays.asList(arr);

        for (Map.Entry<String, ChartData> entry : chart_data.entrySet()) {
            entry.getValue().reconstractValues();
            for (int i = 0; i < dateList.size(); i++) {
                if (!entry.getValue().getDates().contains(dateList.get(i))) {
                    entry.getValue().appendValueToValues(new Float(0));
                } else {
                    entry.getValue().appendValueToValues(entry.getValue().getData().get(dateList.get(i)));
                }
            }
            entry.getValue().emptyDatesList();
        }

    }

    private void shortData_time() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        String[] arr = dateList.toArray(new String[dateList.size()]);
        int n = arr.length;
        String temp = "";
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                Date date2 = format.parse(arr[j]);
                Date date1 = format.parse(arr[j - 1]);
                if (date2.compareTo(date1) <= 0) {
                    //swap elements  
                    temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }

            }
        }
        dateList = (List<String>) Arrays.asList(arr);

        for (Map.Entry<String, ChartData> entry : chart_data.entrySet()) {
            entry.getValue().reconstractValues();
            for (int i = 0; i < dateList.size(); i++) {
                if (!entry.getValue().getDates().contains(dateList.get(i))) {
                    entry.getValue().appendValueToValues(new Float(0));
                } else {
                    entry.getValue().appendValueToValues(entry.getValue().getData().get(dateList.get(i)));
                }
            }
            entry.getValue().emptyDatesList();
        }

    }

    private void shortData_timeData() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
        String[] arr = dateList.toArray(new String[dateList.size()]);
        int n = arr.length;
        String temp = "";
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                Date date2 = format.parse(arr[j]);
                Date date1 = format.parse(arr[j - 1]);
                if (date2.compareTo(date1) <= 0) {
                    //swap elements  
                    temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }

            }
        }
        dateList = (List<String>) Arrays.asList(arr);

        for (Map.Entry<String, ChartData> entry : chart_data.entrySet()) {
            entry.getValue().reconstractValues();
            for (int i = 0; i < dateList.size(); i++) {
                if (!entry.getValue().getDates().contains(dateList.get(i))) {
                    entry.getValue().appendValueToValues(new Float(0));
                } else {
                    entry.getValue().appendValueToValues(entry.getValue().getData().get(dateList.get(i)));
                }
            }
            entry.getValue().emptyDatesList();
        }

    }

    public void setRecord_list(ArrayList<Sensor_Template> record_list) {
        this.record_list = record_list;
    }

    public ArrayList<Sensor_Template> getRecord_list() {
        return record_list;
    }

    public void setMap(Map<String, List<Sensor_Template>> map) {
        this.map = map;
    }

    public void setChart_data(HashMap<String, ChartData> chart_data) {
        this.chart_data = chart_data;
    }

    public void setDateList(ArrayList<String> dateList) {
        this.dateList = dateList;
    }

    public Map<String, List<Sensor_Template>> getMap() {
        return map;
    }

    public HashMap<String, ChartData> getChart_data() {
        return chart_data;
    }

    public List<String> getDateList() {
        return dateList;
    }

}
