/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.system_static.templates;

/**
 *
 * @author onelove
 */
public class NameTarget {

    private final String date;
    private final String region;

    public NameTarget(String name, String target) {
        this.date = name;
        this.region = target;
    }

    public NameTarget() {
        date = null;
        region = null;
    }

    public String getDate() {
        return date;
    }

    public String getRegion() {
        return region;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((region == null) ? 0 : region.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        NameTarget other = (NameTarget) obj;
        if (date == null) {
            if (other.date != null) {
                return false;
            }
        } else if (!date.equals(other.date)) {
            return false;
        }
        if (region == null) {
            if (other.region != null) {
                return false;
            }
        } else if (!region.equals(other.region)) {
            return false;
        }
        return true;
    }

    // add getters here
}
