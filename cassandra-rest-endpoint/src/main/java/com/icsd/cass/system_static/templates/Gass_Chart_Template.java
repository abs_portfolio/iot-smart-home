/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.system_static.templates;

import io.swagger.annotations.ApiModel;

/**
 *
 * @author onelove
 */
@ApiModel(value="Gass_Chart_Template", description="Gass Class Model")

public class Gass_Chart_Template {

    private String date;
    private String region;
    private double[] values;

    public Gass_Chart_Template(String date, String region, double[] values) {
        this.date = date;
        this.region = region;
        this.values = values;
    }

    public Gass_Chart_Template(String date, String region) {
        values = new double[10];
        this.date = date;
        this.region = region;
    }

    public Gass_Chart_Template() {
        values = new double[10];
    }

    public void setValue(int pos , double value){
        values[pos] = value;
    }
    public String getDate() {
        return date;
    }

    public String getRegion() {
        return region;
    }

    public double[] getValues() {
        return values;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setValues(double[] values) {
        this.values = values;
    }

}
