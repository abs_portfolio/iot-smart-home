/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.system_static.templates;

import com.icsd.cass.models.SensorPerRow;
import java.util.ArrayList;
import java.util.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author onelove
 */
public class ChartData {

    private List<String> dates;
    private String sensor_id;
    private SensorPerRow sensor;
    private List<Float> values;
    private String model;
    private String region;
    private Map<String, Float> data;

    public ChartData(String sensor_id, String model, String region, String date, Float value) {
        this.sensor_id = sensor_id;
        this.model = model;
        this.region = region;
        dates = new ArrayList<String>();
        dates.add(date);
        data = new HashMap<String, Float>();
        data.put(date, value);
    }

    public ChartData(String sensor_id, String model, String region, String date, Float value, int index) {
        this.sensor_id = sensor_id;
        this.model = model;
        this.region = region;
        dates = new ArrayList<String>();
        dates.add(date);
        values = new ArrayList<Float>();
        if (index == 0) {
            values.add(index, value);
        } else {
            for (int i = 0; i < index; i++) {
                values.add(i, new Float(0));
            }
        }
    }

    public ChartData(String sensor_id, String model) {
        values = new ArrayList();
        this.sensor_id = sensor_id;
        this.model = model;
    }

    public ChartData(String key) {
        this.sensor_id = key;
        values = new ArrayList();
    }

    public Map<String, Float> getData() {
        return data;
    }

    public void emptyDatesList() {
        dates = null;
    }

    public void setData(Map<String, Float> data) {
        this.data = data;
    }

    public void setSensor_id(String sensor_id) {
        this.sensor_id = sensor_id;
    }

    public String getModel() {
        return model;
    }

    public String setModel() {
        return model;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public String getSensorId() {
        return this.sensor_id;
    }

    public void reconstractValues() {
        values = new ArrayList<Float>();
    }

    public void appendDataToMap(String date, Float value) {
        data.put(date, value);
    }

    public void appendValueToDates(String date) {
        this.dates.add(date);
    }

    public void appendValueToDates(String date, int index) {
        this.dates.add(index, date);
    }

    public void appendValueToValues(Float value, int index) {
        this.values.add(index, value);
    }

    public void appendValueToValues(Float value) {
        this.values.add(value);
    }

    public void swappValuePosstion(int i, int j) {
        Collections.swap(values, i, j);
    }

    public ChartData(List<String> dates, SensorPerRow sensor, List<Float> values) {
        this.dates = dates;
        this.sensor = sensor;
        this.values = values;
    }

    public SensorPerRow getSensor() {
        return sensor;
    }

    public void setSensor(SensorPerRow sensor) {
        this.sensor = sensor;
    }

    public ChartData(List<String> dates, List<Float> values) {
        this.dates = dates;
        this.values = values;
    }

    public void setModel(String model) {
        this.sensor_id = model;
    }

    public ChartData(List<String> dates, String sensor_id, List<Float> values) {
        this.dates = dates;
        this.sensor_id = sensor_id;
        this.values = values;
    }

    @Override
    public String toString() {
        return "ChartData{" + "dates=" + dates + ", values=" + values + '}';
    }

    public List<String> getDates() {
        return dates;
    }

    public List<Float> getValues() {
        return values;
    }

    public void setDates(List<String> dates) {
        this.dates = dates;
    }

    public void setValues(List<Float> values) {
        this.values = values;
    }
}
