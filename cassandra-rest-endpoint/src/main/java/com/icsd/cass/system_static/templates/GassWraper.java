/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.system_static.templates;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author onelove
 */
public class GassWraper {

    Map<Integer, String> myMap;
    private Map<NameTarget, double[]> recordlist;
    private ArrayList<Gass_Chart_Template> result;
    private ArrayList<String> datelist ;
    
    public GassWraper(ResultSet result) {
        recordlist = new HashMap<NameTarget, double[]>();
        datelist = new ArrayList<String>();
        initializeMap();
        for (Row row : result) {
            NameTarget target = new NameTarget(row.getString("date"), row.getString("region"));
            if (recordlist.containsKey(target)) {
                int pos = findPos(row.getString("sensor_id"));
                if (pos != 66) {
                    double[] temp = recordlist.get(target);
                    temp[pos] = (double) row.getFloat("avg");
                }
            } else {
                int pos = findPos(row.getString("sensor_id"));
                if (pos != 66) {
                    if(!datelist.contains(row.getString("date"))){
                        datelist.add(row.getString("date"));
                    }
                    double[] arr = new double[10];
                    arr[pos] = (double) row.getFloat("avg");
                    recordlist.put(target, arr);
                }
            }
        }
        generateList();
    }

    private final void initializeMap() {
        myMap = new HashMap<Integer, String>();
        myMap.put(new Integer(0), "Benzine");
        myMap.put(new Integer(1), "Hexane");
        myMap.put(new Integer(2), "Alcohol");
        myMap.put(new Integer(3), "H2");
        myMap.put(new Integer(4), "CH4");
        myMap.put(new Integer(5), "CO");
        myMap.put(new Integer(6), "LPG");
        myMap.put(new Integer(7), "Smoke");
        myMap.put(new Integer(8), "CO2");
        myMap.put(new Integer(9), "NH4");
    }
    static int count = 0;

    private void generateList() {
        result = new ArrayList<Gass_Chart_Template>();
        for (Map.Entry<NameTarget, double[]> entry : recordlist.entrySet()) {
            result.add(new Gass_Chart_Template(entry.getKey().getDate(), entry.getKey().getRegion(), entry.getValue()));
        }
        recordlist = null;
    }

    private int findPos(String id) {

        for (Map.Entry<Integer, String> entry : myMap.entrySet()) {
            if (id.contains(entry.getValue())) {
                if (id.contains("CO2")) {
                    return 8;
                }
                return entry.getKey();
            }
        }
        return 66;
    }

    public Map<Integer, String> getMyMap() {
        return myMap;
    }

    public Map<NameTarget, double[]> getRecordlist() {
        return recordlist;
    }

    public ArrayList<Gass_Chart_Template> getResult() {
        return result;
    }

    public ArrayList<String> getDatelist() {
        return datelist;
    }

    public static int getCount() {
        return count;
    }

}
