/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.system_static.templates;

/**
 *
 * @author onelove
 */
public class SystemTemplate {
    
    private String key;
    private String bootstrapped;
    private String broadcast_address;
    private String cluster_name;
    private String cql_version;
    private String data_center;
    private String listen_address;
    private String rpc_address;

    public SystemTemplate(String key, String bootstrapped, String broadcast_address, String cluster_name, String cql_version, String data_center, String listen_address, String rpc_address, String thrift_version) {
        this.key = key;
        this.bootstrapped = bootstrapped;
        this.broadcast_address = broadcast_address;
        this.cluster_name = cluster_name;
        this.cql_version = cql_version;
        this.data_center = data_center;
        this.listen_address = listen_address;
        this.rpc_address = rpc_address;
        this.thrift_version = thrift_version;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setBootstrapped(String bootstrapped) {
        this.bootstrapped = bootstrapped;
    }

    public void setBroadcast_address(String broadcast_address) {
        this.broadcast_address = broadcast_address;
    }

    public void setCluster_name(String cluster_name) {
        this.cluster_name = cluster_name;
    }

    public void setCql_version(String cql_version) {
        this.cql_version = cql_version;
    }

    public void setData_center(String data_center) {
        this.data_center = data_center;
    }

    public void setListen_address(String listen_address) {
        this.listen_address = listen_address;
    }

    public void setRpc_address(String rpc_address) {
        this.rpc_address = rpc_address;
    }

    public void setThrift_version(String thrift_version) {
        this.thrift_version = thrift_version;
    }

    public String getKey() {
        return key;
    }

    public String getBootstrapped() {
        return bootstrapped;
    }

    public String getBroadcast_address() {
        return broadcast_address;
    }

    public String getCluster_name() {
        return cluster_name;
    }

    public String getCql_version() {
        return cql_version;
    }

    public String getData_center() {
        return data_center;
    }

    public String getListen_address() {
        return listen_address;
    }

    public String getRpc_address() {
        return rpc_address;
    }

    public String getThrift_version() {
        return thrift_version;
    }
    private String thrift_version;
}
