/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.models;

import com.datastax.driver.mapping.annotations.ClusteringColumn;
import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.Field;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.datastax.driver.mapping.annotations.Transient;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author onelove
 */
@Table(keyspace = "sensors", name = "sensor_per_minute", readConsistency = "QUORUM",
        writeConsistency = "QUORUM", caseSensitiveKeyspace = false,
        caseSensitiveTable = false)
public class SensorPerMinute implements Serializable {

    @Transient
    @Column(name = "event_binary")
    private byte[] eventBinary;
    @Column(name = "event_json")
    private String eventJson;
    @Column(name = "model")
    private String model;
    private String region;
    @PartitionKey(0)
    @Column(name = "sensor_id")
    private String sensorId;
    @PartitionKey(1)
    @Column(name = "date")
    private String date;
    @ClusteringColumn
    private long ts;

    private float value;

    public SensorPerMinute() {
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public SensorPerMinute(String sensorId) {
        this.sensorId = sensorId;
    }

    public byte[] getEventBinary() {
        return eventBinary;
    }

    public void setEventBinary(byte[] eventBinary) {
        this.eventBinary = eventBinary;
    }

    public String getEventJson() {
        return eventJson;
    }

    public void setEventJson(String eventJson) {
        this.eventJson = eventJson;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSensorId() {
        return sensorId;
    }

    public void setSensorId(String sensorId) {
        this.sensorId = sensorId;
    }

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sensorId != null ? sensorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SensorPerMinute)) {
            return false;
        }
        SensorPerMinute other = (SensorPerMinute) object;
        if ((this.sensorId == null && other.sensorId != null) || (this.sensorId != null && !this.sensorId.equals(other.sensorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.api.model.SensorPerRow[ sensorId=" + sensorId + " ]";
    }

}
