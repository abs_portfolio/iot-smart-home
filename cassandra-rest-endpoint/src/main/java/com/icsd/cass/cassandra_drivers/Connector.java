/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.cassandra_drivers;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.RoundRobinPolicy;
import com.datastax.driver.mapping.MappingManager;
import com.datastax.driver.mapping.Result;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author onelove
 */
public class Connector {

    private static Cluster cluster = null;
    private static Session session = null;
    private static MappingManager manager = null;

    public Connector() {

    }

    private static ArrayList<InetAddress> reformData(ArrayList<String> list) throws UnknownHostException {
        ArrayList<InetAddress> contact_points = new ArrayList<InetAddress>();
        for (int i = 0; i < list.size(); i++) {
            contact_points.add(InetAddress.getByName(list.get(i)));
        }
        return contact_points;
    }

    public static void initializeCluster(ArrayList<String> cluster_ip ) throws UnknownHostException {
        String log4jConfPath = "./log4j.properties";
//        PropertyConfigurator.configure(log4jConfPath);
        PoolingOptions poolingOptions = new PoolingOptions();
        poolingOptions
                .setCoreConnectionsPerHost(HostDistance.LOCAL, 4)
                .setMaxConnectionsPerHost(HostDistance.LOCAL, 10)
                .setCoreConnectionsPerHost(HostDistance.REMOTE, 2)
                .setMaxConnectionsPerHost(HostDistance.REMOTE, 4);
        /*
            
            If connections stay idle for too long, they might be dropped by intermediate network devices (routers, firewalls…). 
            Normally, TCP keepalive should take care of this; but tweaking low-level keepalive settings might be impractical in some environments.

            The driver provides application-side keepalive in the form of a connection heartbeat: when a connection has been idle for a given amount of time, 
            the driver will simulate activity by writing a dummy request to it.

            This feature is enabled by default. The default heartbeat interval is 30 seconds, it can be customized with the following method:
         */
        poolingOptions.setHeartbeatIntervalSeconds(60);

        cluster = Cluster.builder() // (1)
                .addContactPoints(reformData(cluster_ip))
                .withLoadBalancingPolicy(new RoundRobinPolicy())
                .withPoolingOptions(poolingOptions)
                .build();

    }

    public static Cluster getCluster() {
        return cluster;
    }

    public static Session getSession() {
        return session;
    }

    public static MappingManager getManager() {
        return manager;
    }

    public static final void closeClusterConnection() {
        cluster.close();
    }

    public static final void generateSession(String keyspace) {
        session = cluster.connect(keyspace);
        manager = new MappingManager(session);

    }
    

    public static final void terminateSession() {
        session.close();
    }

    public ResultSet executeCustomQuery(String query) {
        return session.execute(query);
    }

}
