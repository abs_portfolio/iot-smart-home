/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.services;

import com.google.gson.Gson;
import com.icsd.cass.controlers.SensorPerMinute_Controler;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.icsd.cass.system_static.templates.Responce_Wraper;
import com.icsd.cass.system_static.templates.Search_Template;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import java.text.ParseException;
import javax.ws.rs.Consumes;

/**
 *
 * @author onelove
 */
@Path("/sensors/live")
@Api(value = "Live Records")
@Produces({"application/json", "application/xml"})
@SwaggerDefinition(info = @Info(
        description = "Live records Api",
        version = "V1.0",
        title = "The Live records Api",
        termsOfService = "something",
        contact = @Contact(
                name = "Georgios Fiotakis",
                email = "g.fiotakis@akka.eu",
                url = "https://www.linkedin.com/in/george-fiotakis-320967159/"
        ),
        license = @License(
                name = "Apache 2.0",
                url = "http://www.apache.org/licenses/LICENSE-2.0"
        )
),
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"},
        schemes = {SwaggerDefinition.Scheme.HTTP, SwaggerDefinition.Scheme.HTTP},
        tags = {
            @Tag(name = "Live Records", description = "REST Endpoint for Live Sendor Records. Records that are beeing stored for only 3 minutes. Through this Api we can retrieve the last 3 minute records.")})

public class LiveResults {

    private static SensorPerMinute_Controler controler = null;
    private final static Gson json = new Gson();

    private SensorPerMinute_Controler getControler() {
        return new SensorPerMinute_Controler();
    }

    @GET
    @Path("/getCurrAvg")
    @ApiOperation(value = "Finds the live average value results and returns them grouped by sensros_id & date",
            notes = "Responce Wraper Template",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Response get_curr_avg() {
        return Response.ok(json.toJson(getControler()
                .get_Currnet_average_Records()),
                MediaType.APPLICATION_JSON)
                .build();
    }

    @GET
    @Path("/")
    @ApiOperation(value = "Returns all the table records as raw data.",
            notes = "The results return in a list form, the whay that they are stored in database",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllCurrent() {
        return Response.ok(getControler()
                .getAllRecords(),
                MediaType.APPLICATION_JSON)
                .build();

    }

    @POST
    @Path("/getLiveBySensor_id/{sensor_id}")
    @ApiOperation(value = "Returns records for a specific Sensor.",
            notes = "The sensor is identified by its sensor_id and the value that we return is the average of the tables records for this specific sensor",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getCurrentAvgBySensor_id(@PathParam("sensor_id") String id) {
        return Response.ok(getControler().get_Currnet_average_Records_by_Sensor_id(id),
                MediaType.APPLICATION_JSON)
                .build();
    }

    @GET
    @Path("/all_sensors")
    @ApiOperation(value = "Returs all the record of the table grouped by sensor_id",
            notes = "The grouped value is the average of the records.",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Response get_all_sensors() {
        return Response.ok(json.toJson(getControler()
                .get_all_sensors()),
                MediaType.APPLICATION_JSON)
                .build();

    }

    @GET
    @Path("/getCurrMin")
    @ApiOperation(value = "Finds the live minimum value and returns them grouped by sensros_id & date",
            notes = "The mimimum value of the grouped data",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Response get_curr_min() {
        return Response.ok(json.toJson(getControler()
                .get_Currnet_min_Records()),
                MediaType.APPLICATION_JSON)
                .build();
    }

    @GET
    @Path("/getCurrMax")
    @ApiOperation(value = "Finds the live maximum value and returns them grouped by sensros_id & date",
            notes = "The maximum value of the grouped data",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Response get_curr_max() {
        return Response.ok(json.toJson(getControler()
                .get_Currnet_max_Records()),
                MediaType.APPLICATION_JSON)
                .build();
    }

    @POST
    @Path("/getMaxBySensor_id/{sensor_id}")
    @ApiOperation(value = "Finds the live maximum value for a specific sensor and returns them grouped by sensros_id & date",
            notes = "The maximum value of the grouped data",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Responce_Wraper getCurrentMaxBySensor_id(@PathParam("sensor_id") String id) {
        return getControler().get_Currnet_max_Records_by_Sensor_id(id);

    }

    @POST
    @Path("/getMinBySensor_id/{sensor_id}")
    @ApiOperation(value = "Finds the live mimimum value for a specific sensor and returns them grouped by sensros_id & date",
            notes = "The mimimum value of the grouped data",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Responce_Wraper getCurrentMinBySensor_id(@PathParam("sensor_id") String id) {
        return getControler().get_Currnet_min_Records_by_Sensor_id(id);

    }

    @GET
    @Path("/get_sensor_raw/{sensor_id}")
    @ApiOperation(value = "Returns the raw data about a specific sensor_id",
            notes = "Non Grouped, Non Formed",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Response get_sensor_raw(@PathParam("sensor_id") String id) {
        return Response.ok(json.toJson(getControler().get_sensor_raw(id)),
                MediaType.APPLICATION_JSON)
                .build();
    }

    @POST
    @Path("/m_searchNvisualize")
    @ApiOperation(value = "This service refairs to the search and visualize service provided by our dashboard",
            notes = "The only field that needs to be defined is the sensor list, which will represent the sensors you want to include. The data are NOT grouped because there is no reason for it in this case. So there is no need to specify the function (min,max avg). Also there is no mean to specify time limits because the data we target are 3 minutes old at most.",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response m_searchNvisualize(Search_Template json) throws ParseException {
        return Response.ok((getControler()
                .m_searchNvisualize(json)),
                MediaType.APPLICATION_JSON)
                .build();
    }
    

}
