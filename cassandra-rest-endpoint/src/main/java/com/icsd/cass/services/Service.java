/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.services;

import com.google.gson.Gson;
import com.icsd.cass.controlers.SensorPerRow_Controler;
import com.icsd.cass.system_static.templates.Responce_Wraper;
import com.icsd.cass.system_static.templates.SystemTemplate;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;

/**
 *
 * @author onelove
 */
@Path("/system/")
@Api(value = "System")
@Produces({"application/json", "application/xml"})
@SwaggerDefinition(info = @Info(
        description = "Sensor List Api",
        version = "V1.0",
        title = "The system Api",
        termsOfService = "something",
        contact = @Contact(
                name = "Georgios Fiotakis",
                email = "g.fiotakis@akka.eu",
                url = "https://www.linkedin.com/in/george-fiotakis-320967159/"
        ),
        license = @License(
                name = "Apache 2.0",
                url = "http://www.apache.org/licenses/LICENSE-2.0"
        )
),
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"},
        schemes = {SwaggerDefinition.Scheme.HTTP, SwaggerDefinition.Scheme.HTTP},
        tags = {
            @Tag(name = "System", description = "REST Endpoint for providing information about the system.")})

public class Service {

    private final static Gson json = new Gson();

    private SensorPerRow_Controler getControler() {
        return new SensorPerRow_Controler();
    }

    @GET
    @Path("pingCass")
    @ApiOperation(value = "This service provides all the information about the cassandra cluster",
            notes = "Cassandra cluster information",
            response = SystemTemplate.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Response pingCassandraClusters() {
        return Response.ok(json.toJson(getControler().getSysInfo()),
                MediaType.APPLICATION_JSON)
                .build();
    }
}
/*
REST Application: http://localhost:8080/cass_prod/                                 -> org.apache.openejb.server.rest.InternalApplication@2c26ba07
     Service URI: http://localhost:8080/cass_prod/sensors/daily                    -> Pojo com.icsd.cass.services.DailyResults 
               GET http://localhost:8080/cass_prod/sensors/daily/                   ->      Response get_all_rows()            
               GET http://localhost:8080/cass_prod/sensors/daily/getDayAvg          ->      Response get_all_rows_Day_average()
     Service URI: http://localhost:8080/cass_prod/sensors/live                     -> Pojo com.icsd.cass.services.LiveResults  
               GET http://localhost:8080/cass_prod/sensors/live/                    ->      Response getAllCurrent()
               GET http://localhost:8080/cass_prod/sensors/live/getCurrAvg          ->      Response authorization()
      Service URI: http://localhost:8080/cass_prod/sensors/overal                   -> Pojo com.icsd.cass.services.OveralResults
               GET http://localhost:8080/cass_prod/sensors/overal/                  ->      Response get_all_rows()            
               GET http://localhost:8080/cass_prod/sensors/overal/getAvgOverallTemp ->      Response get_avg_temp_overal()     
               GET http://localhost:8080/cass_prod/sensors/overal/getDayAvg         ->      Response get_all_rows_Day_average()
     Service URI: http://localhost:8080/cass_prod/system/                          -> Pojo com.icsd.cass.services.Service      
              GET http://localhost:8080/cass_prod/system/pingCass                  ->      String pingCassandraClusters()
*/