/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.services;

import com.google.gson.Gson;
import com.icsd.cass.controlers.SensorPerDay_Controler;
import com.icsd.cass.system_static.templates.Responce_Wraper;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.icsd.cass.system_static.templates.Search_Template;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import javax.ws.rs.DELETE;

/**
 *
 * @author onelove
 */
@Path("/sensors/daily")
@Api(value = "Sensors daily")
@Produces({"application/json", "application/xml"})
@SwaggerDefinition(info = @Info(
        description = "Daily records Api",
        version = "V1.0",
        title = "The daily records Api",
        termsOfService = "something",
        contact = @Contact(
                name = "Georgios Fiotakis",
                email = "g.fiotakis@akka.eu",
                url = "https://www.linkedin.com/in/george-fiotakis-320967159/"
        ),
        license = @License(
                name = "Apache 2.0",
                url = "http://www.apache.org/licenses/LICENSE-2.0"
        )
),
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"},
        schemes = {SwaggerDefinition.Scheme.HTTP, SwaggerDefinition.Scheme.HTTP},
        tags = {
            @Tag(name = "Sensors daily", description = "REST Endpoint for Daily Sendor Records. Records that are beeing stored for only 24 hours. Through this Api we can retrieve the last 24 hours records.")})
public class DailyResults {

    private static SensorPerDay_Controler controler2 = null;
    private final static Gson json = new Gson();

    private SensorPerDay_Controler getControler() {
        return new SensorPerDay_Controler();
    }

    @GET
    @Path("/getDayAvg")
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Finds the daily results and returns them grouped by sensros_id & date",
            notes = "Responce Wraper Template",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    public Response get_Currnet_average_Records() {
        return Response.ok(json.toJson(getControler()
                .get_Currnet_average_Records()),
                MediaType.APPLICATION_JSON)
                .build();

    }

    @GET
    @Path("/")
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Returns the whole table records.",
            notes = "Without formating",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    public Response getAll() {
        return Response.ok(json.toJson(getControler()
                .getAllRecords()),
                MediaType.APPLICATION_JSON)
                .build();

    }

    @POST
    @Path("/avgBetweenTs")
    @ApiOperation(value = "Returns the average value of grouped data between 2 timestamps.",
            notes = "Timestamps must be in String format 'yyyy-mm-dd hh'.",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response return_average_between_ts_grooped_daily(Search_Template template) {
        return Response.ok(json.toJson(getControler()
                .return_average_between_ts_grooped_daily(template.getDate_start(), template.getDate_finish())),
                MediaType.APPLICATION_JSON)
                .build();

    }

    @POST
    @Path("/minBetweenTs")
    @ApiOperation(value = "Returns the minimum value of grouped data between 2 timestamps.",
            notes = "Timestamps must be in String format 'yyyy-mm-dd hh'.",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response return_min_between_ts_grooped_daliy(Search_Template template) {
        return Response.ok(json.toJson(getControler()
                .return_min_between_ts_grooped_daily(template.getDate_start(), template.getDate_finish())),
                MediaType.APPLICATION_JSON)
                .build();

    }

    @POST
    @Path("/maxBetweenTs")
    @ApiOperation(value = "Returns the maximum value of grouped data between 2 timestamps.",
            notes = "Timestamps must be in String format 'yyyy-mm-dd hh'.",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response return_max_between_ts_grooped_daily(Search_Template template) {
        return Response.ok(json.toJson(getControler()
                .return_max_between_ts_grooped_daily(template.getDate_start(), template.getDate_finish())),
                MediaType.APPLICATION_JSON)
                .build();

    }

    @GET
    @Path("/dailyMax")
    @ApiOperation(value = "Returns the max value of records grouped by sensor_id, date.",
            notes = "",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Response get_day_max_() {
        return Response.ok(json.toJson(getControler()
                .get_day_max_()),
                MediaType.APPLICATION_JSON)
                .build();

    }

    @POST
    @Path("/d_searchNvisualize")
    @ApiOperation(value = "This service refairs to the search and visualize service provided by our dashboard",
            notes = "For this service the time limits must be in format 'yyyy-mm-dd hh' (to avoid time zone conflicts), The functions must have the values of max min or avg, This service retrns only grouped data.",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 400, message = "Bad Request. Please provide the correct payload.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response searchNVisualize_daily(Search_Template json) {
        return Response.ok((getControler()
                .searchNVisualize_daily_s(json)),
                MediaType.APPLICATION_JSON)
                .build();
    }

}

//    @DELETE
//    @Path("/Delete_by_id")
//    @ApiOperation(value = "Delets a record by its sensor id.",
//            notes = "Note. If you use this service will delete every record in the table with the specified sensor_id. So practically you wipe the table of a sensor. Speciy the date usint the dateStart field",
//            response = Responce_Wraper.class,
//            responseContainer = "List")
//    @ApiResponses(value = {
//        @ApiResponse(code = 200, message = "OK")
//        ,
//        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
//        ,
//        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
//        ,
//        @ApiResponse(code = 500, message = "Something wrong in Server")})
//    @Produces({MediaType.APPLICATION_JSON})
//    @Consumes({MediaType.APPLICATION_JSON})
//    public Response deleteBySensorId(Search_Template jsonTemp) {
//        getControler().deleteBySensorId(jsonTemp.getSensor_list().get(0), jsonTemp.getDate_start());
//        return Response.ok(json.toJson("Ok"),
//                MediaType.APPLICATION_JSON)
//                .build();
//
//    }
