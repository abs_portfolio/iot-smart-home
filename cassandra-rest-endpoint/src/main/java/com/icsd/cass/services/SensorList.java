/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.services;

import com.google.gson.Gson;
import com.icsd.cass.controlers.SensorList_Controler;
import com.icsd.cass.controlers.SensorPerRow_Controler;
import com.icsd.cass.system_static.templates.Responce_Wraper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author onelove
 */
@Path("/sensors/sensor_list")
@Api(value = "List of Sensors")
@Produces({"application/json", "application/xml"})
@SwaggerDefinition(info = @Info(
        description = "Sensor List Api",
        version = "V1.0",
        title = "Sensor List records Api",
        termsOfService = "something",
        contact = @Contact(
                name = "Georgios Fiotakis",
                email = "g.fiotakis@akka.eu",
                url = "https://www.linkedin.com/in/george-fiotakis-320967159/"
        ),
        license = @License(
                name = "Apache 2.0",
                url = "http://www.apache.org/licenses/LICENSE-2.0"
        )
),
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"},
        schemes = {SwaggerDefinition.Scheme.HTTP, SwaggerDefinition.Scheme.HTTP},
        tags = {
            @Tag(name = "List of Sensors", description = "REST Endpoint for providing the list of sensors registred on our system. This api can access all sensor id if the sensor has at least one record on our system.")})

public class SensorList {
    private static SensorList_Controler controler2 = null;
    private final static Gson json = new Gson();

    private SensorList_Controler getControler() {
        return new SensorList_Controler();
    }
    
    @GET
     @ApiOperation(value = "Returns the list of sensors that has at least one record in our system",
            notes = "The overal sensor id list",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getSensorList() {
        return Response.ok
                (json.toJson(getControler()
                        .get_all_sensors()), 
                MediaType.APPLICATION_JSON)
                        .build();

    }
}
