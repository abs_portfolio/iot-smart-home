/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.services;

import com.google.gson.Gson;
import com.icsd.cass.controlers.SensorPerRow_Controler;
import com.icsd.cass.system_static.templates.GassWraper;
import com.icsd.cass.system_static.templates.Responce_Wraper;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.icsd.cass.system_static.templates.Search_Template;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;

/**
 *
 * @author onelove
 */
@Path("/sensors/overal")
@Api(value = "Overal Records")
@Produces({"application/json", "application/xml"})
@SwaggerDefinition(info = @Info(
        description = "Overal records Api",
        version = "V1.0",
        title = "The overal records Api",
        termsOfService = "something",
        contact = @Contact(
                name = "Georgios Fiotakis",
                email = "g.fiotakis@akka.eu",
                url = "https://www.linkedin.com/in/george-fiotakis-320967159/"
        ),
        license = @License(
                name = "Apache 2.0",
                url = "http://www.apache.org/licenses/LICENSE-2.0"
        )
),
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"},
        schemes = {SwaggerDefinition.Scheme.HTTP, SwaggerDefinition.Scheme.HTTP},
        tags = {
            @Tag(name = "Overal Records", description = "REST Endpoint for Overal Sendor Records. Records that are beeing stored for ever. Through this Api we can retrieve all the records.")})

public class OveralResults {

    private static SensorPerRow_Controler controler2 = null;
    private final static Gson json = new Gson();

    private SensorPerRow_Controler getControler() {
        return new SensorPerRow_Controler();
    }

    @GET
    @Path("/getDayAvg")
    @ApiOperation(value = "Finds the daily results and returns them grouped by sensros_id & date",
            notes = "Responce Wraper Template",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Response get_all_rows_Day_average() {
        return Response.ok(json.toJson(getControler()
                .getAll_average_Records()),
                MediaType.APPLICATION_JSON)
                .build();
    }

    @GET
    @Path("/getMaxDay")
    @ApiOperation(value = "Finds the daily results and returns them grouped by sensros_id & date, maximum value is grouped",
            notes = "Responce Wraper Template",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Response get_max_daily() {
        return Response.ok(json.toJson(getControler()
                .getAll_maximum_Records()),
                MediaType.APPLICATION_JSON)
                .build();
    }

    @GET
    @Path("/getMinDay")
    @ApiOperation(value = "Finds the daily results and returns them grouped by sensros_id & date, minimum value is grouped",
            notes = "Responce Wraper Template",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Response get_min_daily() {
        return Response.ok(json.toJson(getControler()
                .getAll_minimum_Records()),
                MediaType.APPLICATION_JSON)
                .build();
    }

    @POST
    @Path("/avgBetweenTs")
    @ApiOperation(value = "Returns the average value of grouped data between 2 timestamps.",
            notes = "Timestamps must be in long format.",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response return_average_between_ts_grooped(Search_Template template) {
        return Response.ok(json.toJson(getControler()
                .return_average_between_ts_grooped(template.getStart(), template.getFinish())),
                MediaType.APPLICATION_JSON)
                .build();
    }

    @GET
    @Path("/gasses_chart1")
    @ApiOperation(value = "Returns the average value of air quolity data.",
            notes = "As previus times data are grouped by sensor id and date. This service returns only the air quality measurments.",
            response = GassWraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Response get_all_gasses() {
        return Response.ok(json.toJson(getControler()
                .get_all_gasses()),
                MediaType.APPLICATION_JSON)
                .build();
    }

    @GET
    @Path("/")
    @Produces({MediaType.APPLICATION_JSON})
    public Response get_all_rows() {
        return Response.ok(json.toJson(getControler()
                .getAllRecords()),
                MediaType.APPLICATION_JSON)
                .build();
    }

    @GET
    @Path("/getAvgOverallTemp")
    @ApiOperation(value = "Returns the average value of temprature ",
            notes = "This method returns only the live DHT sensor value which is difined as the sensor in region Kitchen",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Response get_avg_temp_overal() {
        return Response.ok(json.toJson(getControler()
                .get_avg_temp_overal()),
                MediaType.APPLICATION_JSON)
                .build();

    }

    @GET
    @Path("/all_sensors")
    @ApiOperation(value = "Returns the average grouped value of all sensor records",
            notes = "All the data are grouped by their sensor_id and their date",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    public Response get_all_sensors() {
        return Response.ok(json.toJson(getControler()
                .get_all_sensors()),
                MediaType.APPLICATION_JSON)
                .build();

    }

    @POST
    @Path("/maxBetweenTs")
    @ApiOperation(value = "Returns the maximum value of grouped data between 2 timestamps.",
            notes = "Timestamps must be in long format.",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response return_max_between_ts_grooped(Search_Template template) {
        return Response.ok(json.toJson(getControler()
                .return_max_between_ts_grooped(template.getStart(), template.getFinish())),
                MediaType.APPLICATION_JSON)
                .build();

    }

    @POST
    @Path("/minBetweenTs")
    @ApiOperation(value = "Returns the minimum value of grouped data between 2 timestamps.",
            notes = "Timestamps must be in long format.",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response return_min_between_ts_grooped(Search_Template template) {
        return Response.ok(json.toJson(getControler()
                .return_min_between_ts_grooped(template.getStart(), template.getFinish())),
                MediaType.APPLICATION_JSON)
                .build();

    }

    @POST
    @Path("/hello")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public Response temp(Search_Template json) {
        try {
            System.out.println(json.getDate_finish());
            System.out.println(json.getDate_start());
            for (int i = 0; i < json.getSensor_list().size(); i++) {
                System.out.println(json.getSensor_list().get(i));
            }
            System.out.println(json.isGroup_data());

        } catch (java.lang.IllegalArgumentException ex) {
            return Response.ok(json,
                    MediaType.APPLICATION_JSON)
                    .build();
        }
        return Response.ok(json,
                MediaType.APPLICATION_JSON)
                .build();
    }

    @POST
    @Path("/searchNvisualize")
    @ApiOperation(value = "This service refairs to the search and visualize service provided by our dashboard",
            notes = "Timestamps must be in long format, The functions must have the values of max min or avg, This service retrns only grouped data.",
            response = Responce_Wraper.class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response searchNVisualize_overal(Search_Template json) {
        return Response.ok((getControler()
                .searchNVisualize_overal(json)),
                MediaType.APPLICATION_JSON)
                .build();
    }
}
