/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.testing.repository;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.icsd.cass.cassandra_drivers.Connector;
import com.icsd.cass.controlers.interfaces.SensorListInterface;
import com.icsd.cass.controlers.interfaces.SensorPerMinuteInterface;
import com.icsd.cass.testing.Connector_Test;
import com.icsd.cass.testing.integration.SensorList_Test;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author onelove
 */
public class SensorListTest {

    private static Connector_Test conn;
    private static SensorListInterface sensors_accesor;

    public SensorListTest() {
    }

    private static boolean run_tests_SensorListTest = false;

    @BeforeClass
    public static void setUp() {
        if (!run_tests_SensorListTest) {
            return;
        }
        conn = new Connector_Test();
        sensors_accesor = Connector_Test.getManager().createAccessor(SensorListInterface.class);
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void hello() {
        if (!run_tests_SensorListTest) {
            return;
        }
        System.out.println("DAILY TEST hello");
    }

    @Test
    public void print() {
        if (!run_tests_SensorListTest) {
            return;
        }
        System.out.println("DAILY TEST print");
    }

    public static void set_run_tests_SensorListTest(boolean arg) {
        run_tests_SensorListTest = arg;
    }

    @Test
    public void testMockQuery() throws Exception {
        Session session = Mockito.mock(Session.class);

        // Mock a ResultSet that gets returned from ResultSetFuture#get()
        ResultSet result = Mockito.mock(ResultSet.class);
        List<Row> rows = new ArrayList<Row>();
        Row r = Mockito.mock(Row.class);
        Mockito.doReturn(1).when(r).getInt(0);
        rows.add(r);

        Mockito.doReturn(rows).when(result).all();

        // Mock a ResultSetFuture that gets returned from Session#executeAsync()
        ResultSetFuture future = Mockito.mock(ResultSetFuture.class);
        Mockito.doReturn(result).when(future).get();
        Mockito.doReturn(future).when(session).executeAsync(Mockito.anyString());

        // Execute the query and print the 0th column of the first result.
        ResultSetFuture resultF = session.executeAsync("select * from sensor_list where sensor_id= 'Alcohol_Kitchen' Allow Filtering;");
        assertEquals(false, result.all().isEmpty());
        assertEquals(1, resultF.get().all().iterator().next().getInt(0));
    }
}
