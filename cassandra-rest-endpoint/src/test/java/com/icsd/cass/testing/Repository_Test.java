/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.testing;

import com.datastax.driver.core.*;
import com.icsd.cass.conf.Config;
import com.icsd.cass.testing.repository.SensorListTest;
import static com.icsd.cass.testing.repository.SensorListTest.set_run_tests_SensorListTest;
import com.icsd.cass.testing.repository.SensorPerDayTest;
import com.icsd.cass.testing.repository.SensorPerMinuteTest;
import com.icsd.cass.testing.repository.SensorPerRowTest;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author onelove
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    SensorListTest.class,
    SensorPerRowTest.class,
    SensorPerMinuteTest.class,
    SensorPerDayTest.class,})
public class Repository_Test {

    private final static boolean initialize_cluster_connection = false;

    @After
    public void cleanUp() {

    }

    @Test
    public void testMockExample() throws Exception {
        Session session = Mockito.mock(Session.class);

        // Mock a ResultSet that gets returned from ResultSetFuture#get()
        ResultSet result = Mockito.mock(ResultSet.class);
        List<Row> rows = new ArrayList<Row>();
        Row r = Mockito.mock(Row.class);
        Mockito.doReturn(5).when(r).getInt(0);
        rows.add(r);

        Mockito.doReturn(rows).when(result).all();

        // Mock a ResultSetFuture that gets returned from Session#executeAsync()
        ResultSetFuture future = Mockito.mock(ResultSetFuture.class);
        Mockito.doReturn(result).when(future).get();
        Mockito.doReturn(future).when(session).executeAsync(Mockito.anyString());

        // Execute the query and print the 0th column of the first result.
        ResultSetFuture resultF = session.executeAsync("select * from sensor_per_row where sensor_id= 'Alcohol_Kitchen' Allow Filtering;");
        assertEquals(false, result.all().isEmpty());
        assertEquals(5, resultF.get().all().iterator().next().getInt(0));
    }

    private List mock = mock(List.class);

    @Test
    public void size_basic() {
        when(mock.size()).thenReturn(5);
        assertEquals(5, mock.size());
    }

    public static final ArrayList<String> clusters_test;
    public static final String keyspace_test = "sensors";

    static {
        clusters_test = new ArrayList<String>();
        clusters_test.add("185.244.128.27");
    }

    //intializing cluester connection
    @BeforeClass
    public static void create_test_cluster_connection() {
        if (!initialize_cluster_connection) {
            return;
        }
        run_tests();
        System.out.println("INITIALIAZINGGGGG>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        try {
            Connector_Test.initializeCluster(clusters_test);
            Connector_Test.generateSession(keyspace_test);
        } catch (UnknownHostException ex) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //closing cluster connection
    @AfterClass
    public static void close_test_cluster_connection() {
        if (!initialize_cluster_connection) {
            return;
        }
        stop_tests();
        System.out.println("CLOSINGGGGGG>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        Connector_Test.terminateSession();
        Connector_Test.closeClusterConnection();

    }

    private final static void run_tests() {
        set_run_tests_SensorListTest(false);
    }

    private final static void stop_tests() {
        set_run_tests_SensorListTest(false);
    }

}
