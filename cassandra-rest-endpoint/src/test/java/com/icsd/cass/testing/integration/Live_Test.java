/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.testing.integration;

import com.google.gson.Gson;
import com.icsd.cass.system_static.templates.Search_Template;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author onelove
 */
public class Live_Test {
    
    public Live_Test() {
    }
    
    private Gson jsonify = new Gson();


//    @Test
//    public void check_service_responce() throws IOException {
//        String name = RandomStringUtils.randomAlphabetic(8);
//        HttpUriRequest request = new HttpGet("http://185.244.128.27:8090/cass_prod/sensors/live/getCurrAvg");
//
//        // When
//        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
//
//        // Then
//        assertEquals(
//                httpResponse.getStatusLine().getStatusCode(),
//                (HttpStatus.SC_OK));
//
//    }
//    @Test
//    public void check_service_post_responce() throws IOException {
//        String name = RandomStringUtils.randomAlphabetic(8);
//        HttpUriRequest request = new HttpPost("http://185.244.128.27:8090/cass_prod/sensors/live/getLiveBySensor_id/DHT22_Kitchen");
//
//        // When
//        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
//
//        // Then
//        assertEquals(
//                httpResponse.getStatusLine().getStatusCode(),
//                (HttpStatus.SC_OK));
//
//    }
//
//    @Test
//    public void check_service_not_allowd() throws IOException {
//        String name = RandomStringUtils.randomAlphabetic(8);
//        HttpUriRequest request = new HttpGet("http://185.244.128.27:8090/cass_prod/sensors/live/getLiveBySensor_id/DHT22_Kitchen");
//
//        // When
//        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
//        HttpEntity entity = httpResponse.getEntity();
//        String responseString = EntityUtils.toString(entity, "UTF-8");
//        // Then
//        assertEquals(
//                httpResponse.getStatusLine().getStatusCode(),
//                (HttpStatus.SC_METHOD_NOT_ALLOWED));
//
//    }
//
//    @Test
//    public void check_service_unsaported_media_type() throws IOException {
//        String name = RandomStringUtils.randomAlphabetic(8);
//        HttpUriRequest request = new HttpPost("http://185.244.128.27:8090/cass_prod/sensors/live/m_searchNvisualize");
//
//        // When
//        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
//        // Then
//        assertEquals(
//                httpResponse.getStatusLine().getStatusCode(),
//                (HttpStatus.SC_UNSUPPORTED_MEDIA_TYPE));
//
//    }
//
//    @Test
//    public void check_service_internal() throws IOException {
//        String name = RandomStringUtils.randomAlphabetic(8);
//        HttpUriRequest request = new HttpPost("http://185.244.128.27:8090/cass_prod/sensors/live/m_searchNvisualize");
//        request.addHeader("Content-Type", "application/json");
//        // When
//        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
//
//        HttpEntity entity = httpResponse.getEntity();
//        // Then
//        assertEquals(
//                httpResponse.getStatusLine().getStatusCode(),
//                (HttpStatus.SC_INTERNAL_SERVER_ERROR));
//
//    }

//    @Test
//    public void check_service_ok() throws IOException {
//        String name = RandomStringUtils.randomAlphabetic(8);
//        HttpPost request = new HttpPost("http://185.244.128.27:8090/cass_prod/sensors/live/m_searchNvisualize");
//        request.addHeader("Content-Type", "application/json");
//        StringEntity postingString = new StringEntity(jsonify.toJson(create_dummy_object()));//gson.tojson() converts your pojo to json
//        request.setEntity(postingString);
//        
//        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
//
//        HttpEntity entity = httpResponse.getEntity();
//        // Then
//        assertEquals(
//                httpResponse.getStatusLine().getStatusCode(),
//                (HttpStatus.SC_OK));
//
//    }

//    public static final Search_Template create_dummy_object() {
//        Search_Template tempalte = new Search_Template();
//        List<String> list = new ArrayList();
//        list.add("Smoke_Kitchen");
//        list.add("CO2_Kitchen");
//        list.add("CH4_Kitchen");
//        list.add("DHT22_Bedroom");
//        tempalte.setSensor_list(list);
//        return tempalte;
//    }
}
