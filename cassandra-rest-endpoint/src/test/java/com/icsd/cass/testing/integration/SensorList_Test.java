/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.testing.integration;

import com.google.gson.Gson;
import java.io.IOException;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author onelove
 */
public class SensorList_Test {
    
    private Gson jsonify = new Gson();
    private static final int sensor_count = 36;
    public SensorList_Test() {
    }

    @Before
    public void setUp() {

    }

//    @Test
//    public void check_service_responce() throws IOException {
//        String name = RandomStringUtils.randomAlphabetic(8);
//        HttpUriRequest request = new HttpGet("http://185.244.128.27:8090/cass_prod/sensors/sensor_list");
//
//        // When
//        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
//
//        // Then
//        assertEquals(
//                httpResponse.getStatusLine().getStatusCode(),
//                (HttpStatus.SC_OK));
//
//    }
//
//    @Test
//    public void check_service_responce_list_size() throws IOException {
//        String name = RandomStringUtils.randomAlphabetic(8);
//        HttpUriRequest request = new HttpGet("http://185.244.128.27:8090/cass_prod/sensors/sensor_list");
//
//        // When
//        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
//        HttpEntity entity = httpResponse.getEntity();
//        String responseString = EntityUtils.toString(entity, "UTF-8");
//        Responce_Model resp = jsonify.fromJson(responseString, Responce_Model.class);
//        // Then
//        assertEquals(
//                resp.getRecord_list().size(),
//               sensor_count);
//
//    }
//    @Test
//    public void check_service_bad_request() throws IOException {
//        String name = RandomStringUtils.randomAlphabetic(8);
//        HttpUriRequest request = new HttpPost("http://185.244.128.27:8090/cass_prod/sensors/sensor_list");
//
//        // When
//        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
//        HttpEntity entity = httpResponse.getEntity();
//        String responseString = EntityUtils.toString(entity, "UTF-8");
//        // Then
//        assertEquals(
//                httpResponse.getStatusLine().getStatusCode(),
//                (HttpStatus.SC_METHOD_NOT_ALLOWED));
//
//    }

}
