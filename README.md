# IoT-Smart-Home

This is a smart-home based on IoT technology. Main services provided are : real time environment monitoring & data visualization, device control & automation and feature extraction. This project is the practical implementation of my thesis.
This system is scalable and can be used for different use case senarios. You just need to modify it a bit. It can be used as smart agricalture system, on a robot etc.

There are a numerous of different components in which this system depends on. In order to give the best demonstration possible I will start with the demonstration of the overal system's architecture and I will start explaining the different systems from botom to top (endpoints - data distribution etc.)


**The System's Architecture.**

In this chapter the overal systems architecture is discused. For a more detailed view on the system's architecture you can navigate to the [architecture file](./architecture.md) which gives a more detailed demonstration of every systme's component and its architecture.

With that being said lets dive into the overal system architecture. The system as mentioned is based on IoT technology therefore it's architecture is based on IoT. The IoT architecture model that it's based on is the four layer architecture.

*In this particular situation the four layer architecture consists from the following 4 layers.*

1.  Perception or Hardware layer.
2.  Transport or Networking layer.
3.  Midlware layer.
4.  Application layer.

    <p align="center">
    <img src="./images/arch/Four Layer Architecture.jpg" alt="architecture" width="1000" height="600">
    </p>



* **Perception Layer.** 
    On the perception layer we clasify all teh objects/endpoints. These are the body of this system, without them we can not collect data or controll devices. In this senario we have used raspberry pi as endpoints. But generally there is no limitation on what device you can use as endpoint. The only limitations are that it has to have the capability to execute at least code on C/C++ and have a valid way of send a recieve data such as bluetouth, wify and so on. With theese 2 limitations satisfied, the only think that limits you is your imagination and your usecase.

*  **Transport layer.**
    On this layer we define the transportation mechanism of our data. With what infastracture our data will flow from and off the endpoints. In this particular situation we have used the standart IPv4 throug a router acting as gateway. But this can be modified to use infastractures such as bluetouth etc.

*  **Midlware layer**. 
    This layer is a very controversonal layer across the researchers. In this use case this layer consist of an open source IoT platform named [Kaa](https://www.kaaproject.org/), 2 rest endpoints, one No-SQL database caled [Cassandra](http://cassandra.apache.org/) and one standar [Maria DB](https://mariadb.org/) for MySQL db.

*  **Application layer**. 
    This is the fourth and final layer of our architecture. In this layer we define the applications that the system supports. In order for the system to be used from users we need some kind of HMI. In this particular case a dashboard & an Android application has been developed in order to bring the services that the system provides to a more user friendly and convininet way through a Graphical User Interface. Through the dashboard the user has the abilty to controll the system overal & see real & historical measurments.


*More details about the system architecture can be found  [here](./architecture.md).*

**System components.**

This system consists of a number of different componets. This componets essentially are subsystems that all together constract the overal system. In this part, we will discuss what theese systems are and how each one of them opperates. We will start the subsystem analization from the botom to top. We will start from the first architecture layer the one that controlls the endpoints and we will work our way to the top.

*    **Hardware**

1.    A machine with at least 4 cores and 8 gigabytes of ram. On  this machine we need to install 
      prefferably a linux server distribution. This system has been tested on server instances of  
      operating systems ubuntu/debian. The installation instactions provided are for debian based 
      distributions but this system is also capable of running on windows if you make the wright 
      adjustments. This machine must prefferably have a static public ip and a strong insternet 
      connection with nice upload speeds. We need static ip & upload in order to be able to 
      controll the system remotely. If we don't want to controll the system and therefore the
      house remotely there is no need of theese tow, in that case we can have a static private 
      network ip. The network on wich we will attach the system will be the network of the house.

2.    Prefferably tow raspberry pi or an equelevant piece of hardware that is capable of run 
      java/c/c++ and have a network interface that can support at least the IPv4  protocol.
      This system can opperate under different networking protocols also but in this particular
      senario we have used the IPv4 protocol inorder to connect the subsystems as an overal system.

3.    A variety of different sensors that monitors the leaving conditions in the house. On this 
      senario we have focused on air quality, the temprature and the humidity of the house. 
      But the only limitation is your imagination. For the complete list of sensors used in this
      implementation you can click [here](http://ec2-52-47-144-179.eu-west-3.compute.amazonaws.com/thesis/sensors_list/).

4.    A router for the private network of the house. We recomend this router to run [ddwrt](https://dd-wrt.com/) firmaware.
      In case we decide that we want the remote system's controll we need to attach this router to
      a a bridge with access to the overal network (WWW). In that case we will need ofcourse a static
      ip or a ddns system that will provide as a dns that will automaticaly refresses our ip. OpenDNS 
      is a good system that can provide this service.


*    **Software**

1.   First of all we will need at least one running instance of the IoT platform's server. This platform is 
     the open-source [IoT platform Kaa](https://github.com/AbstractVersion/kaa). Instractions about it's installation can be found [here](installation.md). 
     This platform is to be used in order for the system to gather the measurments and the events 
     from our different endpoints. After the system obtains the measurments it distributes them to
     a NoSQL database in this case we have used Cassandra DB but there are options for other simmilar
     data gathering systems such as MongoDB, kafka and postgresSQL.

2.   A running instance of Cassandra DB cluster with at least one node. In order to store the measurments 
     and the events obtained by the endpoints. More information about the configuration of the Cassandra DB
     for our senario can be found here.

3.   An [Apache Tomee aplication server](https://tomee.apache.org/) or something equilevant, in order to host the REST APIs we have. 
     The REST APIs are written in java so we need an application server capable to run java. We have used Tomee 
     but other application servers such as glassfish will do the trick.

4.   An [Apache HTTP server](https://httpd.apache.org/) or somehting equilevant. On this server we will host the dashboard of our system.
     The dashboard is written in HTML/CSS/JS so every http server will do the trick too. More information about the dashboard you can find here.

5.   In order to gather the data from the endpoints we use the sdk that the IoT platform provides as. 
     This sdk essentially creates buckets with payloads of data and periodically uploads them to
     the server/s. We have 2 different software for the endpoints. The gass-collector software and the 
     the themprature/humidity-collector.

6.   The [humidity/temprature-collector](https://gitlab.com/abs_portfolio/iot-smart-home/tree/master/kaa-endpoint-aps) software is a code written in c. 
     It's main responsibility is to gather the data from the dht22 sensor about the temprature & humidity and distribute them by using
     the sdk to the server/s. We read the sensor measurments using the wiringPi library. In case that we do not have the wright equipment we 
     can use the simulated apps. These apps will generate random values for temprature & humidity and can be found [here](https://gitlab.com/abs_portfolio/iot-smart-home/tree/master/kaa-endpoint-aps/simulation).

7.   The [gass-collector software](https://gitlab.com/abs_portfolio/iot-smart-home/tree/master/c-py-sockets). As we mentioned in order to obtain the air-conditions we use a variaty of different sensors
     from the MQ fammily. This data collection system is seperated in tow parts. On the translator and on the distributor. 
     The translator is written in python and it's main job is to collect the measurments from the sensors and translate the 
     electrical signal that the sensors produce to a more efficient measurment such as a float variable. This system is written
     in python. Other than being a translator it is also a socket client. Which brigs as to the second part of our gass-collector,
     the data-distributor. This is essentially a socket server which is used to distribute the measurments obtained from the 
     translator to the IoT server. In case you don't have the apropriate hardware, you can use the simulator we have used for the
     air-quality measurments which can be found [here](https://gitlab.com/abs_portfolio/iot-smart-home/tree/master/kaa-endpoint-aps/simulation/air-check).

8.   The [device controll & automation software](https://gitlab.com/abs_portfolio/iot-smart-home/tree/master/device_controll%20&%20automation). This software is essentially a socket server written in java.
     This server is responsible for the smart device controll & the automation. The devices unfortunatelly are simulated with leds on this 
     senario. The automation system is controlled by the user and works with crutial values. 

9.   The tow REST endpoints we have created. Theese tow APIs are used in order to distribute the data 
     from the Cassandra DB & provide the device controll services over http.

10.  The first REST API is the [cass_prod](https://gitlab.com/abs_portfolio/iot-smart-home/tree/master/cassandra-rest-endpoint) which is responsible for the data distribution from the database to the services that request them.
     

11.  The second endpoint is the [device-rest-endpoint](https://gitlab.com/abs_portfolio/iot-smart-home/tree/master/device-rest-endpoint) which is used to provide the
     services of user managment as well as the services of device controll & automation to the users & devices. On this endpoint we implement the client of the 
     socket server which is responsible for the device controll and automation. That way we distribute the services of automation & device controll over http/https.

12.  Finally we have the [dashboard](https://gitlab.com/abs_portfolio/iot-smart-home/tree/master/dashboard) which is written on HTML/CSS/JS. This dashboard is used to 
     in order to distribute to the user all the above services in a more convinient way to the user through a graphical interface. Serivices provided by the dashboard are :
     system's state, live-enviroment monitoring, historical search and visualize, device control, automation & feature extraction. 


**GUI Demonstration.**
As mentioned our system offers a number of different services to the users. Such as enviroment monitoring, data visualization, device controll, automation etc. In order to bring 
theese services to the users in a more convinient way, this system ofers a dashboard from which the user can controll the house and use the services mentioned. This dashboard is based
on HTML/CSS/JS and it consumes the web-services mentioned above in order to bring to life the hole system. Below you can see the screens of the dashboard as whell as a short discription
about their usage.

1.  The first screen is the welcome screen or home screen. On this screen we can see on top 6 icons. Each one of this icons represents
    the services we described above. In cases the service is active it has a greeen tick upon it, if the service is not active it has a red 
    block. The first icon represents the cloud server, the second represents the Cassandra claster, the third the mysql db, the forth represents
    the data collection system, the fifth represents the device controller & automation system and the sixth represents the REST API's.
    Beyond that we can see a thermomiter and a gauce chart. Theese charts are displaying the current temprature & humidity of a specific area 
    of the house (Bedroom, Living Room, Kitchen etc.).

    <p align="center">
    <img src="./images/dashboard/dash1.jpg" alt="home_screen" width="1000" height="400">
    </p>
    
    Next beyond those charts we can see a chart that displays the current measurments for the air quality of a specific area of the house. Every point of the x axis represents the time that
    the measurment was gained and each point of the y axis represents the value of the measurment for each gass.
    
    <p align="center">
    <img src="./images/dashboard/dash2.png" alt="home_screen" width="1000" height="400">
    </p>
    
2.  In the next screen we can see the overal data visualization of the system. The overal data from the system are beeing displayied with charts. The data are grouped by area that
    way a user can see the differences in each measurments by area. The first chart represents the overal average daily temprature, the second visualizes the humidity and the third one
    the air quality.

    <p align="center">
    <img src="./images/dashboard/dash5.png" alt="home_screen" width="1000" height="400">
    </p>
    
    <p align="center">
    <img src="./images/dashboard/dash6.png" alt="home_screen" width="1000" height="400">
    </p>
    
    
3.  Next we can see the overal air quality data visualization. We have used a chart provied by e-cahrts.   

    <p align="center">
    <img src="./images/dashboard/dash4.png" alt="home_screen" width="1000" height="400">
    </p>
    
4.  The on demand search & visualize screen for the histrorical data of the system. In this screen the user can search & visualize on demand data. Also the system provides the abolity
    of feature extraction and data labeling to the user in form of csv for data analysis perposes.
    

    <p align="center">
    <img src="./images/dashboard/dash8.png" alt="home_screen" width="1000" height="200">
    </p>
    <p align="center">
    <img src="./images/dashboard/dash9.png" alt="home_screen" width="1000" height="400">
    </p>
    <p align="center">
    <img src="./images/dashboard/dash10.png" alt="home_screen" width="1000" height="400">
    </p>
    
5.  Next screen shows the device controller interface. From this interface the user can controll the devices of his house. 
    In this case we have registered as devices the lights, the exaust and the radiator of the house.

    <p align="center">
    <img src="./images/dashboard/dash11.png" alt="home_screen" width="1000" height="400">
    </p>
6.  Last but not least we have the automation controller interface. From this interface the user can controll the automation of his/her house. The automation can be turned on/off and it
    works with critical values. Once the user set those critical values and activates the automation, the system will maintain the leaving condition between those critical values.
    

    <p align="center">
    <img src="./images/dashboard/dash12.png" alt="home_screen" width="1000" height="400">
    </p>

**Model of the house**
<p align="center">
    <img src="./images/smart_home.png" alt="home_screen" width="900" height="600">
</p>

**Video Demo**
For the above mentioned services there is a video demonstration as whell .

Video by sections :

*  Home screen presentation
http://www.youtube.com/watch?v=-0gzgBTAmA4&t=2m10s

* Temprature & Humidity data visualization
http://www.youtube.com/watch?v=-0gzgBTAmA4&t=7m13s

* Overal Air Quality data visualization
http://www.youtube.com/watch?v=-0gzgBTAmA4&t=7m30s
 
* Device Controll
http://www.youtube.com/watch?v=-0gzgBTAmA4&t=7m52s
 
* Automation 
http://www.youtube.com/watch?v=-0gzgBTAmA4&t=8m29s

    



  
