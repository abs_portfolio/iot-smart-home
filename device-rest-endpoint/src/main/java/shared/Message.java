/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shared;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author onelove
 */
public class Message implements Serializable {

    private Auto_Controler_config config;
    private Device device;
    private Actions action;
    private Region region;
    private int ttl;
    private String responce;
    private boolean active;

    private List<Sinfo> infoList;

    public Message(Actions action, List<Sinfo> infoList) {
        this.infoList = infoList;
        this.action = action;
    }

    public Message(Actions action, Auto_Controler_config temp) {
        this.config = temp;
        this.action = action;
    }

    public Message(Device device, Actions action, Auto_Controler_config temp) {
        this.device = device;
        this.config = temp;
        this.action = action;
    }

    public Message(Actions actions) {
        this.action = actions;
    }

    public List<Sinfo> getInfoList() {
        return infoList;
    }

    public void setInfoList(Actions action, List<Sinfo> infoList) {
        this.infoList = infoList;
    }

    public boolean isActive() {
        return active;
    }

    public Message(String responce) {
        this.responce = responce;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Message(String responce, boolean active) {
        this.responce = responce;
        this.active = active;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public void setResponce(String responce) {
        this.responce = responce;
    }

    public String getResponce() {
        return responce;
    }

    public Message(Device device, Actions action, Region region, int ttl) {
        this.device = device;
        this.action = action;
        this.region = region;
        this.ttl = ttl;
    }

    public Actions getAction() {
        return action;
    }

    public int getTtl() {
        return ttl;
    }

    public void setTtl(int ttl) {
        this.ttl = ttl;
    }

    public Region getRegion() {
        return region;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public void setAction(Actions action) {
        this.action = action;
    }

    public Device getDevice() {
        return device;
    }

    public Message(Device dev, Actions act) {
        this.device = dev;
        this.action = act;
    }

    public Message(Device dev, Actions act, Region reg) {
        this.region = reg;
        this.device = dev;
        this.action = act;
    }

    public Auto_Controler_config getConfig() {
        return config;
    }

    public void setConfig(Auto_Controler_config config) {
        this.config = config;
    }

    public void setInfoList(List<Sinfo> infoList) {
        this.infoList = infoList;
    }

    public Message(Auto_Controler_config config) {
        this.config = config;
    }

}
