/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.services;

import com.api.essentials.KaaApi_URL_Collection;
import static com.api.essentials.NetClientGet.requestFromKaaServer;
import com.api.model.User_Tempalte;
import com.google.gson.Gson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import java.io.IOException;
import static java.nio.file.attribute.AclEntryPermission.DELETE;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import sun.misc.BASE64Decoder;

/**
 *
 * @author onelove
 */
@Path("secured/user")
@Api(value = "User Service")
@Produces({"application/json", "application/xml"})
@SwaggerDefinition(info = @Info(
        description = "User managment",
        version = "V1.0",
        title = "The user managment Api",
        termsOfService = "something",
        contact = @Contact(
                name = "Georgios Fiotakis",
                email = "g.fiotakis@akka.eu",
                url = "https://www.linkedin.com/in/george-fiotakis-320967159/"
        ),
        license = @License(
                name = "Apache 2.0",
                url = "http://www.apache.org/licenses/LICENSE-2.0"
        )
),
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"},
        schemes = {SwaggerDefinition.Scheme.HTTP, SwaggerDefinition.Scheme.HTTP},
        tags = {
            @Tag(name = "User Service", description = "REST Endpoint for user managment. The users are the same as the ones on the IoT platform Kaa. Therefore they are stored in the same database which named kaa.")})
public class Service {

    private static final Gson json = new Gson();

    @GET
    @Path("/auth")
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Using Basic Authorization (provide the user credentials on header with base 64 encode). We authorize a user action.",
            notes = "Json with the users Details",
            response = Response.class,
            responseContainer = "User Object")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    public Response authorization(@HeaderParam("authorization") String authString) {
        ArrayList<String> list = getUname(authString);
        String answer = requestFromKaaServer(list.get(0), list.get(1), KaaApi_URL_Collection.getAuthUrl());
        return Response.ok(answer, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/userProfile")
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Using Basic Authorization (provide the user credentials on header with base 64 encode). We authorize the user and return his basic profile information NOT PASSWORD. The user returned is the one who mentoned on basic auth credentials.",
            notes = "Json with the users Details",
            response = Response.class,
            responseContainer = "User Object")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    public Response get_user_profile(@HeaderParam("authorization") String authString) {
        ArrayList<String> list = getUname(authString);
        System.out.println("het");
        String answer = requestFromKaaServer(list.get(0), list.get(1), KaaApi_URL_Collection.getProfile_url());
        if (answer.contains("Failed")) {
            return Response.status(Response.Status.UNAUTHORIZED).entity("User Can not access the resource status : " + answer).build();
        }
        return Response.ok(answer, MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/create_edit_usr")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Using Basic Authorization (provide the user credentials on header with base 64 encode). We create a user based on the json provided. If the json contains the user Id of a certific user, then the user will be eddited with the informaton provided. Only a user with the role TENAND_ADMIN can use this service.",
            notes = "Json with the users Details and the temp password return by the service. The user must login to the kaa dashboard or to our dashboard and change the temp password.",
            response = Response.class,
            responseContainer = "User Object")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    public Response create_user(@HeaderParam("authorization") String authString, User_Tempalte user) throws IOException {
        ArrayList<String> list = getUname(authString);
        String answer = requestFromKaaServer(list.get(0), list.get(1), KaaApi_URL_Collection.createUser(), user);
        if (answer.contains("Failed")) {
            return Response.status(Response.Status.UNAUTHORIZED).entity("User Can not access the resource status : " + answer).build();
        }
        return Response.ok(answer, MediaType.APPLICATION_JSON).build();
    }

//    @DELETE
//    @Path("/del_usr")
//    @Produces({MediaType.APPLICATION_JSON})
//    @Consumes({MediaType.APPLICATION_JSON})
//    public Response delete_usr(@HeaderParam("authorization") String authString) throws IOException {
//        ArrayList<String> list = getUname(authString);
//        String answer = requestFromKaaServer(list.get(0), list.get(1), KaaApi_URL_Collection.getProfile_url());
//        if (answer.contains("Failed")) {
//            return Response.status(Response.Status.UNAUTHORIZED).entity("User Can not access the resource status : " + answer).build();
//        }
//        return Response.ok(answer, MediaType.APPLICATION_JSON).build();
//    }
    private static final ArrayList<String> getUname(String authString) {
        String decodedAuth = "";
        String[] authParts = authString.split("\\s+");
        String authInfo = authParts[1];
        byte[] bytes = null;
        try {
            bytes = new BASE64Decoder().decodeBuffer(authInfo);
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        ArrayList<String> res = new ArrayList<String>();
        decodedAuth = new String(bytes);
        System.out.println(decodedAuth);
        StringTokenizer tockenizer = new StringTokenizer(new String(decodedAuth), ":");
        res.add(tockenizer.nextToken());
        res.add(tockenizer.nextToken());
        return res;
    }

}
