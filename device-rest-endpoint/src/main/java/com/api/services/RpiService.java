/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.services;

import com.api.essentials.Rpi_client;
import com.api.model.Rpi_details;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import shared.Actions;
import com.api.model.Auto_Controler_config_model;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import shared.Auto_Controler_config;
import shared.Device;
import shared.Message;
import shared.Region;
import shared.Sinfo;

/**
 *
 * @author onelove
 */
@Path("secured/rpi")
@Api(value = "Controller & Automation")
@Produces({"application/json", "application/xml"})
@SwaggerDefinition(info = @Info(
        description = "Controller & Automation",
        version = "V1.0",
        title = "Controller & Automation Api",
        termsOfService = "something",
        contact = @Contact(
                name = "Georgios Fiotakis",
                email = "g.fiotakis@akka.eu",
                url = "https://www.linkedin.com/in/george-fiotakis-320967159/"
        ),
        license = @License(
                name = "Apache 2.0",
                url = "http://www.apache.org/licenses/LICENSE-2.0"
        )
),
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"},
        schemes = {SwaggerDefinition.Scheme.HTTP, SwaggerDefinition.Scheme.HTTP},
        tags = {
            @Tag(name = "Controller & Automation", description = "REST Endpoint that forwards the device controller services and the automation configuration services over the HTTP protocol.")})
public class RpiService {

    private final static Gson jsonfy = new Gson();

    @POST
    @Path("/info")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "This service provides the information of the controller (Operating System, Hardware details etc...)",
            notes = "In order to recieve this infomation the ip and the port of the device that the controller must be specified.",
            response = Message.class,
            responseContainer = "Message Objcet")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    public Response getSystemInfo(Rpi_details json) throws IOException {
        Rpi_client client = new Rpi_client(json.getIp(), Integer.parseInt(json.getPort()));
        Message msg = client.generateInfoRequest(Actions.SYS_INFO, generateInfoSet(true, true, true, true, true, true));
        Message resp = client.tongle_request(msg);
        return Response.ok(resp.getResponce(), MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/devInfo")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "This service provides the information about all the devices attached to the controller (on , off ,etc..). In order to get those information the device must be registered to the controller.",
            notes = "In order to recieve this infomation the ip and the port of the device that the controller must be specified. ",
            response = Message.class,
            responseContainer = "Message Objcet")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    public Response getDeviceInfo(Rpi_details json) throws IOException {
        try {
            Rpi_client client = new Rpi_client(json.getIp(), Integer.parseInt(json.getPort()));
            Message msg = client.generateSimpleActionReq(Actions.DEVICE_INFO);
            Message resp = client.tongle_request(msg);
            return Response.ok(resp.getResponce(), MediaType.APPLICATION_JSON).build();
        } catch (java.lang.NullPointerException ex) {
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();
        }
    }

    @POST
    @Path("/trigerDevice")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "This service is responsible for a device action. In order to switch on/off a device we use this service.",
            notes = "In order to recieve this infomation the ip and the port of the device that the controller must be specified. Additionally we must specify the device we want to interract (radiator,exaust,lights, automation) , the region of the device and the action we want (on/off).",
            response = Message.class,
            responseContainer = "Message Objcet")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    public Response trigerDevice(Rpi_details json) throws IOException {
        Rpi_client client = new Rpi_client(json.getIp(), Integer.parseInt(json.getPort()));
        Message msg = client.generatActionRequest(json.getDevice(), json.getAction(), json.getRegion());
        Message resp = client.tongle_request(msg);
        return Response.ok(resp.getResponce(), MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/trigerDeviceTtl")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "This service is responsible for a device action over a specific TTL. In order to switch on a device for a specific period of time, we use this service.",
            notes = "In order to recieve this infomation the ip and the port of the device that the controller must be specified. Additionally we must specify the device we want to interract (radiator,exaust,lights, automation) , the region of the device, the action we want (on/off) as well as the TTL of the activation.",
            response = Message.class,
            responseContainer = "Message Objcet")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    public Response trigerPeriodDevice(Rpi_details json) throws IOException {
        Rpi_client client = new Rpi_client(json.getIp(), Integer.parseInt(json.getPort()));
        Message msg = client.generateTtlActiveRequest(json.getDevice(), json.getAction(), json.getRegion(), Integer.parseInt(json.getTtl()));
        Message resp = client.tongle_request(msg);
        return Response.ok(resp.getResponce(), MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/auto_config_info")
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "This service returns the configuration of the automation controller.",
            notes = "In order to recieve this infomation the ip and the port of the device that the controller must be specified.",
            response = Message.class,
            responseContainer = "Message Objcet")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    public Response get_automation_configuration(Rpi_details json) throws IOException {
        Rpi_client client = new Rpi_client(json.getIp(), Integer.parseInt(json.getPort()));
        Message msg = client.genareateAutomationConfigurationMessage(json.getAction());
        Message resp = client.tongle_request(msg);
        System.out.println(resp.getResponce());
        return Response.ok(resp.getResponce(), MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/trigerAuto")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "This service enables or desables the automation controller.",
            notes = "In order to recieve this infomation the ip and the port of the device that the controller must be specified. Also we need to specify the device as automation and the action accordinarlly (on/off).",
            response = Message.class,
            responseContainer = "Message Objcet")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    public Response trigerAutomation(Rpi_details json) throws IOException {
        Rpi_client client = new Rpi_client(json.getIp(), Integer.parseInt(json.getPort()));
        Message msg = client.generatActionRequest(json.getDevice(), json.getAction());
        Message resp = client.tongle_request(msg);
        return Response.ok(resp.getResponce(), MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/auto_config")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "This service allows to a user to configure the automation controller. (mainly the critical values of it).",
            notes = "In order to recieve this infomation the ip and the port of the device that the controller must be specified. Also we need to specify the device as automation and the action auto_config and the configuration json accordinarlly.",
            response = Message.class,
            responseContainer = "Message Objcet")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "We Was not able to identify the resource")
        ,
        @ApiResponse(code = 403, message = "Unothorized action, please provide a valid username & password or a valid token.")
        ,
        @ApiResponse(code = 500, message = "Something wrong in Server")})
    public Response configure_automation_controler(Auto_Controler_config_model json) throws IOException {
        Rpi_client client = new Rpi_client(json.getIp(), Integer.parseInt(json.getPort()));
        Message msg = client.constractConfigurationRequest(json.getDevice(), json.getAction(), decodeConfiguration(json));
        Message resp = client.tongle_request(msg);
        return Response.ok(resp.getResponce(), MediaType.APPLICATION_JSON).build();
    }

    private Auto_Controler_config decodeConfiguration(Auto_Controler_config_model config) {
        config.setAction(null);
        config.setIp(null);
        config.setPort(null);
        config.setDevice(null);
        return jsonfy.fromJson(jsonfy.toJson(config), Auto_Controler_config.class);
    }

    private List<Sinfo> generateInfoSet(boolean platform, boolean cpu, boolean memeory, boolean os, boolean network, boolean hardware) {
        List<Sinfo> list = new ArrayList<Sinfo>();
        if (platform) {
            list.add(Sinfo.PLATFORM);
        }
        if (cpu) {
            list.add(Sinfo.CPU);
        }
        if (memeory) {
            list.add(Sinfo.MEMORY);
        }
        if (os) {
            list.add(Sinfo.OS);
        }
        if (network) {
            list.add(Sinfo.NETWORK);
        }
        if (hardware) {
            list.add(Sinfo.HARWARE);
        }
        return list;
    }
}
