/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.filter;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author onelove
 */
@Provider
public class CrossOriginResourceSharingFilter implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext response) {
        response.getHeaders().putSingle("Access-Control-Allow-Origin", requestContext.getHeaderString("origin"));
//        response.getHeadsers().putSingle("Access-Control-Allow-Origin", "http://185.244.128.27/");
        response.getHeaders().putSingle("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, DELETE");
//        response.getHeaders().putSingle("Access-Control-Allow-Headers", true);
//        response.getHeaders().putSingle("Access-Control-Allow-Headers", "xyz");
//        response.getHeaders().putSingle("Access-Control-Allow-Headers", "Authorization");
        response.getHeaders().putSingle("Access-Control-Allow-Credentials", true);
//        response.getHeaders().putSingle("Access-Control-Expose-Headers", "token");
//        response.getHeaders().putSingle("Access-Control-Expose-Headers", "user");
//        response.getHeaders().putSingle("Access-Control-Expose-Headers", "Authorization");
        response.getHeaders().putSingle("Access-Control-Max-Age", 3628800);
        response.getHeaders().putSingle("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization,token,Application/json");
//        response.getHeaders().putSingle("Access-Control-Allow-Headers", "");
//        response.getHeaders().putSingle("Access-Control-Allow-Headers", "");
    }

}
