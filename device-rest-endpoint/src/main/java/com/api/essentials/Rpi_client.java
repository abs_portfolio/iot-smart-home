/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.essentials;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import shared.Actions;
import static shared.Actions.PERIOD_ACTIVATE;
import static shared.Actions.QUIT;
import static shared.Actions.START;
import static shared.Actions.STOP;
import shared.Auto_Controler_config;
import shared.Device;
import shared.Message;
import shared.Region;
import shared.Sinfo;

/**
 *
 * @author onelove
 */
public class Rpi_client {

    private static Socket clientSocket = null;
    private static ObjectInputStream is = null;
    private static ObjectOutputStream os = null;
    private String ip;
    private int port;

    public Rpi_client(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public Message generateInfoRequest(Actions action, List<Sinfo> infoList) {
        return new Message(action, infoList);
    }

    public Message constractConfigurationRequest(String device, String action, Auto_Controler_config config) {
        return new Message(getDeviceFromString(device), getActionFromString(action), config);
    }

    public Message generatActionRequest(String device, String action) {
        return new Message(getDeviceFromString(device), getActionFromString(action));
    }

    public Message generateSimpleActionReq(Actions action) {
        return new Message(action);
    }

    public Message genareateAutomationConfigurationMessage(String req) {
        return new Message(getActionFromString(req));
    }

    public Message generateTtlActiveRequest(String device, String action, String region, int ttl) {
        return new Message(getDeviceFromString(device), getActionFromString(action), getRegionFromString(region), ttl);
    }

    public Message generatActionRequest(String dev, String act, String reg) {
        return new Message(getDeviceFromString(dev), getActionFromString(act), getRegionFromString(reg));
    }

    private Actions getActionFromString(String action) {
        if (action.equalsIgnoreCase("start")) {
            return Actions.START;
        } else if (action.equalsIgnoreCase("stop")) {
            return Actions.STOP;
        } else if (action.equalsIgnoreCase("quit")) {
            return Actions.QUIT;
        } else if (action.equalsIgnoreCase("ttl")) {
            return Actions.PERIOD_ACTIVATE;
        } else if (action.equalsIgnoreCase("dev_info")) {
            return Actions.DEVICE_INFO;
        } else if (action.equalsIgnoreCase("sys_info")) {
            return Actions.SYS_INFO;
        } else if (action.equalsIgnoreCase("auto_config")) {
            return Actions.Config;
        }else if (action.equalsIgnoreCase("auto_info"))
            return Actions.Config_info;
        return null;
    }

    private Device getDeviceFromString(String device) {
        if (device.equalsIgnoreCase("radiator")) {
            return Device.Radiator;
        } else if (device.equalsIgnoreCase("exaust")) {
            return Device.Exaust;
        } else if (device.equalsIgnoreCase("lights")) {
            return Device.Lights;
        } else if (device.equalsIgnoreCase("automation")) {
            return Device.Automation;
        }
        return null;
    }

    private Region getRegionFromString(String device) {
        if (device.equalsIgnoreCase("bedroom")) {
            return Region.BEDROOM;
        } else if (device.equalsIgnoreCase("kitchen")) {
            return Region.KITCHEN;
        } else if (device.equalsIgnoreCase("living_room")) {
            return Region.LIVING_ROOM;
        }
        return null;
    }

    public Message tongle_request(Message msg) throws IOException {
        initializeConnection();
        Message message = null;
        if (clientSocket != null && os != null && is != null) {
            try {
                System.out.println("The client started.");
                os.writeObject(msg);
                os.flush();
                message = (Message) is.readObject();
                if (message != null) {
                    return message;
                }
            } catch (UnknownHostException e) {
                System.err.println("Trying to connect to unknown host: " + e);
            } catch (IOException e) {
                System.err.println("IOException:  " + e);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Rpi_client.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                os.close();
                is.close();
                clientSocket.close();

            }
        }
        return null;
    }

    private void initializeConnection() {

        try {
            clientSocket = new Socket(ip, port);

            os = new ObjectOutputStream(clientSocket.getOutputStream());
            is = new ObjectInputStream(clientSocket.getInputStream());

        } catch (UnknownHostException e) {
            System.err.println("Don't know about host");
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to host");
        }
    }
}
