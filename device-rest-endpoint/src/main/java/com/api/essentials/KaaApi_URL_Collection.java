/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.essentials;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author onelove
 */
public class KaaApi_URL_Collection {

    private final static String kaa_ip = "185.244.128.27";
    private final static String kaa_port = "8080";
    public static URL authUrl;
    public static URL profile_url;

    public static URL getAuthUrl() {
        try {
            return new URL("http://" + kaa_ip + ":" + kaa_port + "/kaaAdmin/rest/api/auth/checkAuth");
        } catch (MalformedURLException ex) {
            Logger.getLogger(KaaApi_URL_Collection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static URL getProfile_url() {
        try {
            return new URL("http://" + kaa_ip + ":" + kaa_port + "/kaaAdmin/rest/api/userProfile");
        } catch (MalformedURLException ex) {
            Logger.getLogger(KaaApi_URL_Collection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static URL createUser() {
        try {
            return new URL("http://185.244.128.27:8080/kaaAdmin/rest/api/user");
        } catch (MalformedURLException ex) {
            Logger.getLogger(KaaApi_URL_Collection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
