/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author onelove
 */
@XmlRootElement
public class Rpi_details {

    @XmlElement
    private String ip;
    @XmlElement
    private String port;
    @XmlElement
    private String device;
    @XmlElement
    private String region;
    @XmlElement
    private String action;
    @XmlElement
    private String ttl;

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    public String getTtl() {
        return ttl;
    }

    public String getDevice() {
        return device;
    }

    public String getRegion() {
        return region;
    }

    public String getAction() {
        return action;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Rpi_details(String ip, String port) {
        this.ip = ip;
        this.port = port;
    }

    public Rpi_details() {
    }

    public String getIp() {
        return ip;
    }

    public String getPort() {
        return port;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(String port) {
        this.port = port;
    }

}
