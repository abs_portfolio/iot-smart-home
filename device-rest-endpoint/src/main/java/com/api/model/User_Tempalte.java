/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author onelove
 */
@XmlRootElement
public class User_Tempalte {

    @XmlElement
    private String username;
    @XmlElement
    private String firstName;
    @XmlElement
    private String lastName;
    @XmlElement
    private String mail;
    @XmlElement
    private String pwd;
    @XmlElement
    private String authority;
    @XmlElement
    private String tenantId;

    public User_Tempalte() {

    }

    public User_Tempalte(String username, String name, String surname, String email, String pwd, String authority, String tenand_id) {
        this.username = username;
        this.firstName = name;
        this.lastName = surname;
        this.mail = email;
        this.pwd = pwd;
        this.authority = authority;
        this.tenantId = tenand_id;
    }

    public User_Tempalte(String username, String email, String pwd) {
        this.username = username;
        this.mail = email;
        this.pwd = pwd;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMail() {
        return mail;
    }

    public String getPwd() {
        return pwd;
    }

    public String getAuthority() {
        return authority;
    }

    public String getTenantId() {
        return tenantId;
    }

}
