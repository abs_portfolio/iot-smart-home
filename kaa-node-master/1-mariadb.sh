
# ubuntu PPA for official MariaDB 10.3
# sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
# sudo add-apt-repository 'deb [arch=amd64,arm64,ppc64el] http://ftp.cc.uoc.gr/mirrors/mariadb/repo/10.3/ubuntu bionic main'
#
# Ubuntu 14.04  - Trusty
# Ubuntu 16.04  - Xenial
# Ubuntu 18.04  - Bionic
# Debian 10     - Buster
# Debian 9      - Stretch
# Debian 8      - Jessie

sudo apt remove mariadb*
sudo apt autoremove

sudo apt update
sudo apt install -y mariadb-server

# Installing rsync
sudo apt-get install -y rsync

# Checking if mysql is running on localhost:3306
netstat -ntlp | grep 3306

# Open MySQL port (ufw must be installed)
sudo ufw allow 3306

# Connect as root in mariadb

# sudo mysql
# CREATE USER 'sqladmin'@'localhost' IDENTIFIED BY 'admin'; GRANT ALL PRIVILEGES ON *.* TO 'sqladmin'@'localhost' WITH GRANT OPTION; FLUSH PRIVILEGES;
# CREATE DATABASE kaa;
# GRANT ALL PRIVILEGES ON *.* TO 'sqladmin'@'185.244.128.27' IDENTIFIED BY 'admin' WITH GRANT OPTION;

sudo mysql -e "CREATE USER 'sqladmin'@'localhost' IDENTIFIED BY 'admin'; GRANT ALL PRIVILEGES ON *.* TO 'sqladmin'@'localhost' WITH GRANT OPTION; FLUSH PRIVILEGES;"
sudo mysql -e "CREATE DATABASE kaa;"
sudo mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'sqladmin'@'185.244.128.27' IDENTIFIED BY 'admin' WITH GRANT OPTION;"

# Needs authentication, better reboot
# systemctl daemon-reload

# !!
# tested on presentation server on April 17, 1019 (Debian)
