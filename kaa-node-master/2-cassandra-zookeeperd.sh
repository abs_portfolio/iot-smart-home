
# zookeeper
sudo apt-get install -y zookeeperd
netstat -ntlp | grep 2181

# cassandra keys
echo "deb http://www.apache.org/dist/cassandra/debian 311x main" | sudo tee -a /etc/apt/sources.list.d/cassandra.sources.list
curl https://www.apache.org/dist/cassandra/KEYS | sudo apt-key add -
sudo apt-key adv --keyserver pool.sks-keyservers.net --recv-key A278B781FE4B2BDA
sudo apt-get update

# Install Cassandra
sudo apt-get install -y cassandra

sudo ufw allow 9042

# Hard reset after service stop to flush out old .YAML
# cd /etc/cassandra
# sudo cassandra -f -R

# !!
# tested on presentation server on April 17, 1019 (Debian)
