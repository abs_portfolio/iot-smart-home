
# SED HELP
# replace string
# sed -i 's/aaa=bbb/aaa=xxx/g' testfile

# replace line containing string
# sed -i 's/aaa=.*/aaa=xxx/g' testfile
# sed -i 's/aaa=/c\aaa=xxx' testfile

host_ip_local=$(echo "192.168.1.66")
host_ip_global=$(echo "185.244.128.27")

sudo ./control/0_stop.sh

# Change /etc/cassandra/cassandra.yaml
# https://gist.github.com/andykuszyk/7644f334586e8ce29eaf8b93ec6418c4
sudo sed -i 's/start_rpc:.*/start_rpc: true/g' /etc/cassandra/cassandra.yaml
sudo sed -i 's/rpc_address:.*/rpc_address: 0.0.0.0/g' /etc/cassandra/cassandra.yaml
sudo sed -i 's/broadcast_rpc_address:/broadcast_rpc_address: '$host_ip_global'/g' /etc/cassandra/cassandra.yaml
sudo sed -i 's/listen_address:/listen_address: '$host_ip_global'/g' /etc/cassandra/cassandra.yaml
sudo sed -i 's/seeds:/          - seeds: '$host_ip_global'/g' /etc/cassandra/cassandra.yaml

# Change /etc/kaa-node/conf/kaa-node.properties
# thrift_host=$MACHINE_IP
sudo sed "s/thrift_host=/thrift_host='$host_ip_global'/g" /etc/kaa-node/conf/kaa-node.properties

# Change /etc/mysql/my.cnf
# comment-out both bind-addresses
sudo sed -n '/bind-address/c\# bind DISABLED' /etc/mysql/my.cnf



# from old file kaa-ip
# in /etc/kaa-node/conf/kaa-node.properties
# change transport_public_interface to the ip of kaa-node machine's ip
