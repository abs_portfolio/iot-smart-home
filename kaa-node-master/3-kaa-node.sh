# Place tar.gz file in directory if downloading (190mb) is not an option
# echo "MAKE SURE YOU COPIED kaa-deb.tar.gz IN THE SCRIPT FOLDER (./kaa-node/)"

# Download node image
wget -O kaa-deb-0.10.tar.gz https://www.dropbox.com/s/wgayedqpw9duhmi/kaa-deb-0.10.tar.gz?dl=0

# Command to unpack .tar.gz
tar -xvf kaa-deb*.tar.gz

# Command to install .deb
sudo dpkg -i ./deb/kaa-node*.deb

# Clean up
rm kaa-deb*
rm -rf deb

# Kaa-node configuration file attributes
sudo cat /etc/kaa-node/conf/admin-dao.properties | grep jdbc_username
sudo cat /etc/kaa-node/conf/admin-dao.properties | grep jdbc_password
sudo cat /etc/kaa-node/conf/sql-dao.properties | grep jdbc_username
sudo cat /etc/kaa-node/conf/sql-dao.properties | grep jdbc_password

# Iptables rules
sudo iptables -I INPUT -p tcp -m tcp --dport 22 -j ACCEPT
sudo iptables -I INPUT -p tcp -m tcp --dport 8080 -j ACCEPT
sudo iptables -I INPUT -p tcp -m tcp --dport 9888 -j ACCEPT
sudo iptables -I INPUT -p tcp -m tcp --dport 9889 -j ACCEPT
sudo iptables -I INPUT -p tcp -m tcp --dport 9997 -j ACCEPT
sudo iptables -I INPUT -p tcp -m tcp --dport 9999 -j ACCEPT

# Save iptables
sudo service netfilter-persistent start
sudo netfilter-persistent save
sudo systemctl daemon-reload

# NoSQL database name & kaa cassandra tables

# If line exists ..
# sudo sed -i '/nosql_db_provider_name=/c\nosql_db_provider_name=cassandra' /etc/kaa-node/conf/kaa-node.properties

echo ' ' | sudo tee -a /etc/kaa-node/conf/kaa-node.properties
echo '# NoSQL database name & kaa cassandra tables' | sudo tee -a /etc/kaa-node/conf/kaa-node.properties
echo 'nosql_db_provider_name=cassandra' | sudo tee -a /etc/kaa-node/conf/kaa-node.properties

sudo cqlsh -f /etc/kaa-node/conf/cassandra.cql

# !!
# tested on presentation server on April 17, 1019 (Debian)
