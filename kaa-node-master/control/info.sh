# Services STATUS
sudo echo " "
echo "   MariaDB"
sudo service mariadb status | grep active
echo " "
echo "   Zookeeper"
sudo service zookeeper status | grep active
echo " "
echo "   Cassandra"
sudo service cassandra status | grep active
echo " "
echo "   Kaa-Node"
sudo service kaa-node status | grep active
#echo " "
#echo "   Postfix"
#sudo service postfix status | grep active
#echo " "
#echo "   Zeppelin"
#sudo service zeppelin status | grep active
#echo " "
