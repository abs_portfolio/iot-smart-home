wget http://www-us.apache.org/dist/zeppelin/zeppelin-0.8.0/zeppelin-0.8.0-bin-all.tgz


# Unpack in /opt/
sudo tar xf zeppelin-*-bin-all.tgz -C /opt

# Rename folder to "zeppelin"
sudo mv /opt/zeppelin-*-bin-all /opt/zeppelin

# Copy template file into configuration (making use of port 7080 instead of 8080 - which is Kaa)
sudo cp assets/zeppelin-env.sh /opt/zeppelin/conf/zeppelin-env.sh

# Unprivileged user to run zeppelin
sudo useradd -d /opt/zeppelin -s /bin/false zeppelin

# Change ownership of /opt/zeppelin
sudo chown -R zeppelin:zeppelin /opt/zeppelin

# Add zeppelin.service in systemd
sudo cp assets/zeppelin.service /etc/systemd/system/zeppelin.service

# Enable zeppelin autostart
sudo systemctl enable zeppelin

