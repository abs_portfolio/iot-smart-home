

# for google account configuration
#
# enable IMAP/SMTP access
#https://mail.google.com/mail/#settings/fwdandpop
# enable less secure app access
#https://myaccount.google.com/lesssecureapps?pli=1


sudo apt-get update

# install postfix mailutils
sudo apt-get install -y postfix mailutils

# once completed
#sudo mv /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.old
sudo cp assets/sasl_passwd /etc/postfix/sasl_passwd

# Or the manual way
# sudo gedit /etc/postfix/sasl_passwd
# [smtp.gmail.com:587] kaacomms@gmail.com:fiotakis7

sudo chmod 600 /etc/postfix/sasl_passwd

sudo ls -l /etc/postfix/sasl_passwd
sudo cat /etc/postfix/sasl_passwd

if [ "$HOSTNAME" = "sif" ]; then
    echo "Hostname - sif"
	sudo cp assets/main.cf.sif /etc/postfix/main.cf
else
   echo "Hostname - NOT sif"
fi

if [ "$HOSTNAME" = "onelove" ]; then
      echo "Hostname - onelove"
      sudo cp assets/main.cf.onelove /etc/postfix/main.cf
else
   echo "Hostname - NOT onelove"
fi


# Or the manual way
# sudo gedit /etc/postfix/main.cf
# assets/main.cf

# postmap sasl_passwd
sudo postmap /etc/postfix/sasl_passwd

# restart postfix (systemctl)
sudo systemctl restart postfix.service
