curl -v -S -u tendev:adminadmin -X GET "http://185.244.128.27:8080/kaaAdmin/rest/api/applications" | python -mjson.tool > tmpapps
grep applicationToken tmpapps | sed 's/"//g' | rev | cut -d' ' -f1 | rev | sed 's/,//' > diswatineed
apptoken=$(<diswatineed)
grep id tmpapps | sed 's/"//g' | rev | cut -d' ' -f1 | rev | sed 's/,//' > diswatineed
appid=$(<diswatineed)
grep tenantId tmpapps | sed 's/"//g' | rev | cut -d' ' -f1 | rev | sed 's/,//' > diswatineed
tenid=$(<diswatineed)

sed -i '/"applicationId":/c\    "applicationId": "'$appid'",' assets/schemas/log-appenders-json/rest-ready/sensor-per-date.json
sed -i '/"applicationToken":/c\    "applicationToken":"'$apptoken'",' assets/schemas/log-appenders-json/rest-ready/sensor-per-date.json
sed -i '/"tenantId":/c\    "tenantId":"'$tenid'",' assets/schemas/log-appenders-json/rest-ready/sensor-per-date.json

sed -i '/"applicationId":/c\    "applicationId": "'$appid'",' assets/schemas/log-appenders-json/rest-ready/sensor-per-minute.json
sed -i '/"applicationToken":/c\    "applicationToken":"'$apptoken'",' assets/schemas/log-appenders-json/rest-ready/sensor-per-minute.json
sed -i '/"tenantId":/c\    "tenantId":"'$tenid'",' assets/schemas/log-appenders-json/rest-ready/sensor-per-minute.json

sed -i '/"applicationId":/c\    "applicationId": "'$appid'",' assets/schemas/log-appenders-json/rest-ready/sensor-per-region.json
sed -i '/"applicationToken":/c\    "applicationToken":"'$apptoken'",' assets/schemas/log-appenders-json/rest-ready/sensor-per-region.json
sed -i '/"tenantId":/c\    "tenantId":"'$tenid'",' assets/schemas/log-appenders-json/rest-ready/sensor-per-region.json

sed -i '/"applicationId":/c\    "applicationId": "'$appid'",' assets/schemas/log-appenders-json/rest-ready/sensor-per-row.json
sed -i '/"applicationToken":/c\    "applicationToken":"'$apptoken'",' assets/schemas/log-appenders-json/rest-ready/sensor-per-row.json
sed -i '/"tenantId":/c\    "tenantId":"'$tenid'",' assets/schemas/log-appenders-json/rest-ready/sensor-per-row.json

rm diswatineed tmpapps

unset apptoken
unset appid
unset tenid
