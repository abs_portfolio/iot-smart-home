# Grab TENANT_ADMIN temp pass created from Kaa

# explanation:
# grep tempPassword line and remove quotes ("")
# cut last part of the string with ' ' as delimiter (=reverse and cut 1st part)
# remove last character if comma (,)

# test file:
# echo '"tempPass test": "works!1!"' > tmpfile

# step by step filtering
# grep temp tmpfile | sed 's/"//g' > tmpfile1
# cat tmpfile1 | rev | cut -d' ' -f1 | rev > tmpfile2
# cat tmpfile2 | sed 's/,//' > diswatineed

# complete filtering pipeline
grep temp tmpfile | sed 's/"//g' | rev | cut -d' ' -f1 | rev | sed 's/,//' > diswatineed

# store temp pass in shell variable
tpass=$(<diswatineed)
# clean-up
rm tmpfile diswatineed

# node-conf : change tenadmin pass
curl -v -S -u kaa:adminadmin -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' 'http://185.244.128.27:8080/kaaAdmin/rest/api/auth/changePassword?username=tendev&oldPassword='$tpass'&newPassword=adminadmin' | python -mjson.tool

# log original temp pass
echo `date "+%D"`, `date "+%T"` - tendev : $tpass >> assets/temp-passwords-origin

# release tpass
unset tpass
