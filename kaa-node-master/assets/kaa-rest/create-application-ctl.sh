curl -v -S -u tendev:adminadmin -X GET "http://185.244.128.27:8080/kaaAdmin/rest/api/applications" | python -mjson.tool > tmpctl

# complete ctl filtering pipeline
grep applicationToken tmpctl | sed 's/"//g' | rev | cut -d' ' -f1 | rev | sed 's/,//' > diswatineed

# store temp pass in shell variable
tctl=$(<diswatineed)
# clean-up
rm tmpctl diswatineed

# node-conf : create apllication ctl
curl -v -S -u tendev:adminadmin -X POST "http://185.244.128.27:8080/kaaAdmin/rest/api/CTL/saveSchema" -d "body={\"type\":\"record\",\"name\":\"SensorData\",\"namespace\":\"org.kaaproject.kaa.sample\",\"version\":1,\"fields\":[{\"type\":\"string\",\"name\":\"sensorId\"},{\"type\":\"string\",\"name\":\"model\"},{\"type\":\"string\",\"name\":\"region\"},{\"type\":\"float\",\"name\":\"value\"}]}&tenantId=1&applicationToken="$tctl"" | python -mjson.tool > tmpctlid

# log original temp pass
# echo `date "+%D"`, `date "+%T"` - tendev : $tpass >> assets/temp-passwords-origin

# release tpass
unset tpass
