
# Kaa service-based configuration

# KAA API here:
# https://kaaproject.github.io/kaa/docs/v0.10.0/Programming-guide/Server-REST-APIs/#/

# KAA_ADMIN - Create kaa
./assets/kaa-rest/create-kaa-admin.sh
# _______________________________________

# TENANT - Create
./assets/kaa-rest/create-node-tenant.sh
# _______________________________________

# TENANT_ADMIN - Create tenadmin & Change pass
./assets/kaa-rest/create-tenant-admin.sh
./assets/kaa-rest/change-tenadmin-pass.sh
# _______________________________________

# TENANT_DEVELOPER - Create tendev & Change pass
./assets/kaa-rest/create-tenant-developer.sh
./assets/kaa-rest/change-tendev-pass.sh
# _______________________________________

# APPLICATION - Create
./assets/kaa-rest/create-application.sh
# _______________________________________

# Application CTL - Create
./assets/kaa-rest/create-application-ctl.sh

# Log Schema - Create
./assets/kaa-rest/create-log-schema.sh

# Log appenders - Create
./assets/kaa-rest/update-json-configurations.sh
./assets/kaa-rest/create-cassandra-log-appenders.sh

# _______________________________________

# Usefull API calls

# Get all tenants
# curl -v -S -u kaa:adminadmin -X GET "http://185.244.128.27:8080/kaaAdmin/rest/api/tenants" | python -mjson.tool

# Get all applications
# curl -v -S -u tendev:adminadmin -X GET "http://185.244.128.27:8080/kaaAdmin/rest/api/applications" | python -mjson.tool

# Get all app CTL schemas
# curl -v -S -u tendev:adminadmin -X GET "http://185.244.128.27:8080/kaaAdmin/rest/api/CTL/getApplicationSchemas/24964139918191451747" | python -mjson.tool
