/*
 * Copyright 2014-2016 CyberVision, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <wiringPi.h>

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#include <sys/types.h>
#include <unistd.h>

#include <kaa_error.h>
#include <kaa_context.h>
#include <platform/kaa_client.h>
#include <utilities/kaa_log.h>
#include <kaa_logging.h>
#include <platform-impl/common/ext_log_upload_strategies.h>



#define MAX_LOG_BUCKET_SIZE     SIZE_MAX
#define MAX_LOG_COUNT                 5

#define MAXTIMINGS 85
static int DHTPIN = 7;
static int dht22_dat[5] = {0,0,0,0,0};

/*
 * Pin on Rasbery Pi 2 Model B
 */
#define DHT11_PIN    7

#define KAA_DEMO_UPLOAD_COUNT_THRESHOLD      1 /* Count of collected logs needed to initiate log upload */
#define KAA_DEMO_LOG_GENERATION_FREQUENCY    1 /* In seconds */

#define KAA_DEMO_RETURN_IF_ERROR(error, message) \
    if ((error)) { \
        printf(message ", error code %d\n", (error)); \
        return (error); \
    }



/* Retrieves current temperature. */
// static int32_t get_temperature_sample(void) {
//     /* For the sake of example, random data is used */
//     return rand() % 10 + 25;
// }

static uint8_t sizecvt(const int read)
{
  /* digitalRead() and friends from wiringpi are defined as returning a value
  < 256. However, they are returned as int() types. This is a safety function */

  if (read > 255 || read < 0)
  {
    printf("Invalid data from wiringPi library\n");
    exit(EXIT_FAILURE);
  }
  return (uint8_t)read;
}

static float read_dht22_dat()
{
  uint8_t laststate = HIGH;
  uint8_t counter = 0;
  uint8_t j = 0, i;

  dht22_dat[0] = dht22_dat[1] = dht22_dat[2] = dht22_dat[3] = dht22_dat[4] = 0;

  // pull pin down for 18 milliseconds
  pinMode(DHTPIN, OUTPUT);
  digitalWrite(DHTPIN, HIGH);
  delay(10);
  digitalWrite(DHTPIN, LOW);
  delay(18);
  // then pull it up for 40 microseconds
  digitalWrite(DHTPIN, HIGH);
  delayMicroseconds(40);
  // prepare to read the pin
  pinMode(DHTPIN, INPUT);

  // detect change and read data
  for ( i=0; i< MAXTIMINGS; i++) {
    counter = 0;
    while (sizecvt(digitalRead(DHTPIN)) == laststate) {
      counter++;
      delayMicroseconds(2);
      if (counter == 255) {
        break;
      }
    }
    laststate = sizecvt(digitalRead(DHTPIN));

    if (counter == 255) break;

    // ignore first 3 transitions
    if ((i >= 4) && (i%2 == 0)) {
      // shove each bit into the storage bytes
      dht22_dat[j/8] <<= 1;
      if (counter > 16)
        dht22_dat[j/8] |= 1;
      j++;
    }
  }

  // check we read 40 bits (8bit x 5 ) + verify checksum in the last byte
  // print it out if data is good
  if ((j >= 40) &&
      (dht22_dat[4] == ((dht22_dat[0] + dht22_dat[1] + dht22_dat[2] + dht22_dat[3]) & 0xFF)) ) {
        float t, h;
        h = (float)dht22_dat[0] * 256 + (float)dht22_dat[1];
        h /= 10;
        t = (float)(dht22_dat[2] & 0x7F)* 256 + (float)dht22_dat[3];
        t /= 10.0;
        if ((dht22_dat[2] & 0x80) != 0)  t *= -1;


    printf("AM2303 = %.1f | DHT22 = %.1f *C \n", h, t );
    return t;
  }
  else
  {
    // printf("Data not good, skip\n");
    return 0;
  }
}

/*
 * Forward declarations.
 */
extern kaa_error_t ext_unlimited_log_storage_create(void **log_storage_context_p
        , kaa_logger_t *logger);

static void kaa_demo_add_log_record(void *context) {
    static size_t log_record_counter = 0;

    float humidity = 0.0;
    float temperature = 10.0;

    while (read_dht22_dat() == 0)
		{
			 delay(1); // wait 1msec to refresh
		}

    ++log_record_counter;

    kaa_user_log_record_t *log_record = kaa_logging_sensor_data_create();
    if (!log_record) {
        printf("Failed to create log record, error code %d\n", KAA_ERR_NOMEM);
        return;
    }

    log_record->sensor_id = kaa_string_copy_create("Sensor 1");
    log_record->region = kaa_string_copy_create("Samos");
    log_record->model = kaa_string_copy_create("DHT2");
    log_record->value = t;

    printf("Going to add %zuth log record: { id: '%s', region: '%s', model: '%s', val: %g }\n"
            , log_record_counter, log_record->sensor_id->data, log_record->region->data, log_record->model->data, log_record->value);

    kaa_error_t error_code = kaa_logging_add_record(kaa_client_get_context((kaa_client_t *) context)->log_collector, log_record, NULL);
    if (error_code) {
        printf("Failed to add log record, error code %d\n", error_code);
    }

    log_record->destroy(log_record);
}

int main(/*int argc, char *argv[]*/) {
    printf("Cassandra data analytics demo started\n");
    srand(time(NULL));

    kaa_client_t *kaa_client = NULL;

    void *log_storage_context = NULL;
    void *log_upload_strategy_context = NULL;

    /**
     * Initialize Kaa client.
     */
    kaa_error_t error_code = kaa_client_create(&kaa_client, NULL);
    KAA_DEMO_RETURN_IF_ERROR(error_code, "Failed create Kaa client");

    /**
     * Configure Kaa data collection module.
     */
    error_code = ext_unlimited_log_storage_create(&log_storage_context, kaa_client_get_context(kaa_client)->logger);
    KAA_DEMO_RETURN_IF_ERROR(error_code, "Failed to create unlimited log storage");

    error_code = ext_log_upload_strategy_create(kaa_client_get_context(kaa_client), &log_upload_strategy_context, KAA_LOG_UPLOAD_VOLUME_STRATEGY);
    KAA_DEMO_RETURN_IF_ERROR(error_code, "Failed to create log upload strategy");

    error_code = ext_log_upload_strategy_set_threshold_count(log_upload_strategy_context, KAA_DEMO_UPLOAD_COUNT_THRESHOLD);
    KAA_DEMO_RETURN_IF_ERROR(error_code, "Failed to set threshold log record count");

    kaa_log_bucket_constraints_t bucket_sizes = {
        .max_bucket_size = MAX_LOG_BUCKET_SIZE,
        .max_bucket_log_count = MAX_LOG_COUNT,
    };

    error_code = kaa_logging_init(kaa_client_get_context(kaa_client)->log_collector
            , log_storage_context
            , log_upload_strategy_context
            , &bucket_sizes);
    KAA_DEMO_RETURN_IF_ERROR(error_code, "Failed to init Kaa log collector");

    /**
     * Start Kaa client main loop.
     */
    error_code = kaa_client_start(kaa_client, &kaa_demo_add_log_record, (void *) kaa_client, KAA_DEMO_LOG_GENERATION_FREQUENCY);
    KAA_DEMO_RETURN_IF_ERROR(error_code, "Failed to start Kaa main loop");

    /**
     * Destroy Kaa client.
     */
    kaa_client_destroy(kaa_client);

    printf("Cassandra data analytics demo stopped\n");

    return error_code;
}
