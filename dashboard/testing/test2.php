<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="./js/authorization.js"></script>
        <!-- Bootstrap -->
        <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- Datatables -->
        <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <!--<script type="application/javascript" src="http://localhost:8080/SuchSmallDick/secured/user/8"></script>-->
        <script>
            function getSensor_per_row(token, callback) {
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        callback(xhr.response);
                    }
                }

                xhr.open('GET', get_sensors, true);
//                xhr.withCredentials = true;
                xhr.setRequestHeader("Content-Type", "application/json");
//                xhr.setRequestHeader("token", token);
                xhr.send();
            }
            var get_sensors = "http://localhost:8080/KunderaPOC/sensors/";
            function constractDataSet(json) {
                let jsonData = JSON.parse(json);
                let aaData = new Array();
                let innerJson = JSON.parse(jsonData[1].eventJson);
                console.log(innerJson.value.toFixed(2));
                for (i in jsonData) {
                    let data = JSON.parse(jsonData[i].eventJson);
                    aaData.push([data.sensorId,
                        data.model,
                        data.region,
                        (data.value).toFixed(2),
                        jsonData[i].ts
                    ])
                }
                createUserTable(aaData);
            }
            function createUserTable(dataSet) {

                $("#example").DataTable({
                    data: dataSet,
                    columns: [
                        {title: "Sensore-Id"},
                        {title: "Model"},
                        {title: "Region"},
                        {title: "Value"},
                        {title: "Timestamp"}
                    ]
                });
            }

        </script>

        <input id="clickMe" type="button" value="clickme" onclick="getSensor_per_row(getCookie('token'), constractDataSet)" />
        <fieldset id="fieldset">
            <div id="container" style="margin-right: 10px;">
                <table id="example"></table>
            </div>
        </fieldset>

        <!-- jQuery -->
        <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- iCheck -->
        <script src="../vendors/iCheck/icheck.min.js"></script>
        <!-- Datatables -->
        <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
        <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
        <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
        <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
        <script src="../vendors/jszip/dist/jszip.min.js"></script>
        <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
        <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="../js/custom.min.js"></script>
        <script src="../js/authorization.js"></script>
        <script src="../js/cookieFunctions.js"></script>
    </body>
</html>
