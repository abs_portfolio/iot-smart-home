/* 
 * icsd thesis by George Fiotakis && Nikolaos Tritsis
 * Each line should be prefixed with  * 
 */
/* global raspber_controler_automation_ip, raspber_controler_automation_port */

var automation_switch_act;


$(window).load(function () {
    requestAutomationControler(raspber_controler_automation_ip, raspber_controler_automation_port, "auto_info", loadGridLayoutAuto);
})

function createJsonReqRpi(ip, port, request, device) {
    let arr = new Array();
    if (request === "auto_info") {
        return JSON.stringify({"ip": ip, "port": port, "action": request})
    } else if (device === "automation") {
        return JSON.stringify({"ip": ip, "port": port, "device": device, "action": request});
    }

}

function requestAutomationControler(ip, port, action, callback, device, automation_url) {
    let url;
    if (automation_url)
        url = findUrlReqRpi("triger_auto");
    else
        url = findUrlReqRpi(action);

    let json = createJsonReqRpi(ip, port, action, device);
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                callback(xhr.response);
                console.log(json);
            } else {
                onErrorPrint();
            }
        }
    }

    xhr.open('POST', url, true);
    xhr.withCredentials = true;
    xhr.setRequestHeader("Authorization", "Basic " + btoa(sessionStorage.username + ":" + sessionStorage.pwd));
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(json);
}

function loadGridLayoutAuto(json) {
//    load_second_row("second_row_1","second_row_2");
    $("#rpi_loader").fadeOut("slow", function () {
        load_up_first_row("first_row");
    });
    $("#automation_loader").fadeOut("slow", function () {
        $('#automation_container').append(appendGridElemment("automation_up.png"));
    });
    constractCrusialValuesGrid(json);
}

function constractCrusialValuesGrid(json) {
    let jsonData = JSON.parse(json);
    console.log(jsonData);

    $("#temprature_exaust_crutial_container").ionRangeSlider({
        hide_min_max: false,
        keyboard: true,
        min: jsonData.critical_temprature_max - 15,
        max: jsonData.critical_temprature_max + 15,
        from: jsonData.critical_temprature_max,
        to: 100,
        type: 'double',
        step: 1,
        prefix: "°",
        to_fixed: true,
        grid: true
    });
    $("#humidity_exaust_crutial_container").ionRangeSlider({
        hide_min_max: false,
        keyboard: true,
        min: jsonData.critical_humidity_min - 15,
        max: jsonData.critical_humidity_min + 15,
        from: jsonData.critical_humidity_min - 15,
        to: jsonData.critical_humidity_min,
        type: 'double',
        step: 1,
        prefix: "%",
        from_fixed: true,
        grid: true
    });
    $("#temprature_radiator_crutial_container").ionRangeSlider({
        hide_min_max: false,
        keyboard: true,
        min: jsonData.critical_temprature_min - 15,
        max: jsonData.critical_temprature_min + 15,
        from: jsonData.critical_temprature_min - 15,
        to: jsonData.critical_temprature_min,
        type: 'double',
        step: 1,
        prefix: "°",
        from_fixed: true,
        grid: true
    });
    $("#humidity_radiator_crutial_container").ionRangeSlider({
        hide_min_max: false,
        keyboard: true,
        min: jsonData.critical_humidity_max - 15,
        max: jsonData.critical_humidity_max + 15,
        from: jsonData.critical_humidity_max,
        to: jsonData.critical_humidity_max + 15,
        type: 'double',
        step: 1,
        prefix: "%",
        to_fixed: true,
        grid: true
    });
    $('#time_picker_start').datetimepicker({
        showClear: true,
        format: 'HH:mm'
    });

    $('#time_picker_finish').datetimepicker({
        showClear: true,
        format: 'HH:mm'
    });
    setTimeOnTimePicker("close_time", jsonData.close_lights_time);
    setTimeOnTimePicker("open_time", jsonData.open_lights_time);

    console.log($("#close_time").val());
    console.log(jsonData.state);
    activate_state_switch(jsonData);
    console.log($("#temprature_exaust_crutial_container").val());

    document.getElementById("upload_button").addEventListener("click", function () {
        let json = constractConfiginfoJson(raspber_controler_automation_ip, raspber_controler_automation_port);
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    window.alert("successfully updated automation information.");
                } else {
                    onErrorPrint();
                }
            }
        }

        xhr.open('POST', rpi_auto_config, true);
        xhr.withCredentials = true;
        xhr.setRequestHeader("Authorization", "Basic " + btoa(sessionStorage.username + ":" + sessionStorage.pwd));
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(json);
        console.log();
    });
}
function constractConfiginfoJson(ip, port) {
    return  JSON.stringify({"ip": ip, "port": port, "critical_temprature_max": parseInt($("#temprature_exaust_crutial_container").val().split(";")[0]), "critical_temprature_min": parseInt($("#temprature_radiator_crutial_container").val().split(";")[1]),
        "critical_humidity_max": parseInt($("#humidity_radiator_crutial_container").val().split(";")[0]), "critical_humidity_min": parseInt($("#humidity_exaust_crutial_container").val().split(";")[1]),
        "open_lights_time": $("#open_time").val(), "close_lights_time": $("#close_time").val(), "action": "auto_config", "device": "automation"});
}
function activate_state_switch(jsonData) {
    $("#automation_switch").fadeIn(1000);
    automation_switch_act = document.getElementById("automation_switch_act");
    if (jsonData.state === true)
        document.getElementById("automation_switch_act").checked = true;
    automation_switch_act.addEventListener('click', function () {
        if ($('#automation_switch_act').is(":checked"))
        {
            requestAutomationControler(raspber_controler_automation_ip, raspber_controler_automation_port, "start", check_state, "automation", true);
        } else {
            requestAutomationControler(raspber_controler_automation_ip,raspber_controler_automation_port, "stop", check_state, "automation", true);

        }
    });
}

function check_state(jsonData) {
    let json = JSON.parse(jsonData);
    if (json.state === true)
        document.getElementById("automation_switch_act").checked = true;
    else
        document.getElementById("automation_switch_act").checked = false;
}

function setTimeOnTimePicker(container, time) {
//    let d = new Date();
//
//    let month = d.getMonth() + 1;
//    let day = d.getDate();
//
//    let output = d.getFullYear() + '/' +
//            (month < 10 ? '0' : '') + month + '/' +
//            (day < 10 ? '0' : '') + day;

    $("#" + container).val(time);
}