


function createJsonReqRpi(ip, port, request, device, region, action) {
    let arr = new Array();
    if (request === "info" || request === "dev_info") {
        return JSON.stringify({"ip": ip, "port": port})
    } else if (request === "trigger") {
        return JSON.stringify({"ip": ip, "port": port, "region": region, "device": device, "action": action});
    }

}


function getRpiRequest(ip, port, action, callback, device, region, dev_action)
{
    console.log(action);
    console.log(device);
    console.log(region);
    console.log(action);
    let url = findUrlReqRpi(action);
    console.log(url);
    console.log(sessionStorage.username);
    console.log(sessionStorage.pwd);
    let json = createJsonReqRpi(ip, port, action, device, region, dev_action);
    console.log(json);
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                callback(xhr.response);
                second_decodeRpi_request(xhr.response);
            } else {
                onErrorPrint();
            }
        }
    }

    xhr.open('POST', url, true);
    xhr.withCredentials = true;
    xhr.setRequestHeader("Authorization", "Basic " + btoa(sessionStorage.username + ":" + sessionStorage.pwd));
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(json);
}

function printDamp(tmp) {
    console.log(tmp);
}
var status_div;

function decodeRpi_request(json) {

    let jsonData = JSON.parse(json);
    if (jsonData) {
        $("#rpi_status_loader").fadeOut("slow", function () {
            console.log(jsonData);
            status_div = document.getElementById("rpi_status_content");
            $('#rpi_status_content').append('<span class="count_top">\n\
                <i class="fa fa-rss-square"></i> ' + jsonData.ip_add + '</span>\n\
                <div class="count"><img style="max-height: 50%; max-width: 50%; display: block;margin-left: auto;margin-right: auto;"\n\
                  src="./images/services_icons/rpi2_up.png" alt="alternative text" title="Raspberry Pi is app at host : ' + jsonData.ip_add[0] + '" /></div>');
        });
    } else {

    }
}
function second_decodeRpi_request(json) {
    let jsonData = JSON.parse(json);
    if (jsonData) {
        $("#rpi_2_status_loader").fadeOut("slow", function () {
            console.log(jsonData);
            status_div = document.getElementById("rpi_status_content");
            $('#rpi_2_status_content').append('<span class="count_top">\n\
                <i class="fa fa-rss-square"></i> ' + jsonData.ip_add + '</span>\n\
                <div class="count"><img style="max-height: 50%; max-width: 50%; display: block;margin-left: auto;margin-right: auto;"\n\
                 src="./images/services_icons/rpi2_up.png" alt="alternative text" title="Raspberry pi is up at host : ' + jsonData.ip_add[0] + '" /></div>');
        });
    } else {
        $('#rpi_2_status_content').append('<span class="count_top">\n\
                <i class="fa fa-rss-square"></i> ' + jsonData.ip_add + '</span>\n\
                <div class="count"><img style="max-height: 50%; max-width: 50%; display: block;margin-left: auto;margin-right: auto;"\n\
                 src="./images/services_icons/rpi2_down.png" alt="alternative text" title="Raspberry pi is down" /></div>');

    }
}
