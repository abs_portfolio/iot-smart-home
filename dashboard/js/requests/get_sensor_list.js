/* 
 * icsd thesis by George Fiotakis && Nikolaos Tritsis
 * Each line should be prefixed with  * 
 */

function getSensorByRequest(url, callback)
{
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            callback(xhr.response);
        }
    }
    xhr.open('GET', url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send();
}

function getCurrentActiveSensors(callback) {
    getSensorByRequest(sensor_id_live_all, callback);
}
function getDayActiveSensors(callback) {
    getSensorByRequest(sensor_id_daily_all, callback);
}
function getOveralActiveSensors(callback) {
    getSensorByRequest(sensor_id_overal_all, callback);
}
function getOveralGassData(callback) {
    getSensorByRequest(overal_gass_data, callback);
}
var sensor_list_id;
var model;
var region;
var sensor_list;

function constract_sensor_list(data) {
    sensor_list_id = new Array();
    model = new Array();
    region = new Array();
    sensor_list = new Array();
    let jsonData = JSON.parse(data);
    jsonData = jsonData.record_list;
    console.log(jsonData[0].model);
    for (let i = 0; i < jsonData.length; i++) {
        if (!model.includes(jsonData[i].model)) {
            model.push(jsonData[i].model);
        }
        if (!region.includes(jsonData[i].region)) {
            region.push(jsonData[i].region);
        }
        if (!sensor_list_id.includes(jsonData[i].sensor_id)) {
            sensor_list_id.push(jsonData[i].sensor_id);
            sensor_list.push(jsonData[i]);
        }
    }
    console.log(sensor_list);
    initializeHomeChart(sensor_list_id);
//    return sensor_list;
}
function constract_sensor_search_nVisualize(data) {
    sensor_list_id = new Array();
    model = new Array();
    region = new Array();
    sensor_list = new Array();
    let jsonData = JSON.parse(data);
    jsonData = jsonData.record_list;
//    console.log(jsonData[0].model);
    for (let i = 0; i < jsonData.length; i++) {
        if (!model.includes(jsonData[i].model)) {
            model.push(jsonData[i].model);
        }
        if (!region.includes(jsonData[i].region)) {
            region.push(jsonData[i].region);
        }
        if (!sensor_list_id.includes(jsonData[i].sensor_id)) {
            sensor_list_id.push(jsonData[i].sensor_id);
            sensor_list.push(jsonData[i]);
        }
    }
//    console.log(sensor_list);
    multysellect_callback(sensor_list_id ,model,region, sensor_list);
//    return sensor_list;
}


var finalized_raw_dataset;
var raw_data = new Array();

function constractRawDataset(json) {
    let jsonData = JSON.parse(json);
    console.log(jsonData.result);

    for (let i = 0; i < jsonData.result.length; i++) {
        raw_data.push(
                new Array(
                  jsonData.result[0].values[0]
                , jsonData.result[i].values[1]
                , jsonData.result[i].values[2]
                , jsonData.result[i].values[3]
                , jsonData.result[i].values[4]
                , jsonData.result[i].values[5]
                , jsonData.result[i].values[6]
                , jsonData.result[i].values[7]
                , jsonData.result[i].values[8]
                , jsonData.result[i].values[9]
                , jsonData.result[i].date
                , jsonData.result[i].region));
    }

    var option = {
        animation: false,
        brush: {
            brushLink: 'all',
            xAxisIndex: [],
            yAxisIndex: [],
            inBrush: {
                opacity: 1
            }
        },
        visualMap: {
            type: 'piecewise',
            categories: ["Kitchen", "Bedroom", "Living_room"],
            dimension: CATEGORY_DIM,
            orient: 'horizontal',
            top: 0,
            left: 'center',
            inRange: {
                color: ['#c23531', '#2f4554', '#61a0a8']
            },
            outOfRange: {
                color: '#ddd'
            },
            seriesIndex: [0]
        },
        tooltip: {
            trigger: 'item'
        },
        parallelAxis: [
            {dim: 0, name: schema[0].text},
            {dim: 1, name: schema[1].text},
            {dim: 2, name: schema[2].text},
            {dim: 3, name: schema[3].text},
            {dim: 4, name: schema[4].text},
            {dim: 5, name: schema[5].text},
            {dim: 6, name: schema[6].text},
            {dim: 7, name: schema[7].text},
            {dim: 8, name: schema[8].text},
            {dim: 9, name: schema[9].text},
            {dim: 10, name: schema[10].text,
                type: 'category', data: jsonData.datelist
            }
        ],
        parallel: {
            bottom: '5%',
            left: '5%',
            height: '31%',
            width: '55%',
            parallelAxisDefault: {
                type: 'value',
                name: 'parallelAxisDefault',
                nameLocation: 'end',
                nameGap: 20,
                splitNumber: 3,
                nameTextStyle: {
                    fontSize: 14
                },
                axisLine: {
                    lineStyle: {
                        color: '#555'
                    }
                },
                axisTick: {
                    lineStyle: {
                        color: '#555'
                    }
                },
                splitLine: {
                    show: false
                },
                axisLabel: {
                    textStyle: {
                        color: '#555'
                    }
                }
            }
        },
        grid: [],
        xAxis: [],
        yAxis: [],
        series: [
            {
                name: 'parallel',
                type: 'parallel',
                smooth: true,
                lineStyle: {
                    normal: {
                        width: 1,
                        opacity: 0.3
                    }
                },
                data: raw_data
            }
        ]
    };

    generateGrids(option);
    myChart.setOption(option, true);
}



// Schema:
// date,AQIindex,PM2.5,PM10,CO,NO2,SO2
var schema = [
    {name: 'Benzine', index: 1, text: 'Benzine'},
    {name: 'Hexane', index: 2, text: 'Hexane'},
    {name: 'Alcohol', index: 3, text: 'Alcohol'},
    {name: 'H2', index: 4, text: 'H2'},
    {name: 'CH4', index: 5, text: 'CH4'},
    {name: 'CO', index: 6, text: 'CO'},
    {name: 'LPG', index: 7, text: 'LPG'},
    {name: 'Smoke', index: 8, text: 'Smoke'},
    {name: 'CO2', index: 9, text: 'CO2'},
    {name: 'NH4', index: 10, text: 'NH4'},
    {name: 'DateList', index: 11, text: 'Dates'}
];

var CATEGORY_DIM_COUNT = 6;
var GAP = 2
var BASE_LEFT = 5;
var BASE_TOP = 10;
// var GRID_WIDTH = 220;
// var GRID_HEIGHT = 220;
var GRID_WIDTH = (100 - BASE_LEFT - GAP) / CATEGORY_DIM_COUNT - GAP;
var GRID_HEIGHT = (100 - BASE_TOP - GAP) / CATEGORY_DIM_COUNT - GAP;
var CATEGORY_DIM = 11;
var SYMBOL_SIZE = 3;

function retrieveScatterData(data, dimX, dimY) {
    var result = [];
    for (var i = 0; i < data.length; i++) {
        var item = [data[i][dimX], data[i][dimY]];
        item[CATEGORY_DIM] = data[i][CATEGORY_DIM];
        result.push(item);
    }
    return result;
}

function generateGrids(option) {
    var index = 0;

    for (var i = 0; i < CATEGORY_DIM_COUNT; i++) {
        for (var j = 0; j < CATEGORY_DIM_COUNT; j++) {
            if (CATEGORY_DIM_COUNT - i + j >= CATEGORY_DIM_COUNT) {
                continue;
            }

            option.grid.push({
                left: BASE_LEFT + i * (GRID_WIDTH + GAP) + '%',
                top: BASE_TOP + j * (GRID_HEIGHT + GAP) + '%',
                width: GRID_WIDTH + '%',
                height: GRID_HEIGHT + '%'
            });

            option.brush.xAxisIndex && option.brush.xAxisIndex.push(index);
            option.brush.yAxisIndex && option.brush.yAxisIndex.push(index);

            option.xAxis.push({
                splitNumber: 3,
                position: 'top',
                axisLine: {
                    show: j === 0,
                    onZero: false
                },
                axisTick: {
                    show: j === 0,
                    inside: true
                },
                axisLabel: {
                    show: j === 0
                },
                type: 'value',
                gridIndex: index,
                scale: true
            });

            option.yAxis.push({
                splitNumber: 3,
                position: 'right',
                axisLine: {
                    show: i === CATEGORY_DIM_COUNT - 1,
                    onZero: false
                },
                axisTick: {
                    show: i === CATEGORY_DIM_COUNT - 1,
                    inside: true
                },
                axisLabel: {
                    show: i === CATEGORY_DIM_COUNT - 1
                },
                type: 'value',
                gridIndex: index,
                scale: true
            });

            option.series.push({
                type: 'scatter',
                symbolSize: SYMBOL_SIZE,
                xAxisIndex: index,
                yAxisIndex: index,
                data: retrieveScatterData(raw_data, i, j)
            });

            option.visualMap.seriesIndex.push(option.series.length - 1);

            index++;
        }
    }

}




