/* 
 * icsd thesis by George Fiotakis && Nikolaos Tritsis
 * Each line should be prefixed with  * 
 */


function getCassandra_status(callback)
{
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            callback(xhr.response);
        }
    }

    xhr.open('GET', cass_status_url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send();
}

var status_div;

function decodeCassandra_status(json) {

    let jsonData = JSON.parse(json);
//    console.log(jsonData[0]);
    console.log(jsonData);
    decodeKaa_NodeStatus(json);
    decodeMysql_NodeStatus(json);
    decodeRest_Status(json);
    if (jsonData[0].bootstrapped === "COMPLETED") {
        $("#cass_status_loader").fadeOut("slow", function () {
            status_div = document.getElementById("cassandra_status_content");
            $('#cassandra_status_content').append('<span class="count_top">\n\
                <i class="fa fa-database"></i> ' + jsonData[0].broadcast_address + '</span>\n\
                <div class="count"><img style="max-height: 50%; max-width: 50%; display: block;margin-left: auto;margin-right: auto;"\n\
                 src="./images/services_icons/cassandra_up.png" alt="alternative text" title="Cassandra is app at host : ' + jsonData[0].broadcast_address + '" /></div>');
        });
    } else {
        $("#cass_status_loader").fadeOut("slow", function () {
            console.log(jsonData);
            status_div = document.getElementById("cassandra_status_content");
            $('#cassandra_status_content').append('<span class="count_top">\n\
                <i class="fa fa-database"></i>  Apache Cassandra</span>\n\
                <div class="count"><img style="max-height: 50%; max-width: 50%; display: block;margin-left: auto;margin-right: auto;"\n\
                 src="./images/services_icons/cassandra_down.png"alt="alternative text" title="MySql is down" /></div><span class="count_bottom" id="cluster_ip">\n\
                <i class="fa fa-rss-square"></i>    ' + jsonData[0].broadcast_address + '</span>');
        });

    }
}

function decodeKaa_NodeStatus(json) {

    let jsonData = JSON.parse(json);
    if (jsonData[0].bootstrapped === "COMPLETED") {
        $("#kaa_status_loader").fadeOut("slow", function () {
            $('#kaa_status_content').append('<span class="count_top">\n\
                 <i class="fa fa-rss-square"></i>    ' + jsonData[0].broadcast_address + '</span>\n\
                <div class="count"><img style="max-height: 50%; max-width: 50%; display: block;margin-left: auto;margin-right: auto;"\n\
                src="./images/services_icons/server2_up.png" alt="alternative text" title="Kaa-node is app at host : ' + jsonData[0].broadcast_address + '" /></div><span class="count_bottom">');
        });
    } else {
        $('#kaa_status_content').append('<span class="count_top">\n\
                <i class="fa fa-desktop"></i>  Kaa Node</span>\n\
                <div class="count"><img style="max-height: 40%; max-width: 40%; display: block;margin-left: auto;margin-right: auto;"\n\
                src="./images/services_icons/server2_down.png" alt="alternative text" title="Kaa-node is down " /></div><span class="count_bottom">\n\
                <i class="fa fa-rss-square"></i>    ' + jsonData[0].broadcast_address + '</span>');
    }
}
function decodeMysql_NodeStatus(json) {

    let jsonData = JSON.parse(json);
    if (jsonData[0].bootstrapped === "COMPLETED") {
        $("#mySql_status_loader").fadeOut("slow", function () {
//            console.log(jsonData);
            status_div = document.getElementById("mySql_status_content");
            $('#mySql_status_content').append('<span class="count_top">\n\
                <i class="fa fa-database"></i>' + jsonData[0].broadcast_address + '</span>\n\
                <div class="count"><img style="max-height: 50%; max-width: 50%; display: block;margin-left: auto;margin-right: auto;"\n\
                 src="./images/services_icons/mysql_up.png" alt="alternative text" title="MySql is app at host : ' + jsonData[0].broadcast_address + '" /></div>');
        });
    } else {

    }
}
function decodeRest_Status(json) {

    let jsonData = JSON.parse(json);
    if (jsonData[0].bootstrapped === "COMPLETED") {
        $("#rest_api_status_loader").fadeOut("slow", function () {
            $('#rest_api_status_content').append('<span class="count_top">\n\
                <i class="fa fa-database"></i>' + jsonData[0].broadcast_address + '</span>\n\
                <div class="count"><img style="max-height: 50%; max-width: 50%; display: block;margin-left: auto;margin-right: auto;"\n\
                 src="./images/services_icons/rest_up.png" alt="alternative text" title="MySql is app at host : ' + jsonData[0].broadcast_address + '" /></div>');
        });
    } else {

    }
}


                        