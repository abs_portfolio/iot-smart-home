/* 
 * icsd thesis by George Fiotakis && Nikolaos Tritsis
 * Each line should be prefixed with  * 
 */


$(document).ready(function () {
    console.log(sessionStorage.getItem("username"));
    document.getElementById("user_name_h2").innerHTML = sessionStorage.getItem("username");
    document.getElementById("dropdown_username").appendChild(document.createTextNode(sessionStorage.getItem("username")));
//                openModal();
    getSensor_liver_results(constractSensorDataSet);
    getSensor_overal_results(constractSensorDataSet);
    getSensor_daily_results(constractSensorDataSet);

})


function openModal() {
}

function closeLoader(container) {
    $("#" + container).fadeOut("fast");
}

function getSensor_liver_results(callback) {
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            callback(xhr.response, "live_table");
        }
    }
    xhr.open('GET', table_live_all, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send();
}

function getSensor_overal_results(callback) {
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            callback(xhr.response, "overal_table");
        }
    }
    xhr.open('GET', table_overal_all, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send();
}
function getSensor_daily_results(callback) {
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            callback(xhr.response, "daily_table");
        }
    }
    xhr.open('GET', table_daily_all, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send();
}

function constractSensorDataSet(json, container) {
    let jsonData = JSON.parse(json);
    let aaData = new Array();
    let innerJson = JSON.parse(jsonData[1].eventJson);
    console.log(innerJson.value.toFixed(2));
    for (i in jsonData) {
        let data = JSON.parse(jsonData[i].eventJson);
        aaData.push([data.sensorId,
            data.model,
            data.region,
            (data.value).toFixed(2),
            new Date(jsonData[i].ts)
        ])
    }
    findNcloseLoader(container);
    constractSensorTable(aaData, container);
}
function findNcloseLoader(container) {
    console.log(container);
    if (container.includes("daily")) {
        closeLoader("daily_loader");
    } else if (container.includes("live")) {
        closeLoader("live_loader");
    } else if (container.includes("overal")) {
        closeLoader("overal_loader");
    }
}
function constractSensorTable(dataSet, container) {
    $("#" + container).DataTable({
        data: dataSet,
        columns: [
            {title: "Sensor-Id"},
            {title: "Model"},
            {title: "Region"},
            {title: "Value"},
            {title: "Timestamp"}
        ]
    });
}

/*
 var table_daily_all ="http://localhost:8080/cass_prod/sensors/daily/";
 var table_live_all ="http://localhost:8080/cass_prod/sensors/live";
 var table_overal_all ="http://localhost:8080/cass_prod/sensors/overal/";*/