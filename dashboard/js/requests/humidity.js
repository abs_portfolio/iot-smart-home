/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getCurrDay() {
    let now = new Date();
    let year = now.getFullYear();
    let month = now.getMonth() + 1;
    let day = now.getDate();
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    let dateTime = year + '-' + month + '-' + day + ' 00:00:00';
    return dateTime;
}
function getCurrDayShow() {
    let now = new Date();
    let year = now.getFullYear();
    let month = now.getMonth() + 1;
    let day = now.getDate();
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    if (day.toString().length == 1) {
        day = '0' + day;
    }

    let dateTime = day + '-' + month + '-' + year;
    return dateTime;
}
function getDateTime(substraction) {
    let now = new Date();
    let year = now.getFullYear();
    let month = now.getMonth() + 1;
    let day = now.getDate();
    let hour = now.getHours();
    let minute = now.getMinutes();
    let second = now.getSeconds();
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    if (hour.toString().length == 1) {
        hour = '0' + hour;
    }
    if (minute.toString().length == 1) {
        minute = '0' + minute;
    }
    if (second.toString().length == 1) {
        second = '0' + second;
    }
    let dateTime;
    if (parseInt(parseInt(minute) - substraction) >= 0) {
        dateTime = year + '-' + month + '-' + day + ' ' + hour + ':' + parseInt(parseInt(minute) - substraction) + ':' + second;
    } else if (parseInt(parseInt(hour) - 1) >= 0) {
        dateTime = year + '-' + month + '-' + day + ' ' + parseInt(parseInt(hour) - 1) + ':' + 45 + ':' + second;
    } else
        dateTime = year + '-' + month + '-' + parseInt(parseInt(day) - 1) + ' ' + 23 + ':' + 45 + ':' + second;

    return dateTime;
}
var sensor_id_humidity;

function getHumidityData(callback, s_id) {
    sensor_id_humidity = s_id;
    let myXhr = new XMLHttpRequest();
    console.log(getDateTime(15));
    myXhr.onreadystatechange = function () {
        if (myXhr.readyState === 4) {
            callback(myXhr.response);
//            callback2(xhr.response);
        }
    }

    myXhr.open('POST', live_avg_by_sensor_id + s_id, true);
    myXhr.setRequestHeader("Content-Type", "application/json");//AM2303_Kitchen
    myXhr.send();//JSON.stringify({"sensor_id": sensor_id_humidity, "date": getDateTime(10)})

}
var coun_text = document.getElementById("av_tmp_day_count");



function constractCurrHumidityDataSet(json) {
    let jsonData = JSON.parse(json);
    console.log(jsonData.average);
    if (jsonData.total_mesurments_curr !== 0) {
        $("#loader_humidity_chart_container").fadeOut("slow", function () {
            constranct_humidity_chart(jsonData.average);
        });
    } else
        window.alert("Cant read data from sensor " + jsonData.sensor_id);

}
var echartGauge;

function constranct_humidity_chart(value) {
    echartGauge = echarts.init(document.getElementById('egauce_chart'));
    option = {
        tooltip: {
            formatter: "{a} <br/>{b} : {c}%"
        },
        toolbox: {
            feature: {
                restore: {},
                saveAsImage: {}
            }
        },
        series: [
            {
                name: 'Humidity',
                type: 'gauge',
                detail: {formatter: '{value}%'},
                data: [{value: value.toFixed(2), name: 'Humidity'}],
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: [
                            [0.2, '#A9A9A9'],
                            [0.4, '#9999ff'],
                            [0.6, '#7f7fff'],
                            [0.8, '#6666ff'],
                            [1, '#d21919']
                        ],
                        width: 20
                    }
                }
            }
        ]
    };
    echartGauge.setOption(option);
    setInterval(function () {
        getHumidityData(updateHumidityValue, "AM2303_Kitchen");

    }, 10000);

}

function updateHumidityValue(json) {
//    console.log(json);
    let jsonData = JSON.parse(json);
    let value = jsonData.average;
    console.log("update humidity value : "+value);
    option.series[0].data[0].value = value.toFixed(2);
    echartGauge.setOption(option, true);
}

function constranct_humidity_chart2(value) {
    var echartGauge = echarts.init(document.getElementById('egauce_chart'));
    // use configuration item and data specified to show chart
    echartGauge.setOption({
        tooltip: {
            formatter: "{a} <br/>{b} : {c}%"
        },
        toolbox: {
            show: true,
            feature: {
                restore: {
                    show: true,
                    title: "Restore"
                },
                saveAsImage: {
                    show: true,
                    title: "Save Image"
                }
            }
        },
        series: [{
                name: 'Humidity',
                type: 'gauge',
                startAngle: 140,
                endAngle: -140,
                min: 0,
                max: 100,
                precision: 0,
                splitNumber: 10,
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: [
                            [0.2, 'green'],
                            [0.4, '#A9A9A9'],
                            [0.6, '#696969'],
                            [0.8, '#990000'],
                            [1, '#660000']
                        ],
                        width: 30
                    }
                },
                axisTick: {
                    show: true,
                    splitNumber: 5,
                    length: 8,
                    lineStyle: {
                        color: '#eee',
                        width: 1,
                        type: 'solid'
                    }
                },
                axisLabel: {
                    show: true,
                    formatter: function (v) {
                        switch (v + '') {
                            case '0':
                                return '0%';
                            case '20':
                                return '20%';
                            case '40':
                                return '40%';
                            case '60':
                                return '60%';
                            case '80':
                                return '80%';
                            case '100':
                                return '100%';
                            default:
                                return '';
                        }
                    },
                    textStyle: {
                        color: '#333'
                    }
                },
                splitLine: {
                    show: true,
                    length: 30,
                    lineStyle: {
                        color: '#eee',
                        width: 2,
                        type: 'solid'
                    }
                },
                pointer: {
                    length: '80%',
                    width: 8,
                    color: 'auto'
                },
                title: {
                    show: true,
                    offsetCenter: ['-65%', -10],
                    textStyle: {
                        color: '#333',
                        fontSize: 15
                    }
                },
                detail: {
                    show: true,
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderWidth: 0,
                    borderColor: '#ccc',
                    width: 100,
                    height: 40,
                    offsetCenter: ['-60%', 10],
                    formatter: '{value}%',
                    textStyle: {
                        color: 'auto',
                        fontSize: 30
                    }
                },
                data: [{
                        value: value.toFixed(2),
                        name: 'Humidity'
                    }]
            }]
    });
    echartGauge.setOption(echartGauge.setOption);
}