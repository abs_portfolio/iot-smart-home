/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Date Functions
function getCurrDay() {
    let now = new Date();
    let year = now.getFullYear();
    let month = now.getMonth() + 1;
    let day = now.getDate();
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    let dateTime = year + '-' + month + '-' + day + ' 00:00:00';
    return dateTime;
}
function getCurrDayShow() {
    let now = new Date();
    let year = now.getFullYear();
    let month = now.getMonth() + 1;
    let day = now.getDate();
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    if (day.toString().length == 1) {
        day = '0' + day;
    }

    let dateTime = day + '-' + month + '-' + year;
    return dateTime;
}
function getDateTime(substraction) {
    let now = new Date();
    let year = now.getFullYear();
    let month = now.getMonth() + 1;
    let day = now.getDate();
    let hour = now.getHours();
    let minute = now.getMinutes();
    let second = now.getSeconds();
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    if (hour.toString().length == 1) {
        hour = '0' + hour;
    }
    if (minute.toString().length == 1) {
        minute = '0' + minute;
    }
    if (second.toString().length == 1) {
        second = '0' + second;
    }
    let dateTime;
    if (parseInt(parseInt(minute) - substraction) >= 0) {
        dateTime = year + '-' + month + '-' + day + ' ' + hour + ':' + parseInt(parseInt(minute) - substraction) + ':' + second;
    } else if (parseInt(parseInt(hour) - 1) >= 0) {
        dateTime = year + '-' + month + '-' + day + ' ' + parseInt(parseInt(hour) - 1) + ':' + 45 + ':' + second;
    } else
        dateTime = year + '-' + month + '-' + parseInt(parseInt(day) - 1) + ' ' + 23 + ':' + 45 + ':' + second;

    return dateTime;
}

//The id of the Sensor to display.
var sensor_id;
var url;
function getThermoData(callback1, s_id) {
    sensor_id = s_id;
    let xhr = new XMLHttpRequest();
    console.log(getDateTime(15));
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            url = live_avg_by_sensor_id + s_id;
            callback1(xhr.response );
//            callback2(xhr.response);
        }
    }

    xhr.open('POST', live_avg_by_sensor_id + s_id, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send();//JSON.stringify({"sensor_id": sensor_id, "date": getDateTime(10)})

}
var coun_text = document.getElementById("av_tmp_day_count");


function constractThermoDataSet(json) {
    let jsonData = JSON.parse(json);
    console.log(jsonData.average_value);
    if (jsonData.total_mesurments != 0) {
        $("#loader_day_avg_thermo-container").fadeOut("slow", function () {
            createDay_avg_Chart(jsonData.average_value);
            document.getElementById("av_tmp_day_date").appendChild(document.createTextNode(getCurrDayShow()));
            coun_text.appendChild(document.createTextNode(" , data from " + jsonData.total_mesurments));
//            $("#chartobject-1").removeClass('.raphael-group-12-background');
//            console.log($("#chartobject-1"));

        });
    } else
        window.alert("Cant read data from sensor " + jsonData.sensor_id);


}
function constractCurrThermoDataSet(json) {
    let jsonData = JSON.parse(json);
    console.log(jsonData.average);
    if (jsonData.total_mesurments_curr !== 0) {
        $("#loader_curr_avg_thermo-container").fadeOut("slow", function () {
            createCurr_avg_Chart(jsonData.average );
        });
    } else
        window.alert("Cant read data from sensor " + jsonData.sensor_id);

}


async function createDay_avg_Chart(value) {
    var fusioncharts = new FusionCharts({
        type: 'thermometer',
        renderAt: 'day_avg_thermo-container',
        width: '240',
        height: '310',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Day Average Temprature ",
//                "subcaption": "Total data count : " + data,
                "lowerLimit": value - 15, //limits
                "upperLimit": value + 15,

                "decimals": "1",
                "numberSuffix": "°C",
                "showhovereffect": "1",
                "thmFillColor": "#0b0f09",
                "showGaugeBorder": "1",
                "gaugeBorderColor": "#0b0f09",
                "gaugeBorderThickness": "2",
                "gaugeBorderAlpha": "30",
                "thmOriginX": "100",
                "chartBottomMargin": "20",
                "valueFontColor": "#000000",
                "theme": "" //fusion
            },
            "value": value, //value
            //All annotations are grouped under this element
            "annotations": {
                "showbelow": "0",
                "groups": [{
                        //Each group needs a unique ID
                        "id": "indicator",
                        "items": [
                            //Showing Annotation
                            {
                                "id": "background",
                                //Rectangle item
                                "type": "rectangle",
                                "alpha": "50",
                                "fillColor": "#AABBCC",
                                "x": "$gaugeEndX-40",
                                "tox": "$gaugeEndX",
                                "y": "$gaugeEndY+54",
                                "toy": "$gaugeEndY+72"
                            }
                        ]
                    }]

            },
        },
        "events": {
            "rendered": function (evt, arg) {//iterval refresh rate
                evt.sender.dataUpdate = setInterval(function () {
                    var value, prevTemp = evt.sender.getData();
                    let xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState === 4) {
                            let jsonData = JSON.parse(xhr.response);
                            mainTemp = jsonData.average_value
                            console.log("Average value interval : " + jsonData.average_value);
                            var diff = Math.abs(prevTemp - mainTemp);

                            diff = diff > 1 ? (Math.random() * 1) : diff;
                            if (mainTemp > prevTemp) {
                                value = prevTemp + diff;
                            } else {
                                value = prevTemp - diff;
                            }
                            console.log("average mesurments : " + jsonData.total_mesurments);
                            console.log("average value : " + jsonData.average_value);
//                          
                            coun_text.innerHTML = " , data from " + jsonData.total_mesurments;
                            evt.sender.feedData("&value=" + value);

                        }
                    }
                    xhr.open('POST', thermo_data, true);
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.send(JSON.stringify({"sensor_id": sensor_id, "date": getDateTime(10)}));



                }, 60 * 1000);
                updateAnnotation = function (evtObj, argObj) {
                    $('.raphael-group-12-background').remove();
                    var code,
                            chartObj = evtObj.sender,
                            val = chartObj.getData(),
                            annotations = chartObj.annotations;

                    if (val <= 15) {
                        code = "#a7d3f0";
                    } else if (val < 29 && val > 15) {
                        code = "#a7d3f0";
                    } else if (val >= 29) {
                        code = "#ff0000";
                    }
                    annotations.update("background", {
                        "fillColor": code
                    });
                };
            },
            'renderComplete': function (evt, arg) {

                updateAnnotation(evt, arg);
                $('.raphael-group-7-background').remove();
                $('.raphael-group-130-creditgroup').remove();
            },
            'realtimeUpdateComplete': function (evt, arg) {

                updateAnnotation(evt, arg);
                $('.raphael-group-7-background').remove();
                $('.raphael-group-130-creditgroup').remove();
            },
            'disposed': function (evt, arg) {
                clearInterval(evt.sender.dataUpdate);
                $('.raphael-group-7-background').remove();
                $('.raphael-group-130-creditgroup').remove();
            }
        }
    }
    );
    onFinish();
//    $('.raphael-group-12-background').remove();
    async function onFinish() {
        await fusioncharts.render();
        // now wait for firstFunction to finish...
        // do something else
        $('.raphael-group-7-background').remove();
    }
    ;

}

function createCurr_avg_Chart(value) {
    var fusioncharts = new FusionCharts({
        type: 'thermometer',
        renderAt: 'curr_avg_thermo-container',
        width: '240',
        height: '310',
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Current Temprature ",
//                "subcaption": "Total data count : " + data,
                "lowerLimit": value - 15, //limits
                "upperLimit": value + 15,

                "decimals": "1",
                "numberSuffix": "°C",
                "showhovereffect": "1",
                "thmFillColor": "#0b0f09",
                "showGaugeBorder": "1",
                "gaugeBorderColor": "#0b0f09",
                "gaugeBorderThickness": "2",
                "gaugeBorderAlpha": "30",
                "thmOriginX": "100",
                "chartBottomMargin": "20",
                "valueFontColor": "#000000",
                "theme": "" //fusion
            },
            "value": value, //value
            //All annotations are grouped under this element
            "annotations": {
                "showbelow": "0",
                "groups": [{
                        //Each group needs a unique ID
                        "id": "indicator",
                        "items": [
                            //Showing Annotation
                            {
                                "id": "background",
                                //Rectangle item
                                "type": "rectangle",
                                "alpha": "50",
                                "fillColor": "#AABBCC",
                                "x": "$gaugeEndX-40",
                                "tox": "$gaugeEndX",
                                "y": "$gaugeEndY+54",
                                "toy": "$gaugeEndY+72"
                            }
                        ]
                    }]

            },
        },
        "events": {
            "rendered": function (evt, arg) {//iterval refresh rate
                evt.sender.dataUpdate = setInterval(function () {
                    var value, prevTemp = evt.sender.getData();
                    let xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState === 4) {
                            let jsonData = JSON.parse(xhr.response);
                            mainTemp = jsonData.average;
                            console.log("Current temprature interval : " + jsonData.average);
                            var diff = Math.abs(prevTemp - mainTemp);

                            diff = diff > 1 ? (Math.random() * 1) : diff;
                            if (mainTemp > prevTemp) {
                                value = prevTemp + diff;
                            } else {
                                value = prevTemp - diff;
                            }
                            console.log("current mesurments : " + jsonData.mesurments);
                            console.log("current value : " + jsonData.average);
//                            coun_text.innerHTML=" , data from " +jsonData.total_mesurments;
                            evt.sender.feedData("&value=" + value);

                        }
                    }
                    xhr.open('POST', url, true);
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.send();//JSON.stringify({"sensor_id": sensor_id, "date": getDateTime(10)})



                }, 15 * 1000);
                updateAnnotation = function (evtObj, argObj) {
                    $('.raphael-group-12-background').remove();
                    var code,
                            chartObj = evtObj.sender,
                            val = chartObj.getData(),
                            annotations = chartObj.annotations;

                    if (val <= 15) {
                        code = "#a7d3f0";
                    } else if (val < 29 && val > 15) {
                        code = "#a7d3f0";
                    } else if (val >= 29) {
                        code = "#ff0000";
                    }
                    annotations.update("background", {
                        "fillColor": code
                    });
                };
            },
            'renderComplete': function (evt, arg) {
                $('.raphael-group-7-background').remove();
                $('.raphael-group-21-creditgroup').remove();
                updateAnnotation(evt, arg);
            },
            'realtimeUpdateComplete': function (evt, arg) {
                $('.raphael-group-21-creditgroup').remove();
                $('.raphael-group-7-background').remove();
                updateAnnotation(evt, arg);
            },
            'disposed': function (evt, arg) {
                $('.raphael-group-7-background').remove();
                $('.raphael-group-21-creditgroup').remove();
                clearInterval(evt.sender.dataUpdate);
            }
        }
    }
    );
    fusioncharts.render();
    $('.raphael-group-7-background').remove();
}
