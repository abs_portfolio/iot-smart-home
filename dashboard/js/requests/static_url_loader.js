/* 
 * icsd thesis by George Fiotakis && Nikolaos Tritsis
 * Each line should be prefixed with  * 
 */
var raspber_controler_automation_ip = "192.168.43.157";//85.72.226.230
var machine_ip_rest = "185.244.128.27";
var local_rest_rpi = "localhost";
var machine_port_rest = "8090";
var raspber_controler_automation_port = "1978";

//chart requests
var get_overal_avg_all = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/overal/getDayAvg";
var get_overal_max_all = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/overal/getMaxDay";
var get_overal_min_all = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/overal/getMinDay";
//! chart Requests
//thermometer old
var thermo_data = " http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/live/getLiveBySensor_id";
var live_avg_by_sensor_id = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/live/getLiveBySensor_id/";
//!thermometer old

//himidity old
var get_avg_tmp = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/daily/getDayAvg";
var get_curr_temp = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/live/getCurrAvg";
//!himidity old
//rpi request

var rpi_status_url = "http://" + local_rest_rpi + ":" + machine_port_rest + "/Rest_Api/secured/rpi/info";
var rpi_devInfo_url = "http://" + local_rest_rpi + ":" + machine_port_rest + "/Rest_Api/secured/rpi/devInfo";
var rpi_triggerDev_url = "http://" + local_rest_rpi + ":" + machine_port_rest + "/Rest_Api/secured/rpi/trigerDevice";

var rpi_auto_info = "http://" + local_rest_rpi + ":" + machine_port_rest + "/Rest_Api/secured/rpi/auto_config_info";
var rpi_auto_trigger = "http://" + local_rest_rpi + ":" + machine_port_rest + "/Rest_Api/secured/rpi/trigerAuto";
var rpi_auto_config = "http://" + local_rest_rpi + ":" + machine_port_rest + "/Rest_Api/secured/rpi/auto_config";

var table_daily_all = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/daily/";
var table_live_all = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/live";
var table_overal_all = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/overal/";

var sensor_id_daily_all = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/daily/all_sensors";
var sensor_id_live_all = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/live/all_sensors";
var get_sensor_raw = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/live/get_sensor_raw/";
var sensor_id_overal_all = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/sensor_list";

var overal_gass_data = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/overal/gasses_chart1 ";

var searchNvsualize_overall = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/overal/searchNvisualize";
var searchNvsualize_daily = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/daily/d_searchNvisualize";
var searchNvsualize_live = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/sensors/live/m_searchNvisualize";


var create_edit_user_url =  "http://" + machine_ip_rest + ":" + machine_port_rest + "/Rest_Api/secured/user/create_edit_usr";

function findUrlReqRpi(request) {
    if (request === "info") {
        return rpi_status_url;
    } else if (request === "dev_info") {
        return rpi_devInfo_url;
    } else if (request === "trigger") {
        return rpi_triggerDev_url;
    } else if (request === "auto_info") {
        return rpi_auto_info;
    } else if (request === "triger_auto") {
        return rpi_auto_trigger;
    }
}
//!rpi request

//ping Machines
var cass_status_url = "http://" + machine_ip_rest + ":" + machine_port_rest + "/cass_prod/system/pingCass";
//!ping Machines

//authorization
var url_auth = "http://" + machine_ip_rest + ":" + machine_port_rest + "/Rest_Api/secured/user/userProfile";
//!authorization
