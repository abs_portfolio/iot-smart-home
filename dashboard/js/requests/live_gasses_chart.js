/* 
 * icsd thesis by George Fiotakis && Nikolaos Tritsis
 * Each line should be prefixed with  * 
 */


/* global searchNvsualize_live */
function openLoaderHomeChartLoader() {
    $("#home_chart_loader").append('<div  id="loader_radar_chart"><img  id="loader" style="max-height: 100%; max-width: 100%; display: block;margin-left: auto; margin-right: auto;fade" src="./images/loading.gif" alt="Be patient..." /></div>');
}

function get_Live_data(json, uri, callback) {
    let req = new XMLHttpRequest();
    req.onreadystatechange = function () {
        if (req.readyState === 4) {
            callback(req.response);
        }
    }

    req.open('POST', uri, true);
    req.setRequestHeader("Content-Type", "application/json");
    req.send(json);
}

function createJsonSearchReq_live_(sensor_list) {
    sensors_to_parse = sensor_list;
    return JSON.stringify({"sensor_list": sensor_list});
}

var live_chart_dom;
var my_live_Chart;
var option_my_chart = null;

$(document).ready(function () {
//    openLoaderHomeChartLoader();
    let json = createJsonSearchReq_live_(live_sensors);
    get_Live_data(json, searchNvsualize_live, create_live_graph);
});



function create_live_graph(json) {
    live_chart_dom = document.getElementById("echart_line");
    my_live_Chart = echarts.init(live_chart_dom);
    let jsonData = JSON.parse(json);
    console.log(json);
    option_my_chart = {
        title: {
            text: 'Live Air Quality data'
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                label: {
                    backgroundColor: '#283b56'
                }
            }
        },
        legend: {
            type: 'scroll',
            bottom: 0,
            data: live_sensors},
        toolbox: {
            show: true,
            feature: {
                dataView: {readOnly: false},
                restore: {},
                saveAsImage: {}
            }
        },
        dataZoom: {
            show: false,
            start: 0,
            end: 100
        },
        xAxis: [],
        yAxis: [
            {
                type: 'value',
                scale: true,
                name: 'PpM',
                max: 600,
                min: 0,
                boundaryGap: [0.2, 0.2]
            }
        ],
        series: []
    };
//    document.getElementById('home_chart_loader').innerHTML = '';
    generate_series_table(jsonData);

    setInterval(function () {
        let json = createJsonSearchReq_live_(live_sensors);
        get_Live_data(json, searchNvsualize_live, update_graph);
    }, 20500);

}

function update_graph(json) {
    let jsonData = JSON.parse(json);
    let value = jsonData.dateList.filter(function (item) {
        return !current_dataList.includes(item.split('.')[0]);
    });
    console.log(jsonData.dateList);
    console.log(current_dataList);
    console.log(value);
    console.log(jsonData);
    if (typeof value !== 'undefined' && value.length > 0) {
        axisData = value[0];
        for (var i = 0; i < live_sensors.length; i++) {
            var data = option_my_chart.series[i].data;
            data.shift();
            if (typeof jsonData.chart_data[live_sensors[i]].data[value[0]] !== 'undefined') {
                console.log(jsonData.chart_data[live_sensors[i]].data[value[0]]);
                data.push(jsonData.chart_data[live_sensors[i]].data[value[0]]);
            } else {
                console.log(jsonData.chart_data[live_sensors[i]].values[jsonData.dateList.length - 2]);
                data.push(jsonData.chart_data[live_sensors[i]].values[jsonData.dateList.length - 2]);
            }


        }
        option_my_chart.xAxis[0].data.shift();
        option_my_chart.xAxis[0].data.push(axisData);

        my_live_Chart.setOption(option_my_chart);
    }
}
var current_dataList;

function constract_xAxis(jsonData) {
    option_my_chart.xAxis.push({
        type: 'category',
        boundaryGap: true,
        data: jsonData.dateList
    });
    current_dataList = jsonData.dateList;
}

function pushLineSeriesElement(obj) {
    option_my_chart.series.push({
        name: obj.sensorId,
        type: 'line',
        data: obj.values
    });

}

function pusheBarSeriesElement(obj) {
    option_my_chart.series.push(
            {
                name: obj.sensorId,
                type: 'bar',
                data: obj.values
            }
    );
}
var sensor_id_live = new Array();

function contractZeroArrray(obj) {
    let arr = new Array();
    for (let i = 0; i < obj.dateList; i++) {
        arr.push(0);
    }
    return arr;
}

function generate_series_table(jsonData) {
    constract_xAxis(jsonData);
    for (let i = 0; i < live_sensors.length; i++) {
        sensor_id_live.push(jsonData.chart_data[live_sensors[i]]);
        if (sensor_id_live[i] !== null) {
            if (i < 5) {
                pushLineSeriesElement(sensor_id_live[i]);
            } else {
                pusheBarSeriesElement(sensor_id_live[i]);
            }
        } else {
            let arr = contractZeroArrray();
            let temp = {"sensorId": live_sensors[i], "values": arr};
            pushLineSeriesElement(temp);
        }
    }
    if (option_my_chart && typeof option_my_chart === "object") {
        my_live_Chart.setOption(option_my_chart, true);
    }
}

var live_sensors = [
    'Alcohol_Living_room',
    'Hexane_Living_room',
    'CH4_Living_room',
    'CO2_Living_room',
    'NH4_Living_room',
    'LPG_Living_room',
    'H2_Living_room',
    'Smoke_Living_room',
    'Benzine_Living_room',
    'CO_Living_room'];