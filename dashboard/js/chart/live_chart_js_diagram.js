/* global get_sensor_raw */

var sensor_id_for_live_chart;
var timeLine_raw_data = new Array();
var dataTable = new Array();


var created = false;
function requestLiveRawData() {

    let xhr2 = new XMLHttpRequest();
    xhr2.onreadystatechange = function () {
        if (xhr2.readyState === 4) {
            constractRawDataSet(xhr2.response);
        }
    }

    xhr2.open('GET', get_sensor_raw + sensor_id_for_live_chart, true);
    xhr2.setRequestHeader("Content-Type", "application/json");
    xhr2.send();
}

function refreshTimeLine(json) {
    for (let i = 0; i < json.length; i++) {
        if (!contains.call(timeLine_raw_data, json[i].ts)) {
            timeLine_raw_data.push(json[i].ts);
            dataTable.push(json[i].value);
        }
    }
}

function constractRawDataSet(json) {
    if (created === false) {
        console.log("DSADSADSADJKSANDKLSADNKSAJDKLBASKDLJSADJNSAKLDBSAJK");
        $("#loader_live_data_air_q").fadeOut(30, function () {
            constractLineChartRaw(createTempDataset(sensor_id_for_live_chart, dataTable), suckme);
        });
        created = true;
    }
    let jsonData = JSON.parse(json);
    console.log(timeLine_raw_data);
    console.log(jsonData[0].ts);
//    for (let i = 0; i < jsonData.length; i++) {
//        if (!contains.call(timeLine_raw_data, jsonData[i].ts) && created) {
//            addData(lineChartRaw, jsonData[i].ts, createTempDataset(json[i].sensorId, json[i].value, ));
//        }
//    }
    refreshTimeLine(jsonData);
    setInterval(requestLiveRawData, 5000);
}

function addData(chart, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    chart.update();
}

function removeData(chart) {
    chart.data.labels.pop();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.pop();
    });
    chart.update();
}


var lineChartRaw;
function constractLineChartRaw(data, options) {
//    console.log(list);
    var ctx = document.getElementById("live_data_air_q");
    lineChartRaw = new Chart(ctx, {
        type: 'line',
        data: data,
        options: options
    });

}

function createTempDataset(labels, values) {
    let colore = constractRundomColore();
    let data = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [
            {
                fillColor: "rgba(220,220,220,0.5)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                data: [65, 59, 90, 81, 56, 55, 40]
            },
            {
                fillColor: "rgba(151,187,205,0.5)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                data: [28, 48, 40, 19, 96, 27, 100]
            }
        ]
    }
    return data;
}


var suckme = {

    //Boolean - If we show the scale above the chart data			
    scaleOverlay: false,

    //Boolean - If we want to override with a hard coded scale
    scaleOverride: false,

    //** Required if scaleOverride is true **
    //Number - The number of steps in a hard coded scale
    scaleSteps: null,
    //Number - The value jump in the hard coded scale
    scaleStepWidth: null,
    //Number - The scale starting value
    scaleStartValue: null,

    //String - Colour of the scale line	
    scaleLineColor: "rgba(0,0,0,.1)",

    //Number - Pixel width of the scale line	
    scaleLineWidth: 1,

    //Boolean - Whether to show labels on the scale	
    scaleShowLabels: false,

    //Interpolated JS string - can access value
    scaleLabel: "<%=value%>",

    //String - Scale label font declaration for the scale label
    scaleFontFamily: "'Arial'",

    //Number - Scale label font size in pixels	
    scaleFontSize: 12,

    //String - Scale label font weight style	
    scaleFontStyle: "normal",

    //String - Scale label font colour	
    scaleFontColor: "#666",

    ///Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: true,

    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",

    //Number - Width of the grid lines
    scaleGridLineWidth: 1,

    //Boolean - Whether the line is curved between points
    bezierCurve: true,

    //Boolean - Whether to show a dot for each point
    pointDot: true,

    //Number - Radius of each point dot in pixels
    pointDotRadius: 3,

    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth: 1,

    //Boolean - Whether to show a stroke for datasets
    datasetStroke: true,

    //Number - Pixel width of dataset stroke
    datasetStrokeWidth: 2,

    //Boolean - Whether to fill the dataset with a colour
    datasetFill: true,

    //Boolean - Whether to animate the chart
    animation: true,

    //Number - Number of animation steps
    animationSteps: 60,

    //String - Animation easing effect
    animationEasing: "easeOutQuart",

    //Function - Fires when the animation is complete
    onAnimationComplete: null

}