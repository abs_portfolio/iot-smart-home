/* 
 * icsd thesis by George Fiotakis && Nikolaos Tritsis
 * Each line should be prefixed with  * 
 */
var myChart;

function openLoaderHomeChartLoader() {
    $("#home_chart_loader").append('<div  id="loader_radar_chart"><img  id="loader" style="max-height: 100%; max-width: 100%; display: block;margin-left: auto; margin-right: auto;fade" src="./images/loading.gif" alt="Be patient..." /></div>');
}

function initializeHomeChart(data) {
    myChart = echarts.init(document.getElementById('echart_line'));
    // specify chart configuration item and data
    var option = {

        tooltip: {trigger: 'axis'},
        legend: {
            x: 220,
            y: 40,
            data: data
        },
        xAxis: {
            data: []
        },
        tooltip: {
            trigger: 'axis'
        },
        toolbox: {
            show: true,
            orient: 'vertical',
            itemGap: 20,
            feature: {
                magicType: {
                    show: true,
                    title: {
                        line: 'Line',
                        bar: 'Bar',
                        stack: 'Stack',
                        tiled: 'Tiled'
                    },
                    type: ['line', 'bar', 'stack', 'tiled']
                },
                restore: {
                    show: true,
                    title: "Restore"
                },
                saveAsImage: {
                    show: true,
                    title: "Save Image"
                }
            }
        },
        dataZoom: [
            {// This dataZoom component controls x-axis by dafault
                type: 'slider', // this dataZoom component is dataZoom component of slider
                start: 0, // the left is located at 10%
                end: 100         // the right is located at 60%
            }
        ],
        calculable: true,
        yAxis: {}
        ,
        itemStyle: {
            normal: {
                shadowBlur: 200,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
        },
        series: []
    };
    for (var i = 0; i < data.length; i++) {
        option.series.push({
            name: data[i],
            type: 'line'
        });
    }
    myChart.setOption(option);
    temp_sensor_list = data;
    getSensor_per_row_home(setOption); //fill chart
}
var temp_sensor_list;
function getSensor_per_row_home(callback) {

    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            callback(xhr.response);
        }
    }

    xhr.open('GET', get_overal_avg_all, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send();
}


function setOption(json) {
    let jsonData = JSON.parse(json);
    console.log(jsonData.dateList);
    let opt = {
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: jsonData.dateList
        },
        series: []};

    for (var i = 0; i < temp_sensor_list.length; i++) {
        let tempdata= new Array();
//        console.log(jsonData.chart_data[temp_sensor_list[i]]);
//        console.log(temp_sensor_list[i]);
//        console.log(jsonData.chart_data[temp_sensor_list[i]].data);
        for (let j = 0; j < jsonData.dateList.length; j++) {
            tempdata.push(jsonData.chart_data[temp_sensor_list[i]].data[jsonData.dateList[j]]);
        }
//        console.log(tempdata);
//        console.log("FINISHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
        opt.series.push({
            // find series by name
            name: temp_sensor_list[i],
            smooth: true,
            itemStyle: {
                normal: {
                    areaStyle: {
                        type: 'default'
                    }
                }
            },
            data: tempdata//hyugugukh
        });
    }
    myChart.setOption(opt);
}