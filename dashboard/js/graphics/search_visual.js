/* 
 * icsd thesis by George Fiotakis && Nikolaos Tritsis
 * Each line should be prefixed with  * 
 */

//request Function
function getSearchNVisualisedata(json, uri, callback) {

    let req = new XMLHttpRequest();
    req.onreadystatechange = function () {
        if (req.readyState === 4) {
            callback(req.response);
        }
    }

    req.open('POST', uri, true);
    req.setRequestHeader("Content-Type", "application/json");
    req.send(json);
}
/* global get_overal_avg_all, get_overal_min_all, get_overal_max_all, searchNvsualize_overall, searchNvsualize_daily, date_start, date_finish, constract_sensor_search_nVisualize, searchNvsualize_live */

var start_date;
var finish_date;

var start_time = "00:00:00";
var finish_time = "23:59:59";

var living_room = $('<optgroup>');
var bedroom = $('<optgroup>');
var kitchen = $('<optgroup>');

var sensors_to_parse;
//on document load
window.onload = function () {
    console.log(sessionStorage.getItem("username"));
    document.getElementById("user_name_h2").innerHTML = sessionStorage.getItem("username");
    document.getElementById("dropdown_username").appendChild(document.createTextNode(sessionStorage.getItem("username")));
    set_sensor_multiselect_data();
    $('#daterange').prop("disabled", true);
    $('#datetimepicker3').prop("disabled", true);
};
// run pre selected options
$('#sensor_list_multisellect').multiSelect();

//search button listener
$('#search_button').click(function () {
    if ($('#service_pick').val() === "overall") {
        let start = new Date(start_date).getTime();
        let finish = new Date(finish_date).getTime();
        let json = createJsonSearchReq(start, finish, getSelectValues(document.getElementById("sensor_list_multisellect")), $('select[name=function_sellect]').val());
        console.log(searchNvsualize_overall);
        console.log(json);
        getSearchNVisualisedata(json, searchNvsualize_overall, process_results);
    } else if ($('#service_pick').val() === "daily") {
        let start = convertTimeToTs(start_time);
        let finish = convertTimeToTs(finish_time);
        let json = createJsonDateSearchReq(start, finish, getSelectValues(document.getElementById("sensor_list_multisellect")), $('select[name=function_sellect]').val());
        console.log(searchNvsualize_daily);
        console.log(json);
        getSearchNVisualisedata(json, searchNvsualize_daily, process_results);
    } else if ($('#service_pick').val() === "live") {
        let json = createJsonSearchReq_live(getSelectValues(document.getElementById("sensor_list_multisellect")));
        getSearchNVisualisedata(json, searchNvsualize_live, process_results);
    }
});
//setting multy sellect sensors data
function set_sensor_multiselect_data() {
    getOveralActiveSensors(constract_sensor_search_nVisualize);
}

function multysellect_callback(sensor_id, model, region, sensor_list) {
//    console.log( sensor_list);
    kitchen.attr('label', 'Kitchen');
    bedroom.attr('label', 'Bedroom');
    living_room.attr('label', 'Living Room');

    for (var i = 0; i < sensor_id.length; i++) {
        if (sensor_list[i].region.toUpperCase() === "Bedroom".toUpperCase()) {
            bedroom.append("<option value=\"" + sensor_id[i] + "\">" + sensor_id[i].substring(0, sensor_id[i].indexOf('_')) + "</option>");
        } else if (sensor_list[i].region.toUpperCase() === "Living_room".toUpperCase()) {
            living_room.append("<option value=\"" + sensor_id[i] + "\">" + sensor_id[i].substring(0, sensor_id[i].indexOf('_')) + "</option>");
        } else if (sensor_list[i].region.toUpperCase() === "Kitchen".toUpperCase()) {
            kitchen.append("<option value=\"" + sensor_id[i] + "\">" + sensor_id[i].substring(0, sensor_id[i].indexOf('_')) + "</option>");
        }
    }
    $("#sensor_list_multisellect").append(kitchen);
    $("#sensor_list_multisellect").append(bedroom);
    $("#sensor_list_multisellect").append(living_room);
    $('#sensor_list_multisellect').multiSelect('refresh');
}
function getSelectValues(select) {
    let result = [];
    let options = select && select.options;
    let opt;

    for (var i = 0, iLen = options.length; i < iLen; i++) {
        opt = options[i];

        if (opt.selected) {
            result.push(opt.value || opt.text);
        }
    }
    return result;
}

//construct the json to send as request to cass_prod
function createJsonSearchReq(date_start, date_finish, sensor_list, funct) {
    sensors_to_parse = sensor_list;
    return JSON.stringify({"start": date_start, "finish": date_finish, "sensor_list": sensor_list, "funct": funct});
}
function createJsonSearchReq_live(sensor_list) {
    sensors_to_parse = sensor_list;
    return JSON.stringify({"sensor_list": sensor_list});
}
function createJsonDateSearchReq(date_start, date_finish, sensor_list, funct) {
    sensors_to_parse = sensor_list;
    let d1 = dateTsToSearch(start_time);
    let d2 = dateTsToSearch(finish_time);
    return JSON.stringify({"date_start": d1, "date_finish": d2, "start": date_start, "finish": date_finish, "sensor_list": sensor_list, "funct": funct});
}


//Service sellect Listener
$('#service_pick').click(function () {
    console.log($('#service_pick').val());
    if ($('#service_pick').val() === "overall") {
        $('#daterange').prop("disabled", false);
        $('#datetimepicker3').prop("disabled", true);
    } else if ($('#service_pick').val() === "daily") {
        $('#daterange').prop("disabled", true);
        $('#datetimepicker3').prop("disabled", false);
    } else if ($('#service_pick').val() === "live") {
        $('#daterange').prop("disabled", true);
        $('#datetimepicker3').prop("disabled", true);
    }
});

////////////////////////////// Convert Timestamps
function convertDateToTs(myDate) {
    myDate = myDate.split("/");
    var newDate = myDate[0] + "/" + myDate[1] + "/" + myDate[2];
    alert(new Date(newDate).getTime());
}

var currentDate = new Date();

var date = currentDate.getDate();
var month = currentDate.getMonth(); //Be careful! January is 0 not 1
var year = currentDate.getFullYear();
var dateString = date + "-" + (month + 1) + "-" + year;

function connstractCurrnetDate() {
    let datestr = year + "-";
    if ((month + 1) < 10) {
        datestr = datestr + "0" + (month + 1) + "-";
    } else {
        datestr = datestr + (month + 1) + "-";
    }
    if (date < 10) {
        datestr = datestr + "0" + date;
    } else {
        datestr = datestr + date;
    }
    return datestr;
}

function convertTimeToTs(time) {

    var d = dateString + " " + time,
            dArr = d.split('-'),
            ts = new Date(dArr[1] + "-" + dArr[0] + "-" + dArr[2] + currentDate.getTimezoneOffset() * 60 * 1000).getTime();
    return ts;
}

function dateTsToSearch(time) {
    return connstractCurrnetDate() + " " + time;
}

////////////////////// Charts ///////////////////////////////////////

function process_results(json) {

    $("#loader_bar_chart").fadeOut(30, function () {
        constructDataSet(json);
    });
}

function constructDataSet(json) {
    console.log(json);
    let jsonData = JSON.parse(json);
    let sensor_id = new Array();
    for (let i = 0; i < sensors_to_parse.length; i++) {
        sensor_id.push(jsonData.chart_data[sensors_to_parse[i]]);
    }
    console.log(sensor_id);
    let datasets = new Array();
    for (var i = 0; i < sensor_id.length; i++) {
        if (sensor_id[i] !== null) {
            datasets.push(createDataset(sensor_id[i]));
            console.log(datasets[i]);
        }
    }
    $("#loader_bar_chart").fadeOut(30, function () {
        switch ($('#chart_pick').val()) {
            case 'line':
                constractLineChart(datasets, jsonData.dateList);
                break;
            case 'bar':
                constractBatChart(datasets, jsonData.dateList);
                break;
            case 'radar':
                constractRadarChart(datasets, jsonData.dateList);
                break;
            default:
                constractLineChart(datasets, jsonData.dateList);
        }

    });

    creteDataTable(json);

}

function createDataset(data) {
    let colore = constractRundomColore();
    let dataset = {
        label: data.sensorId,
        borderColor: colore,
        pointBorderColor: "rgba(38, 185, 154, 0.7)",
        pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        fillColor: colore,
        pointBorderWidth: 1,
        data: data.values
    }
    return dataset;
}

function constractRundomColore() {
    let num = Math.floor(Math.random() * 23);
    let static_colors = ["#455C73",
        "#9B59B6",
        "#BDC3C7",
        "#26B99A",
        "#ffa700",
        "#ff0040",
        "#929a9e",
        "#1d2e36",
        "#40dbdb",
        "#008744",
        "#f9cece",
        "#4a6ca8",
        "#DC143C",
        "#B22222",
        "#967357",
        "#d3108f",
        "#3e5144",
        "#053013",
        "#474002",
        "#0259f9",
        "#224077",
        "#141413",
        "#04ef4f",
        "#3498DB"];
    return static_colors[num];
}

function constractLineChart(datasets, list) {
    console.log(list);
    var ctx = document.getElementById("search_result");
    var lineChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: list,
            datasets: datasets
        },
    });

}

function constractBatChart(datasets, list) {
    var ctx = document.getElementById("search_result");
    var mybarChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: list,
            datasets: datasets
        },
        options: {
            scale: {
                ticks: {
                    beginAtZero: true
                }
            }
        }
    });

}
function constractRadarChart(datasets, labeling) {

    var ctx = document.getElementById("search_result");
    var data = {
        labels: labeling,
        datasets: datasets
    };

    var canvasRadar = new Chart(ctx, {
        type: 'radar',
        data: data
    });


}



//////////////////////////////////////tables functions////////////////////////////////
var table_contents;

function creteDataTable(json) {
    $("#loader_bar_table").fadeOut(30, function () {
        constractSensorDataSet(json, "search_table");
    });
}


function constractSensorDataSet(json, container) {
    let jsonData = JSON.parse(json);
    let aaData = new Array();

    for (let i = 0; i < sensors_to_parse.length; i++) {
        if (jsonData.chart_data[sensors_to_parse[i]] === null) {
            window.alert("Sorry but the sensor " + sensors_to_parse[i] + " was not active at that time period. So it will not be included to the graph and to the table.");
            continue;
        }
        for (let j = 0; j < jsonData.dateList.length; j++) {

            aaData.push([sensors_to_parse[i],
                jsonData.chart_data[sensors_to_parse[i]].model,
                jsonData.chart_data[sensors_to_parse[i]].region,
                (jsonData.chart_data[sensors_to_parse[i]].values[j]).toFixed(2),
                jsonData.dateList[j]
            ]);
        }
    }
    table_contents = json;
    constractSensorTable(aaData, container);
}

function constractSensorDataSetLabel(json, container, new_column) {
    let jsonData = JSON.parse(json);
    let aaData = new Array();

    for (let i = 0; i < sensors_to_parse.length; i++) {
        for (let j = 0; j < jsonData.dateList.length; j++) {
            aaData.push([sensors_to_parse[i],
                jsonData.chart_data[sensors_to_parse[i]].model,
                jsonData.chart_data[sensors_to_parse[i]].region,
                (jsonData.chart_data[sensors_to_parse[i]].values[j]).toFixed(2),
                jsonData.dateList[j], "", "", ""
            ]);
        }
    }

    constractSensorTableLabel(aaData, container, new_column);
}
var data_table_con;
var colums_count = [{title: "Sensor-Id"}, {title: "Model"}, {title: "Region"}, {title: "Value"}, {title: "Date"}];
function constractSensorTable(dataSet, container) {
    data_table_con = $("#" + container).DataTable({
        destroy: true,
        data: dataSet,
        columns: [
            {title: "Sensor-Id"},
            {title: "Model"},
            {title: "Region"},
            {title: "Value"},
            {title: "Date"}
        ],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
}

function recreateDataTableWithNewCols() {

}

$('#add_column').click(function () {
    if ($("#colum_input").val() === "") {
        window.alert("Please add a column name to proceed.");
    } else {
        data_table_con.destroy();
        $("#" + "search_table").empty();
        constractSensorDataSetLabel(table_contents, "search_table", $("#colum_input").val());
    }
});

function constractSensorTableLabel(dataSet, container, new_column) {
    console.log(new_column);
    colums_count.push({title: new_column});
    data_table_con = $("#" + container).DataTable({
        destroy: true,
        data: dataSet,
        columns: colums_count,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
}
