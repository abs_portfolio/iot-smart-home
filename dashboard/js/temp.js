/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//< -- REQUEST FUNCTIONS
            function getUsers(token, callback) {
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        callback(xhr.response);
                    }
                }

                xhr.open('GET', get_usr, true);
                xhr.withCredentials = true;
                xhr.setRequestHeader("Content-Type", "text/plain");
                xhr.setRequestHeader("token", token);
                xhr.send();
            }

            function constractDataSet(json) {
                let jsonData = JSON.parse(json);
                let aaData = new Array();
                console.log(jsonData.email)
                for (i in jsonData) {
                    aaData.push([jsonData[i].id,
                        jsonData[i].name,
                        jsonData[i].surname,
                        jsonData[i].email,
                        jsonData[i].username,
                        jsonData[i].roleId,
                        jsonData[i].dateCreated
                    ])
                }
                createUserTable(aaData);
            }
            function createUserTable(dataSet) {

                $("#example").DataTable({
                    data: dataSet,
                    columns: [
                        {title: "Id"},
                        {title: "Name"},
                        {title: "Surname"},
                        {title: "E-mail"},
                        {title: "Username"},
                        {title: "Role - Id"},
                        {title: "Date - Created"}
                    ]
                });
            }